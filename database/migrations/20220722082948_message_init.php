<?php

use think\migration\Migrator;

class MessageInit extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('message', ['collation' => 'utf8mb4_general_ci', 'comment' => '消息通知表'])
            ->addColumn('source', 'string', ['limit' => 32, 'default' => '', 'comment' => '来源,见dict'])
            ->addColumn('sourceid', 'integer', ['default' => 0, 'comment' => '来源id'])
            ->addColumn('topic', 'string', ['limit' => 128, 'default' => '', 'comment' => '话题'])
            ->addColumn('to_userid', 'integer', ['default' => 0, 'comment' => '接收人id'])
            ->addColumn('title', 'string', ['limit' => 255, 'default' => '', 'comment' => '标题'])
            ->addColumn('content', 'string', ['limit' => 255, 'default' => '', 'comment' => '内容'])
            ->addColumn('read_time', 'integer', ['default' => 0,  'comment' => '阅读时间'])
            ->addColumn('dest', 'string', ['limit' => 128, 'default' => '', 'comment' => '目的地'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('topic')
            ->addIndex('to_userid')
            ->addIndex('read_time')
            ->create();

        $this->table('message_delivery', ['collation' => 'utf8mb4_general_ci', 'comment' => '消息分发表'])
            ->addColumn('channel', 'enum', ['values' => ['email', 'shortMsg', 'wxGzhMsg'],  'default' => 'wxGzhMsg', 'comment' => ''])
            ->addColumn('messageid', 'integer', ['default' => 0, 'comment' => ''])
            ->addColumn('to_user', 'string', ['limit' => 255, 'default' => '', 'comment' => '接收人,公众号是openid,email是邮箱地址,短信是电话号码'])
            ->addColumn('title', 'string', ['limit' => 255, 'default' => '', 'comment' => '标题,公众号消息title无效'])
            ->addColumn('content', 'text', ['null' => true, 'default' => null, 'comment' => '内容'])
            ->addColumn('expect_send_time', 'integer', ['default' => 0,  'comment' => '预期发送时间'])
            ->addColumn('real_send_time', 'integer', ['default' => 0,  'comment' => '送达时间'])
            ->addColumn('templateid', 'string', ['limit' => 255, 'default' => '', 'comment' => '模板id,短信和公众号消息有不同的用法'])
            ->addColumn('result', 'string', ['limit' => 255, 'default' => '', 'comment' => '消息返回结果'])
            ->addColumn('ttl', 'integer', ['default' => 3,  'comment' => ''])
            ->addColumn('dest', 'string', ['limit' => 128, 'default' => '', 'comment' => '目的地'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('channel')
            ->addIndex('to_user')
            ->addIndex('expect_send_time')
            ->addIndex('ttl')
            ->create();
    }
}
