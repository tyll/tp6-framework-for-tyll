<?php

use think\migration\Migrator;

class XunjianInit extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //检查项
        $this->table('check_item', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查项表'])
            ->addColumn('target', 'string', ['limit' => 64, 'default' => '', 'comment' => '检查目标,见dict'])
            ->addColumn('pk', 'integer', ['default' => 0, 'comment' => '检查目标id'])
            ->addColumn('type', 'string', ['limit' => 32, 'default' => '', 'comment' => '类型,见dict'])
            ->addColumn('content', 'string', ['limit' => 255, 'default' => '', 'comment' => '检查项内容'])
            ->addColumn('unit', 'string', ['limit' => 16, 'default' => '', 'comment' => '单位,type为number时填入'])
            ->addColumn('jianchazhouqi', 'integer', ['default' => 0, 'comment' => '建议多久检查一次,单位天'])
            ->addColumn('jianchazhouqi_unit', 'string', ['limit' => 16, 'default' => '', 'comment' => '检查周期单位'])
            ->addColumn('tag', 'string', ['limit' => 255, 'default' => '', 'comment' => '标签'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('target')
            ->addIndex('pk')
            ->addIndex('jianchazhouqi')
            ->create();

        $this->table('check_item_option', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查项选择项表'])
            ->addColumn('check_itemid', 'integer', ['default' => 0, 'comment' => '检查项id'])
            ->addColumn('content', 'string', ['limit' => 255, 'default' => '', 'comment' => '选项内容'])
            ->addColumn('health', 'integer', ['default' => 1, 'comment' => '健康状态,见dict'])//0不健康 1健康
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('check_itemid')
            ->create();

        $this->table('check_schedule', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查计划表'])
            ->addColumn('title', 'string', ['limit' => 255, 'default' => '', 'comment' => '标题'])
            ->addColumn('canyu_userids', 'json', ['null' => true, 'default' => null, 'comment' => '参与人id'])
            ->addColumn('type', 'integer', ['default' => 0, 'comment' => '类型,见dict', 'after' => 'canyu_userids'])//多人参与完成,每人独立完成
            ->addColumn('period', 'string', ['limit' => 16, 'default' => '', 'comment' => '周期,day,week,month'])
            ->addColumn('path', 'string', ['limit' => 255, 'default' => '', 'comment' => '路径'])
            ->addColumn('status', 'integer', ['default' => 0, 'comment' => '状态,见dict'])
            ->addColumn('enable', 'integer', ['default' => 0, 'comment' => '启用禁用,见dict'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('period')
            ->addIndex('status')
            ->create();

        $this->table('r_item_schedule', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查项计划关联表'])
            ->addColumn('check_scheduleid', 'integer', ['default' => 0, 'comment' => '检查计划id'])
            ->addColumn('check_itemid', 'integer', ['default' => 0, 'comment' => '检查项id'])
            ->addColumn('sort', 'integer', ['default' => 0, 'comment' => '排序'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('check_scheduleid')
            ->addIndex('check_itemid')
            ->create();

        $this->table('check_task_group', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查任务组表'])
            ->addColumn('check_scheduleid', 'integer', ['default' => 0, 'comment' => '检查计划id'])
            ->addColumn('title', 'string', ['limit' => 255, 'default' => '', 'comment' => '标题'])
            ->addColumn('canyu_userids', 'json', ['null' => true, 'default' => null, 'comment' => '参与人id'])
            ->addColumn('expect_start_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '预期开始时间'])
            ->addColumn('expect_end_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '预期结束时间'])
            ->addColumn('fact_userids', 'json', ['null' => true, 'default' => null, 'comment' => '实际巡检人id'])
            ->addColumn('fact_start_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '实际开始时间'])
            ->addColumn('fact_end_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '实际结束时间'])
            ->addColumn('bad_items', 'integer', ['default' => 0, 'comment' => '异常项数量'])
            ->addColumn('status', 'integer', ['default' => 0, 'comment' => '状态,见dict'])//未开始  进行中   已完成
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('check_scheduleid')
            ->addIndex('status')
            ->create();

        $this->table('check_task', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查任务表'])
            ->addColumn('task_groupid', 'integer', ['default' => 0, 'comment' => '检查任务组id'])
            ->addColumn('check_itemid', 'integer', ['default' => 0, 'comment' => '检查项id'])
            ->addColumn('canyu_userids', 'json', ['null' => true, 'default' => null, 'comment' => '参与人id'])
            ->addColumn('expect_start_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '预期开始时间'])
            ->addColumn('expect_start_timestamp', 'integer', ['default' => 0, 'comment' => '预计开始时间戳'])
            ->addColumn('expect_end_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '预期结束时间'])
            ->addColumn('done_userid', 'integer', ['default' => 0, 'comment' => '完成人id'])
            ->addColumn('done_time', 'integer', ['default' => 0, 'comment' => '完成时间'])
            ->addColumn('answer', 'string', ['limit' => 32, 'default' => '', 'comment' => '答案'])
            ->addColumn('note', 'string', ['limit' => 255, 'default' => '', 'comment' => '备注'])
            ->addColumn('fujian', 'json', ['null' => true, 'default' => null, 'comment' => '附件'])
            ->addColumn('health', 'integer', ['default' => 1, 'comment' => '健康状态,见dict'])//1健康  0不健康
            ->addColumn('xunjian_position', 'string', ['limit' => 255, 'default' => '', 'comment' => '实际巡检位置,逗号相隔经纬度'])
            ->addColumn('xunjian_jlpc', 'float', ['default' => 0, 'comment' => '与设备偏差的距离'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('task_groupid')
            ->addIndex('check_itemid')
            ->addIndex('date')
            ->create();

        $this->table('check_target_schedule', ['collation' => 'utf8mb4_general_ci', 'comment' => '目标检查计划表'])
            ->addColumn('check_scheduleid', 'integer', ['default' => 0, 'comment' => '检查计划id'])
            ->addColumn('target', 'string', ['limit' => 64, 'default' => '', 'comment' => '检查目标,见dict'])
            ->addColumn('targetid', 'integer', ['default' => 0, 'comment' => '检查目标id'])
            ->addColumn('enable', 'integer', ['default' => 0, 'comment' => '启用禁用,见dict'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('check_scheduleid')
            ->addIndex('enable')
            ->create();

        $this->table('check_task_target_group', ['collation' => 'utf8mb4_general_ci', 'comment' => '目标检查任务组表'])
            ->addColumn('task_groupid', 'integer', ['default' => 0, 'comment' => '检查任务组id'])
            ->addColumn('target', 'string', ['limit' => 64, 'default' => '', 'comment' => '检查目标,见dict'])
            ->addColumn('targetid', 'integer', ['default' => 0, 'comment' => '检查目标id'])
            ->addColumn('fact_userids', 'json', ['null' => true, 'default' => null, 'comment' => '实际巡检人id'])
            ->addColumn('status', 'integer', ['default' => 0, 'comment' => '状态,见dict'])//未开始 已完成 进行中
            ->addColumn('bad_items', 'integer', ['default' => 0, 'comment' => '异常项数量', 'after' => 'fact_end_time'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('task_groupid')
            ->addIndex('status')
            ->create();

        $this->table('check_schedule_times', ['collation' => 'utf8mb4_general_ci', 'comment' => '检查计划时间段表'])
            ->addColumn('check_scheduleid', 'integer', ['default' => 0, 'comment' => '计划id'])
            ->addColumn('start_sn_day', 'integer', ['default' => 0, 'comment' => '周期的第几天开始'])
            ->addColumn('end_sn_day', 'integer', ['default' => 0, 'comment' => '周期的第几天结束'])
            ->addColumn('start_time', 'time', ['null' => true, 'default' => null, 'comment' => '开始时间'])
            ->addColumn('end_time', 'time', ['null' => true, 'default' => null, 'comment' => '结束时间'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('check_scheduleid')
            ->create();
    }
}
