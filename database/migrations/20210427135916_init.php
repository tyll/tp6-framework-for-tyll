<?php

use Phinx\Db\Adapter\MysqlAdapter;
use think\migration\Migrator;

class Init extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('user', ['collation' => 'utf8mb4_general_ci', 'comment' => '用户表'])
            ->addColumn('name', 'string', ['limit' => 16, 'default' => '', 'comment' => '用户姓名'])
            ->addColumn('tel', 'string', ['limit' => 16, 'default' => '', 'comment' => '电话号码'])
            ->addColumn('openid', 'string', ['limit' => 64, 'default' => '', 'comment' => '微信opennid'])
            ->addColumn('password', 'string', ['limit' => 64, 'default' => '', 'comment' => '登录密码'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('tel')
            ->addIndex('openid')
            ->create();

        $row = ['name' => 'admin', 'tel' => 'root', 'password' => md5('root')];
        $this->table('user')->insert($row)->update();

        //visitor
        $this->table('visitor', ['collation' => 'utf8mb4_general_ci', 'comment' => '微信用户表'])
            ->addColumn('openid', 'string', ['limit' => 100, 'default' => '', 'comment' => ''])
            ->addColumn('nickname', 'string', ['limit' => 255, 'default' => '', 'comment' => '昵称'])
            ->addColumn('tel', 'string', ['limit' => 16, 'default' => '', 'comment' => '电话号码'])
            ->addColumn('headimgurl', 'string', ['limit' => 255, 'default' => '', 'comment' => '头像'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex(['openid'], ['unique' => true])
            ->create();

        $this->table('conf', ['collation' => 'utf8mb4_general_ci', 'comment' => '配置表'])
            ->addColumn('k', 'string', ['limit' => 255, 'default' => '', 'comment' => ''])
            ->addColumn('v', 'text', ['limit' => MysqlAdapter::TEXT_LONG])
            ->addColumn('remark', 'string', ['limit' => 255, 'default' => '', 'comment' => '描述'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->create();
        $rows = [
            ['k' => 'gzh_appid', 'v' => 'wxcb5b14c964fadb27', 'remark' => '微信公众号appid'],
            ['k' => 'gzh_appsecret', 'v' => '', 'remark' => '微信公众号appsecret'],
            ['k' => 'xcx_appid', 'v' => 'wxa3ca2a74e15a5dd7', 'remark' => '小程序appid'],
            ['k' => 'xcx_appsecret', 'v' => 'c74420f54c4ed2d81e2a253a778abcbe', 'remark' => '小程序appsecret'],
            ['k' => 'tencent_secret_id', 'v' => 'AKIDSr5IKCwy770iEJnCxb4TgigXHUXQelAP', 'remark' => ''],
            ['k' => 'tencent_secret_key', 'v' => 'CC2hARWP9hUFtSr693mJesulRwX4Tmz3', 'remark' => ''],
        ];
        $this->table('conf')->insert($rows)->update();

        $this->table('admin', ['collation' => 'utf8mb4_general_ci', 'comment' => '管理员关联表'])
            ->addColumn('userid', 'integer', ['default' => 0, 'comment' => '人员id'])
            ->addColumn('deptid', 'integer', ['default' => 0, 'comment' => '部门id'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('userid')
            ->addIndex('deptid')
            ->create();
        //admin
        /* $this->table('admin', ['collation' => 'utf8mb4_general_ci', 'comment' => '后台管理员表'])
        ->addColumn('username', 'string', ['limit' => 255, 'default' => '', 'comment' => '用户名'])
        ->addColumn('password', 'string', ['limit' => 255, 'default' => '', 'comment' => '密码'])
        ->addColumn('realname', 'string', ['limit' => 255, 'default' => '', 'comment' => '真实姓名'])
        ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
        ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
        ->addIndex('username')
        ->create();
        $row = ['username' => 'admin', 'password' => md5('admin'), 'realname' => '超级管理员'];
        $this->table('admin')->insert($row)->update(); */

        $this->table('visits', ['collation' => 'utf8mb4_general_ci', 'comment' => '访问记录表'])
            ->addColumn('openid', 'integer', ['default' => 0, 'comment' => '用户id'])
            ->addColumn('page', 'string', ['limit' => 64, 'default' => '', 'comment' => '页面'])
            ->addColumn('page_code', 'string', ['limit' => 64, 'default' => '', 'comment' => '页面代号'])
            ->addColumn('params', 'json', ['comment' => '参数'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->create();

        //通讯录
        $this->table('tongxunlu', ['collation' => 'utf8mb4_general_ci', 'comment' => '通讯录表'])
            ->addColumn('userid', 'integer', ['default' => 0, 'comment' => '用户id'])
            ->addColumn('deptid', 'integer', ['default' => 0, 'comment' => '部门id'])
            ->addColumn('positionid', 'integer', ['default' => 0, 'comment' => '岗位id'])
            ->addColumn('status', 'integer', ['default' => 0, 'comment' => '状态,见dict'])
            ->addColumn('rz_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '入职时间'])
            ->addColumn('lz_time', 'datetime', ['null' => true, 'default' => null, 'comment' => '离职时间'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('userid')
            ->addIndex('deptid')
            ->addIndex('positionid')
            ->addIndex('status')
            ->create();

        //部门
        $this->table('dept', ['collation' => 'utf8mb4_general_ci', 'comment' => '部门表'])
            ->addColumn('name', 'string', ['limit' => 64, 'default' => '', 'comment' => '部门名称'])
            ->addColumn('parentid', 'integer', ['default' => 0, 'comment' => '上级部门id'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('parentid')
            ->create();

        $this->table('position', ['collation' => 'utf8mb4_general_ci', 'comment' => '岗位表'])
            ->addColumn('name', 'string', ['limit' => 64, 'default' => '', 'comment' => '名称'])
            ->addColumn('deptid', 'integer', ['default' => 0, 'comment' => '所属部门id'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('deptid')
            ->create();

        //默认部门
        $row = ['id' => 1, 'name' => '宝丞炭材', 'parentid' => 0];
        $this->table('dept')->insert($row)->save();

        //默认岗位
        $rows = ['id' => 1, 'name' => '任务调度员', 'deptid' => 1];
        $this->table('position')->insert($rows)->save();

        $this->table('role_permition', ['collation' => 'utf8mb4_general_ci', 'comment' => '角色权限表'])
            ->addColumn('positionid', 'integer', ['default' => 0, 'comment' => '岗位id'])
            ->addColumn('permition', 'string', ['limit' => 128, 'default' => '', 'comment' => '权限'])
            ->addColumn('action', 'string', ['limit' => 32, 'default' => '', 'comment' => '操作,见dict'])
            ->addColumn('note', 'string', ['limit' => 255, 'default' => '', 'comment' => '描述'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('positionid')
            ->addIndex('action')
            ->create();

        $this->table('token', ['collation' => 'utf8mb4_general_ci', 'comment' => 'token表'])
            ->addColumn('key', 'string', ['limit' => 16, 'default' => '', 'comment' => 'token提供方标识'])
            ->addColumn('token', 'string', ['limit' => 255, 'default' => '', 'comment' => 'token'])
            ->addColumn('token_expire_time', 'integer', ['default' => 0, 'comment' => 'token过期时间'])
            ->addColumn('refresh_token', 'string', ['limit' => 255, 'default' => '', 'comment' => '刷新令牌'])
            ->addColumn('retoken_expire_time', 'integer', ['default' => 0, 'comment' => '刷新令牌过期时间'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('key')
            ->create();
    }
}
