<?php

use think\migration\Migrator;

class GongdanInit extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('gongdan', ['collation' => 'utf8mb4_general_ci', 'comment' => '工单表'])
            ->addColumn('from_userid', 'integer', ['default' => 0, 'comment' => '发起人id'])
            ->addColumn('sure_userid', 'integer', ['default' => 0, 'comment' => '确认人id'])
            ->addColumn('curr_userids', 'json', ['null' => true, 'default' => null, 'comment' => '当前处理人id'])
            ->addColumn('target', 'string', ['limit' => 64, 'default' => '', 'comment' => '目标，见dict'])
            ->addColumn('targetid', 'integer', ['default' => 0, 'comment' => '目标id'])
            ->addColumn('check_taskid', 'integer', ['default' => 0, 'comment' => '巡检任务id'])
            ->addColumn('name', 'string', ['limit' => 64, 'default' => '', 'comment' => '名称'])
            ->addColumn('status', 'integer', ['default' => 0, 'comment' => '状态，见dict'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('from_userid')
            ->addIndex('sure_userid')
            ->addIndex('check_taskid')
            ->create();

        $this->table('gongdan_flow', ['collation' => 'utf8mb4_general_ci', 'comment' => '工单流程表'])
            ->addColumn('gongdanid', 'integer', ['default' => 0, 'comment' => '工单id'])
            ->addColumn('oper_userid', 'integer', ['default' => 0, 'comment' => '操作人id'])
            ->addColumn('to_userids', 'json', ['null' => true, 'default' => null, 'comment' => '转发人id'])
            ->addColumn('action', 'string', ['limit' => 64, 'default' => '', 'comment' => '操作，见dict'])
            ->addColumn('note', 'text', ['null' => true, 'default' => null, 'comment' => '备注'])
            ->addColumn('fujian', 'json', ['null' => true, 'default' => null, 'comment' => '附件'])
            ->addColumn('c_time', 'timestamp', ['default' => 'CURRENT_TIMESTAMP', 'comment' => '创建时间'])
            ->addColumn('delete_time', 'integer', ['default' => 0, 'comment' => '删除时间'])
            ->addIndex('gongdanid')
            ->addIndex('oper_userid')
            ->create();
    }
}
