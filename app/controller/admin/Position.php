<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\Dept as ModelDept;
use app\model\Position as ModelPosition;
use app\model\RolePermition as ModelRolePermition;

/**
 * 岗位.
 */
class Position extends Base
{
    protected $middleware = [TokenAuth::class];
    protected $fixedPositionid = [1];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['deptid' => -1, 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            if (!empty($input['deptid']) && $input['deptid'] != -1) {
                $maps[] = ['deptid', '=', $input['deptid']];
            }

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelPosition::field('id,name,deptid')
                ->where($maps)
                ->order('id desc')
                ->limit($offset, $input['rows'])
                ->append(['dept_name', 'permitions'])
                ->select();
            $num = ModelPosition::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 保存新建的资源.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['name', 'deptid'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Position.create');

            $dept = ModelDept::find($input['deptid']);
            if (!$dept) {
                throw new \Exception('部门不存在');
            }
            if (ModelPosition::where($input)->find()) {
                throw new \Exception('岗位已存在');
            }

            ModelPosition::create($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 修改.
     */
    public function update()
    {
        $adapter = ['id', 'name', 'deptid'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Position.update');

            $position = ModelPosition::find($input['id']);
            if (!$position) {
                throw new \Exception('岗位不存在');
            }
            if (in_array($input['id'], $this->fixedPositionid) && $position->deptid != $input['deptid']) {
                throw new \Exception('该岗位只允许修改名称');
            }
            $maps = [
               ['name', '=', $input['name']],
               ['deptid', '=', $input['deptid']],
               ['id', '<>', $position->id],
            ];
            $tmp = ModelPosition::where($maps)->find();
            if ($tmp) {
                throw new \Exception('岗位已存在!');
            }

            ModelPosition::update($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除.
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Position.delete');

            if (in_array($input['id'], $this->fixedPositionid)) {
                throw new \Exception('该岗位不能删除');
            }
            $position = ModelPosition::find($input['id']);
            if (!$position) {
                throw new \Exception('岗位不存在');
            }

            $position->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /**
     * 设置权限.
     */
    public function setPermition()
    {
        $adapter = ['id', 'permitions'];
        $input = $this->request->only($adapter);

        try {
            $position = ModelPosition::find($input['id']);
            if (!$position) {
                throw new \Exception('岗位不存在');
            }

            $data = $this->getPermitionData($input);
            $model = new ModelRolePermition();
            $model->saveAll($data);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }

    /*************************************************************************. */

    /**
     * 数据.
     */
    private function getPermitionData($input)
    {
        $data = [];
        if (!is_array($input['permitions'])) {
            throw new \Exception('权限格式错误');
        }
        foreach ($input['permitions'] as $k => $v) {
            if (empty($v['permition'])) {
                throw new \Exception('缺少权限code');
            }
            if (empty($v['action']) || !in_array($v['action'], ['deny', 'allow'])) {
                throw new \Exception('操作必须填写');
            }

            $tmp = [
                'positionid' => $input['id'],
                'permition' => $v['permition'],
                'action' => $v['action'],
                'note' => $v['note'] ?? '',
            ];
            if (!empty($v['id']) && $v['id'] > 0) {
                $tmp['id'] = $v['id'];
            }
            if ($this->permitionExist($tmp)) {
                throw new \Exception('权限已存在');
            }
            $data[] = $tmp;
        }

        return $data;
    }

    /**
     * 权限是否存在.
     */
    private function permitionExist($data)
    {
        $maps = [
            ['positionid', '=', $data['positionid']],
            ['permition', '=', $data['permition']],
        ];
        if (!empty($data['id'])) {
            $maps[] = ['id', '<>', $data['id']];
        }
        $res = ModelRolePermition::where($maps)->find();
        if ($res) {
            return true;
        }

        return false;
    }
}
