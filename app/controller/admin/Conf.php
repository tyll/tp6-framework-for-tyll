<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\middleware\TokenAuth;
use app\model\Conf as ModelConf;

/**
 * 配置.
 */
class Conf extends BaseController
{
    protected $middleware = [TokenAuth::class];

    /**
     * 获取配置信息.
     */
    public function index()
    {
        $adapter = ['keys'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Conf.index');

            $res = ModelConf::where('k', 'in', $input['keys'])->select();
            $data = [];
            foreach ($res as $v) {
                $value = json_decode($v->v);
                if (is_bool($value) || is_null($value)) {
                    $value = $v->v;
                }
                $data[$v->k] = $value;
            }
        } catch (\Throwable $e) {
            return json(make_return_arr(0, $e->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $data));
    }

    /**
     * 修改配置信息.
     */
    public function update()
    {
        $adapter = ['data'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Conf.update');

            foreach ($input['data'] as $k => $v) {
                ModelConf::where('k', $k)->update(['v' => is_array($v) ? json_encode($v, JSON_UNESCAPED_UNICODE) : $v]);
            }
        } catch (\Throwable $e) {
            return json(make_return_arr(0, '操作失败 '.$e->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }
}
