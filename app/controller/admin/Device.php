<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\common\TencentCos;
use app\common\WeChatApplet;
use app\middleware\TokenAuth;
use app\model\Device as ModelDevice;

class Device extends BaseController
{
    protected $middleware = [TokenAuth::class => ['except' => ['init']]];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['type' => 0, 'name', 'sn', 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Device.index');

            $maps = [];
            if ($input['type'] != 0) {
                $maps[] = ['type', '=', $input['type']];
            }
            if (isset($input['name'])) {
                $maps[] = ['name', 'like', '%'.$input['name'].'%'];
            }
            if (isset($input['sn'])) {
                $maps[] = ['sn', 'like', '%'.$input['sn'].'%'];
            }

            $list = ModelDevice::where($maps)
                ->order('id desc')
                ->page($input['page'], $input['rows'])
                ->append(['type_zw'])
                ->select();
            $num = ModelDevice::where($maps)->select()->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', ['list' => $list, 'total' => $num]));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['type', 'name', 'model', 'sn'];
        $input = $this->request->only($adapter);

        try {
            ModelDevice::create($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '添加成功'));
    }

    /**
     * 保存更新的资源.
     *
     * @return \think\Response
     */
    public function update()
    {
        $adapter = ['id', 'type', 'name', 'model', 'sn'];
        $input = $this->request->only($adapter);

        try {
            ModelDevice::where('id', $input['id'])->update($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除指定资源.
     *
     * @return \think\Response
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Device.delete');
            $device = ModelDevice::where('id', $input['id'])->find();
            if (!$device) {
                throw new \Exception('设备不存在', 0);
            }
            $device->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /**
     * 获取小程序码
     */
    public function getAppletCode()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $device = ModelDevice::find($input['id']);
            if (!$device) {
                throw new \Exception('设备不存在');
            }
            $device = $this->saveAppletCode($device);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $device));
    }

    /**********************************************************************. */

    /**
     * 获取并保存小程序码
     *
     * @param ModelDevice $device 设备
     */
    private function saveAppletCode(ModelDevice $device)
    {
        if (empty($device->applet_code)) {
            //获取小程序码buffer
            $data = [
                'page' => '',
                'target' => 'device',
                'pk' => $device->id,
            ];
            $scene = http_build_query($data);
            $res = WeChatApplet::getInstance()->getAppletCodeUnlimit($scene);

            //保存到本地
            $path = 'downloads/'.$device->name.'.jpg';
            file_put_contents($path, $res);

            //上传对象存储
            $cos_info = TencentCos::getInstance()->upload($path, '/console/device'.$device->id.date('YmdHis').'.jpg');

            //保存地址
            $device->applet_code = $cos_info['url'];
            $device->save();

            //@unlink($path);
        }

        return $device;
    }
}
