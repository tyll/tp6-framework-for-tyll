<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\middleware\TokenAuth;
use app\model\Admin as ModelAdmin;
use app\model\Dept as ModelDept;
use app\model\Position as ModelPosition;
use app\model\Tongxunlu as ModelTongxunlu;
use app\model\User as ModelUser;

class User extends BaseController
{
    protected $middleware = [TokenAuth::class];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['name', 'tel', 'deptid', 'page' => 1, 'rows' => 10];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            if (!empty($input['name'])) {
                $maps[] = ['name', 'like', '%'.$input['name'].'%'];
            }
            if (!empty($input['tel'])) {
                $maps[] = ['tel', 'like', '%'.$input['tel'].'%'];
            }
            if (!empty($input['deptid']) && $input['deptid'] != -1) {
                $userids = ModelTongxunlu::where('deptid', $input['deptid'])->column('userid');
                $maps[] = ['id', 'in', $userids];
            }
            $maps[] = ['tel', '<>', 'root'];

            $list = ModelUser::where($maps)
                ->order('id desc')
                ->page($input['page'], $input['rows'])
                ->append(['rz_status', 'headimgurl', 'dept_name', 'position_name', 'rz_status_zw'])
                ->hidden(['c_time', 'delete_time', 'tongxunlu'])
                ->select();

            foreach ($list as $k => $v) {
                $v->is_admin = ModelAdmin::where('userid', $v->id)->where('deptid', $input['deptid'] ?? 0)->find() ? 1 : 0;
                $v->deptid = $v->tongxunlu->deptid ?? 0;
                $v->positionid = $v->tongxunlu->positionid ?? 0;
            }
            $num = ModelUser::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'list' => $list,
            'total' => $num,
        ]));
    }

    /**
     * 管理员列表.
     */
    public function getAdminList()
    {
        $adapter = ['user_name', 'deptid', 'page' => 1, 'rows' => 10];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            if (!empty($input['user_name'])) {
                $userids = ModelUser::where('name', 'like', '%'.$input['user_name'].'%')->column('id');
                $maps[] = ['userid', 'in', $userids];
            }
            if (!empty($input['deptid']) && $input['deptid'] != -1) {
                $maps[] = ['deptid', '=', $input['deptid']];
            }

            $list = ModelAdmin::where($maps)
                ->order('id desc')
                ->page($input['page'], $input['rows'])
                ->append([])
                ->hidden(['user'])
                ->select();

            foreach ($list as $k => $v) {
                $v->user_name = $v->user->name ?? '';
                $v->tel = $v->user->tel ?? '';
                $v->headimgurl = $v->user->headimgurl ?? '';
                $v->position_name = $v->user->position_name ?? '';
                $v->user_dept_name = $v->user->dept_name ?? '';
                $v->rz_status_zw = $v->user->rz_status_zw ?? '';
            }
            $num = ModelAdmin::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 新增.
     */
    public function create()
    {
        $adapter = ['name', 'tel', 'deptid', 'positionid', 'gonghao'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'User.ruzhi');
            $this->validateParams($input);
            if (ModelUser::where('tel', $input['tel'])->find()) {
                throw new \Exception('电话号码已存在');
            }
            if (ModelUser::where('gonghao', $input['gonghao'])->find()) {
                throw new \Exception('工号已存在');
            }

            $input['password'] = md5('123456');
            $user = ModelUser::create($input);

            //自动入职
            $this->saveRuzhi($user, $input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 修改.
     */
    public function update()
    {
        $adapter = ['id', 'name', 'tel', 'deptid', 'positionid', 'gonghao'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'User.update');
            $this->validateParams($input);
            $user = ModelUser::find($input['id']);
            if (!$user) {
                throw new \Exception('人员不存在');
            }

            $maps = [
                ['tel', '=', $input['tel']],
                ['id', '<>', $user->id],
            ];
            $tmp = ModelUser::where($maps)->find();
            if ($tmp) {
                throw new \Exception('电话号码已存在!');
            }
            $maps = [
                ['gonghao', '=', $input['gonghao']],
                ['id', '<>', $user->id],
            ];
            $tmp = ModelUser::where($maps)->find();
            if ($tmp) {
                throw new \Exception('工号已存在!');
            }

            \think\facade\Db::startTrans();

            ModelUser::update($input);

            //保存通讯录
            $this->saveTongxunlu($user, $input);

            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除.
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'User.delete');

            $user = ModelUser::find($input['id']);
            if (!$user) {
                throw new \Exception('人员不存在');
            }

            $user->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /**
     * 设置管理员.
     */
    public function setAdmin()
    {
        $adapter = ['id', 'deptid'];
        $input = $this->request->only($adapter);

        try {
            $res = $this->getUserAdmin($input);

            $admin = $res['admin'];
            if ($admin) {
                throw new \Exception("'{$res['user']->name}'已经是'{$res['dept']->name}'的管理员");
            }

            ModelAdmin::create($res['data']);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }

    /**
     * 管理员解绑.
     */
    public function unbindAdmin()
    {
        $adapter = ['id', 'deptid'];
        $input = $this->request->only($adapter);

        try {
            $res = $this->getUserAdmin($input);
            $admin = $res['admin'];
            if (!$admin) {
                throw new \Exception("'{$res['user']->name}'不是'{$res['dept']->name}'的管理员");
            }

            $admin->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }

    /*************************************************************************************. */

    /**
     * 管理员关联关系.
     */
    private function getUserAdmin($input)
    {
        $this->validate($input, 'User.set_admin');
        $user = ModelUser::find($input['id']);
        if (!$user) {
            throw new \Exception('人员不存在');
        }
        $dept = ModelDept::find($input['deptid']);
        if (!$dept) {
            throw new \Exception('部门不存在');
        }

        $data = [
            'userid' => $input['id'],
            'deptid' => $input['deptid'],
        ];
        $admin = ModelAdmin::where($data)->find();

        return ['admin' => $admin, 'user' => $user, 'dept' => $dept, 'data' => $data];
    }

    /**
     * 保存通讯录.
     */
    public function saveTongxunlu($user, $input)
    {
        $t_data = [
            'userid' => $user->id,
            'deptid' => $input['deptid'],
            'positionid' => $input['positionid'],
        ];

        if ($user->tongxunlu) {
            $t_data['id'] = $user->tongxunlu->id;
            ModelTongxunlu::update($t_data);
        } else {
            ModelTongxunlu::create($t_data);
        }
    }

    /**
     * 验证新增参数.
     */
    private function validateParams($input)
    {
        $dept = ModelDept::find($input['deptid']);
        if (!$dept) {
            throw new \Exception('部门不存在');
        }
        $position = ModelPosition::find($input['positionid']);
        if (!$position) {
            throw new \Exception('岗位不存在');
        }
        //可选顶级和该部门下的岗位
        if ($position->deptid != 1 && $position->deptid != $dept->id) {
            throw new \Exception('岗位选择错误');
        }

        return true;
    }

    /**
     * 入职.
     */
    private function saveRuzhi(ModelUser $user, $input)
    {
        $data = [
            'userid' => $user->id,
            'deptid' => $input['deptid'],
            'positionid' => $input['positionid'],
            'status' => 1,
            'rz_time' => date('Y-m-d H:i:s'),
            'lz_time' => '0000-00-00 00:00:00',
        ];

        if ($user->tongxunlu) {
            $data['id'] = $user->tongxunlu->id;
            ModelTongxunlu::update($data);
        } else {
            ModelTongxunlu::create($data);
        }
    }
}
