<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\Tag as ModelTag;

/**
 * 标签.
 */
class Tag extends Base
{
    protected $middleware = [TokenAuth::class];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [];

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelTag::field('id,name')
                ->where($maps)
                ->order('id desc')
                ->limit($offset, $input['rows'])
                ->append([])
                ->select();
            $num = ModelTag::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 保存新建的资源.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['name'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Tag.create');

            if (ModelTag::where($input)->find()) {
                throw new \Exception('标签已存在');
            }

            ModelTag::create($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 修改.
     */
    public function update()
    {
        $adapter = ['id', 'name'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Tag.update');

            $tag = ModelTag::find($input['id']);
            if (!$tag) {
                throw new \Exception('标签不存在');
            }
            $maps = [
               ['name', '=', $input['name']],
               ['id', '<>', $tag->id],
            ];
            $tmp = ModelTag::where($maps)->find();
            if ($tmp) {
                throw new \Exception('标签已存在!');
            }

            ModelTag::update($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除.
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Position.delete');

            $tag = ModelTag::find($input['id']);
            if (!$tag) {
                throw new \Exception('岗位不存在');
            }

            $tag->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /*************************************************************************. */
}
