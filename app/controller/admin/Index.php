<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\middleware\TokenAuth;
use app\model\Admin as ModelAdmin;
use Exception;

class Index extends BaseController
{
    protected $middleware = [TokenAuth::class => ['except' => ['login']]];

    /**
     * 登录.
     *
     * @return \think\Response
     */
    public function login()
    {
        $adapter = ['username', 'password'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Login.admin_login');

            $input['password'] = md5($input['password']);
            $admin = ModelAdmin::where($input)->find();
            if (!$admin) {
                throw new Exception('账号或密码错误!');
            }

            $th = new \app\common\TyToken();
            $th->setTokenid($admin->id);
            $token = $th->getToken();
        } catch (\Throwable $e) {
            return json(make_return_arr(0, $e->getMessage()));
        }

        return json(make_return_arr(1, '登录成功', ['token' => $token]));
    }

    /**
     * 账户信息.
     */
    public function getAdminInfo()
    {
        try {
            $admin = ModelAdmin::field(['id', 'username', 'realname'])->where('id', $this->request->tokenid)->find();
        } catch (\Throwable $e) {
            return json(make_return_arr(0, $e->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $admin));
    }

    /**
     * 修改密码
     */
    public function updatePwd()
    {
        $adapter = ['old_password', 'new_password', 'confirm_password'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Index.updatePwd');

            $admin = ModelAdmin::where('id', $this->request->tokenid)->find();
            if ($admin->password != md5($input['old_password'])) {
                throw new Exception('旧密码错误!');
            }

            $admin->password = md5($input['new_password']);
            $admin->save();
        } catch (\Throwable $e) {
            return json(make_return_arr(0, $e->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }
}
