<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\middleware\TokenAuth;
use app\model\Dict as ModelDict;
use think\facade\Config;

class Dict extends BaseController
{
    protected $middleware = [TokenAuth::class];

    /**
     * 付款计划状态
     */
    public function getPayscheduleStatus()
    {
        return $this->configInfo('pay_schedule_status');
    }

    /**
     * 获取dict表.
     */
    protected function dictInfo($table, $field)
    {
        try {
            $maps = [
                'table' => $table,
                'field' => $field,
            ];
            $data = ModelDict::where($maps)->field(['key', 'value'])->select();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $data));
    }

    /**
     * config dict.
     */
    protected function configInfo($key)
    {
        try {
            $type = Config::get('tyll.dict.'.$key);
            $data = [];
            foreach ($type as $k => $v) {
                $data[] = ['key' => $k, 'value' => $v];
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $data));
    }
}
