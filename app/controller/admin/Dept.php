<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\Dept as ModelDept;

/**
 * 部门.
 */
class Dept extends Base
{
    protected $middleware = [TokenAuth::class];

    protected $fixedDeptid = [1];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['parentid' => 0, 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            if ($input['parentid'] != -1) {
                $maps[] = ['parentid', '=', $input['parentid']];
            }

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelDept::field('id,name,parentid')
                ->where($maps)
                ->order('id desc')
                ->limit($offset, $input['rows'])
                ->append(['children', 'parent_name'])
                ->select();
            $num = ModelDept::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 所有部门.
     */
    public function getTotalList()
    {
        try {
            $list = ModelDept::field('id,name')->select();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $list));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['name', 'parentid' => 0];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Dept.create');

            if (ModelDept::where($input)->find()) {
                throw new \Exception('部门已存在');
            }

            ModelDept::create($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 保存更新的资源.
     *
     * @return \think\Response
     */
    public function update()
    {
        $adapter = ['id', 'name', 'parentid' => 0];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Dept.update');

            $dept = ModelDept::find($input['id']);
            if (!$dept) {
                throw new \Exception('部门不存在');
            }
            if (in_array($input['id'], $this->fixedDeptid) && $dept->parentid != $input['parentid']) {
                throw new \Exception('该部门只允许修改名称');
            }

            $maps = [
                ['name', '=', $input['name']],
                ['parentid', '=', $input['parentid']],
                ['id', '<>', $input['id']],
            ];
            $tmp = ModelDept::where($maps)->find();
            if ($tmp) {
                throw new \Exception('部门已存在!');
            }

            ModelDept::update($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除指定资源.
     *
     * @return \think\Response
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Dept.delete');

            if (in_array($input['id'], $this->fixedDeptid)) {
                throw new \Exception('该部门不允许删除');
            }
            $dept = ModelDept::find($input['id']);
            if (!$dept) {
                throw new \Exception('部门不存在');
            }

            $dept->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }
}
