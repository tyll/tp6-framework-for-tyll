<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\User as ModelUser;

/**
 * 登录api控制器.
 *
 * @Author: godliu
 * @Date: 2022-03-26 10:00:00
 */
class Login extends Base
{
    protected $middleware = [
        TokenAuth::class => ['only' => ['updatePwd']],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录.
     */
    public function login()
    {
        $params = $this->request->param();

        try {
            // 检查登录规则
            $r = $this->checkLoginRule($params);
            $u_detail = $r['data']['u_detail'];

            $tk = new \app\common\TyToken();
            $tk->setTokenid($u_detail['id']);
            $tk->setDay(1);
            $token = $tk->getToken();
        } catch (\Throwable $th) {
            return json(make_return_arr($th->getCode(), $th->getMessage()));
        }
        $data['token'] = $token;

        return json(make_return_arr(1, '登录成功', $data));
    }

    /**
     * 修改密码
     */
    public function updatePwd()
    {
        $params = $this->request->param();
        $userid = $this->request->tokenid;

        try {
            $params['userid'] = $userid;

            // 检查修改密码规则
            $r = $this->checkUpdatePwdRule($params);
            $u_detail = $r['data']['u_detail'];

            $a_data = [];
            $a_data['password'] = md5(trim($params['new_password']));
            $maps = [];
            $maps[] = ['id', '=', $u_detail['id']];
            $r = ModelUser::update($a_data, $maps);
        } catch (\Throwable $th) {
            return json(make_return_arr($th->getCode(), $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**************************************************逻辑函数**************************************************/

    /**
     * 检查登录规则.
     */
    private function checkLoginRule($params)
    {
        if (empty($params['username'])) {
            throw new \Exception('用户名必须填写', 0);
        }
        if (empty($params['password'])) {
            throw new \Exception('密码必须填写', 0);
        }
        $maps = [];
        $maps[] = ['tel', '=', $params['username']];
        $u_detail = ModelUser::getDetail($maps, 'id,name,password');
        if (empty($u_detail)) {
            throw new \Exception('该用户不存在', 0);
        }
        $password = md5($params['password']);
        if ($u_detail['password'] != $password) {
            throw new \Exception('密码错误', 0);
        }
        $data['u_detail'] = $u_detail;

        return make_return_arr(1, 'OK', $data);
    }

    /**
     * 检查修改密码规则.
     */
    private function checkUpdatePwdRule($params)
    {
        if (empty($params['new_password'])) {
            throw new \Exception('新密码必须填写', 0);
        }
        if (empty($params['sure_password'])) {
            throw new \Exception('确认新密码必须填写', 0);
        }
        if ($params['new_password'] != $params['sure_password']) {
            throw new \Exception('两次密码不一致', 0);
        }
        $maps = [];
        $maps[] = ['id', '=', $params['userid']];
        $u_detail = ModelUser::getDetail($maps, 'id');
        if (empty($u_detail)) {
            throw new \Exception('该用户不存在', 0);
        }
        $data['u_detail'] = $u_detail;

        return make_return_arr(1, 'OK', $data);
    }
}
