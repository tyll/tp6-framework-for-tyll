<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\CheckItem as ModelCheckItem;
use app\model\CheckSchedule as ModelCheckSchedule;
use app\model\CheckTargetSchedule as ModelCheckTargetSchedule;
use app\model\CheckTask as ModelCheckTask;
use app\model\CheckTaskGroup as ModelCheckTaskGroup;
use app\model\RItemSchedule as ModelRItemSchedule;
use app\obj\CheckSchedule as ObjCheckSchedule;

/**
 * 检查计划.
 */
class CheckSchedule extends Base
{
    protected $middleware = [TokenAuth::class];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['page' => 1, 'rows' => 10];
        $input = $this->request->only($adapter);

        try {
            $maps = [];

            $append = [
                'canyu_user_names', 'status_zw', 'check_itemids', 'enable_zw', 'canyu_users', 'path_zw', 'time_arr', 'type_zw',
            ];
            $list = ModelCheckSchedule::where($maps)
                ->order('id desc')
                ->page($input['page'], $input['rows'])
                ->append($append)
                ->hidden([])
                ->select();

            $num = ModelCheckSchedule::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['title', 'canyu_userids', 'type', 'check_itemids', 'period', 'time_arr'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckSchedule.create');

            if (ModelCheckSchedule::where('title', $input['title'])->find()) {
                throw new \Exception('计划标题已存在');
            }

            \think\facade\Db::startTrans();

            $input['status'] = 1;
            $input['enable'] = 0;
            $schedule = ModelCheckSchedule::create($input);
            $schedule_obj = ObjCheckSchedule::getInstance();

            //保存关联表
            $this->saveRCheckItem($schedule, $input['check_itemids']);
            // $schedule_obj->saveRCheckItem($schedule, $input['check_itemids']);

            //计算path
            $path = $this->getPath($schedule);
            // $path = $schedule_obj->getPath($schedule);
            $schedule->path = $path;
            $schedule->save();

            //保存目标计划表
            $this->saveTargetSchedule($schedule);
            // $schedule_obj->saveRCheckItem($schedule, $input['check_itemids']);

            //保存时间段
            $schedule_obj->saveTimes($schedule, $input['time_arr'], $input['period']);

            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 保存更新的资源.
     *
     * @return \think\Response
     */
    public function update()
    {
        $adapter = ['id', 'title', 'canyu_userids', 'type', 'check_itemids', 'period', 'time_arr'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckSchedule.update');

            $schedule = ModelCheckSchedule::find($input['id']);
            if (!$schedule) {
                throw new \Exception('检查计划不存在');
            }
            if ($schedule->type != $input['type']) {
                throw new \Exception('类型不能修改');
            }
            if ($schedule->period != $input['period']) {
                throw new \Exception('周期不能修改');
            }
            $maps = [
                ['title', '=', $input['title']],
                ['id', '<>', $schedule->id],
            ];
            $tmp = ModelCheckSchedule::where($maps)->find();
            if ($tmp) {
                throw new \Exception('标题已存在!');
            }

            \think\facade\Db::startTrans();

            ModelCheckSchedule::update($input);
            $schedule_obj = ObjCheckSchedule::getInstance();

            $old_ritemids = $this->saveRCheckItem($schedule, $input['check_itemids']);

            //计算path
            $path = $this->getPath($schedule);
            $schedule->path = $path;
            $schedule->save();

            //保存目标计划表
            $this->saveTargetSchedule($schedule);

            //保存时间段
            $schedule_obj->saveTimes($schedule, $input['time_arr'], $input['period']);

            //更新任务组
            $this->updateTaskGroup($schedule, $input, $old_ritemids);

            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除指定资源.
     *
     * @return \think\Response
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckSchedule.delete');

            $schedule = ModelCheckSchedule::find($input['id']);
            if (!$schedule) {
                throw new \Exception('检查计划不存在');
            }

            $schedule->delete();
            $schedule->rItem->delete();
            $schedule->times->delete();
            $schedule->targetSchedule->delete();

            //删除已生成且未开始的任务组及任务
            $task_groups = $schedule->taskGroup()->where('status', 0)->select();
            foreach ($task_groups as $k => $v) {
                $v->task->delete();
                $v->taskTargetGroup->delete();
                $v->delete();
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /**
     * 启用禁用.
     */
    public function enable()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckSchedule.enable');

            $schedule = ModelCheckSchedule::find($input['id']);
            if (!$schedule) {
                throw new \Exception('检查计划不存在');
            }
            $schedule->enable = !$schedule->enable;
            $schedule->save();

            //修改目标计划
            if ($schedule->targetSchedule) {
                $schedule->targetSchedule->update(['enable' => $schedule->enable]);
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }

    /***************************************************************************************. */

    /**
     * 更新任务组.
     */
    private function updateTaskGroup(ModelCheckSchedule $schedule, array $input, $old_ritemids)
    {
        $task_groups = ModelCheckTaskGroup::where('check_scheduleid', $schedule->id)->where('status', '<>', 1)->select();

        //更新标题和参与人
        $data = [
            'title' => $input['title'],
        ];
        //类型为共同参与才修改参与人
        if ($schedule->type == 2) {
            $data['canyu_userids'] = json_encode($input['canyu_userids']);
        }
        $task_groups->update($data);

        //更新检查任务(删除未开始的任务组的任务并根据新的重新生成)
        $new_ritemids = array_column($input['check_itemids'], 'id');
        if ($old_ritemids != $new_ritemids) {
            $task_groups = $task_groups->where('status', 0);
            ModelCheckTask::where('task_groupid', 'in', $task_groups->column('id'))->select()->delete();

            foreach ($task_groups as $k => $v) {
                $this->saveTask($v, $schedule);
            }
        }
    }

    /**
     * 保存任务
     */
    private function saveTask($group, $schedule)
    {
        $itemids = ModelRItemSchedule::where('check_scheduleid', $schedule->id)
            ->order('sort asc,id desc')
            ->column('check_itemid');

        $data = [];
        foreach ($itemids as $k => $v) {
            $tmp = [
                'task_groupid' => $group->id,
                'check_itemid' => $v,
                'canyu_userids' => $group->canyu_userids,
                // 'date' => $group->date,
                'expect_start_time' => $group->expect_start_time,
                'expect_start_timestamp' => strtotime($group->expect_start_time),
                'expect_end_time' => $group->expect_end_time,
                'fujian' => '[]',
            ];
            $data[] = $tmp;
        }

        $model = new ModelCheckTask();
        $model->saveAll($data);

        return true;
    }

    /**
     * 保存目标计划.
     */
    private function saveTargetSchedule($schedule)
    {
        $schedule->targetSchedule->delete();

        $itemids = $schedule->ritem()->order('sort asc,id desc')->column('check_itemid');
        $items = ModelCheckItem::where('id', 'in', $itemids)->group('target,pk')->select();

        foreach ($items as $k => $v) {
            $tmp = [
                'check_scheduleid' => $schedule->id,
                'target' => $v['target'],
                'targetid' => $v['pk'],
                'enable' => $schedule->enable ?? 1,
            ];
            //查找软删除数据
            $target_schedule = ModelCheckTargetSchedule::withTrashed()
                ->where(['check_scheduleid' => $schedule->id, 'target' => $v['target'], 'targetid' => $v['pk']])
                ->find();
            //存在则恢复并修改
            if ($target_schedule) {
                $target_schedule->restore();
                $tmp['id'] = $target_schedule->id;
                ModelCheckTargetSchedule::update($tmp);
            } else {//不存在则新增
                ModelCheckTargetSchedule::create($tmp);
            }
        }

        return true;
    }

    /**
     * 获取路径.
     */
    private function getPath(ModelCheckSchedule $schedule)
    {
        $itemids = $schedule->rItem()->order('sort asc,id desc')->column('check_itemid');

        $areaids = [];
        foreach ($itemids as $k => $v) {
            $item = ModelCheckItem::find($v);
            if ($item && $item->target == 'device' && $item->target_model && !in_array($item->target_model->areaid, $areaids)) {
                $areaids[] = $item->target_model->areaid;
            }
        }

        return implode('-', $areaids);
    }

    /**
     * 保存关联表.
     */
    private function saveRCheckItem(ModelCheckSchedule $check_schedule, array $check_itemids)
    {
        $old_ritemids = ModelRItemSchedule::where('check_scheduleid', $check_schedule->id)->column('id');
        $new_ritemids = array_column($check_itemids, 'id');
        $delete_ritemids = array_values(array_diff($old_ritemids, $new_ritemids));

        $data = [];
        foreach ($check_itemids as $k => $v) {
            if (empty($v['check_itemid'])) {
                throw new \Exception('检查项id不能为空');
            }
            if (!isset($v['sort'])) {
                throw new \Exception('sort不能为空');
            }

            $tmp = [
                'check_scheduleid' => $check_schedule->id,
                'check_itemid' => $v['check_itemid'],
                'sort' => $v['sort'],
            ];
            if (!empty($v['id']) && $v['id']) {
                $tmp['id'] = $v['id'];
            }
            $data[] = $tmp;
        }

        //新增或者修改
        $model = new ModelRItemSchedule();
        $model->saveAll($data);

        //删除
        ModelRItemSchedule::destroy($delete_ritemids);

        return $old_ritemids;
    }
}
