<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\DeviceType as ModelDeviceType;

/**
 * 设备类型.
 */
class DeviceType extends Base
{
    protected $middleware = [TokenAuth::class];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [];

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelDeviceType::field('id,name')
                ->where($maps)
                ->order('sort asc,id desc')
                ->limit($offset, $input['rows'])
                ->append([])
                ->hidden([])
                ->select();
            $num = ModelDeviceType::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 保存新建的资源.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = ['name', 'sort'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'DeviceType.create');

            if (ModelDeviceType::where('name', $input['name'])->find()) {
                throw new \Exception('设备类型已存在');
            }

            ModelDeviceType::create($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 保存更新的资源.
     *
     * @return \think\Response
     */
    public function update()
    {
        $adapter = ['id', 'name', 'sort'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'DeviceType.update');

            $type = ModelDeviceType::find($input['id']);
            if (!$type) {
                throw new \Exception('设备类型不存在');
            }
            $maps = [
                ['name', '=', $input['name']],
                ['id', '<>', $type->id],
            ];
            $tmp = ModelDeviceType::where($maps)->find();
            if ($tmp) {
                throw new \Exception('设备类型已存在!');
            }

            ModelDeviceType::update($input);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除指定资源.
     *
     * @return \think\Response
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'DeviceType.delete');

            $type = ModelDeviceType::find($input['id']);
            if (!$type) {
                throw new \Exception('设备类型不存在');
            }

            $type->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }
}
