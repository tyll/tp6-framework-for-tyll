<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\middleware\TokenAuth;
use app\model\CheckItem as ModelCheckItem;
use app\model\CheckItemOption as ModelCheckItemOption;
use app\model\Device as ModelDevice;

/**
 * 检查项.
 */
class CheckItem extends Base
{
    protected $middleware = [TokenAuth::class];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        $adapter = ['target', 'pk', 'areaid', 'jianchazhouqi', 'tag', 'page' => 1, 'rows' => 10];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            if (!empty($input['target'])) {
                $maps[] = ['target', '=', $input['target']];
            }
            if (!empty($input['pk']) && $input['pk'] > 0 && !empty($input['target'])) {
                $maps[] = ['pk', '=', $input['pk']];
            }
            if (!empty($input['areaid']) && $input['areaid'] != -1) {
                $deviceids = ModelDevice::where('areaid', $input['areaid'])->column('id');
                $maps[] = ['target', '=', 'device'];
                $maps[] = ['pk', 'in', $deviceids];
            }
            if (!empty($input['jianchazhouqi'])) {
                $jczq_info = $this->getNumAndStr($input['jianchazhouqi']);
                if ($jczq_info['number'] > 0) {
                    $maps[] = ['jianchazhouqi', '=', $jczq_info['number']];
                }
                if (!empty($jczq_info['string'])) {
                    $maps[] = ['jianchazhouqi_unit', '=', $jczq_info['string']];
                }
            }
            if (!empty($input['tag'])) {
                $maps[] = ['tag', 'like', '%'.$input['tag'].'%'];
            }

            $list = ModelCheckItem::where($maps)
                ->order('id desc')
                ->page($input['page'], $input['rows'])
                ->with(['option'])
                ->visible(['option' => ['id', 'content', 'health']])
                ->append(['target_zw', 'type_zw', 'pk_name', 'tag_arr', 'area_name'])
                ->hidden([])
                ->select();
            $num = ModelCheckItem::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 获取字符串的数字及字符串.
     */
    protected function getNumAndStr(string $str)
    {
        $number = '';
        $string = '';
        $length = mb_strlen($str);
        for ($i = 0; $i < $length; ++$i) {
            $tmp = mb_substr($str, $i, 1);
            if (is_numeric($tmp)) {
                $number .= $tmp;
            } elseif (is_string($tmp)) {
                $string .= $tmp;
            }
        }

        return ['number' => (int) $number, 'string' => $string];
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $adapter = [
            'target', 'pk', 'type' => 'radio', 'content', 'option', 'unit' => '', 'jianchazhouqi',
            'jianchazhouqi_unit', 'tag_arr' => [],
        ];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckItem.create');
            \think\facade\Db::startTrans();
            $input['tag'] = implode(',', $input['tag_arr']);
            $item = ModelCheckItem::create($input);

            $input['unit'] = $input['type'] == 'number' ? $input['unit'] : '';
            $this->saveOption($item, $input['option']);
            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '新增成功'));
    }

    /**
     * 保存更新的资源.
     *
     * @return \think\Response
     */
    public function update()
    {
        $adapter = [
            'id', 'target', 'pk', 'type' => 'radio', 'content', 'option', 'unit' => '',  'jianchazhouqi',
            'jianchazhouqi_unit', 'tag_arr' => [],
        ];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckItem.update');

            $item = ModelCheckItem::find($input['id']);
            if (!$item) {
                throw new \Exception('检查项不存在');
            }
            \think\facade\Db::startTrans();

            $input['unit'] = $input['type'] == 'number' ? $input['unit'] : '';
            $input['tag'] = implode(',', $input['tag_arr']);
            ModelCheckItem::update($input);
            $item = ModelCheckItem::find($item->id);

            $this->saveOption($item, $input['option']);

            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '修改成功'));
    }

    /**
     * 删除指定资源.
     *
     * @return \think\Response
     */
    public function delete()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'CheckItem.delete');

            $item = ModelCheckItem::find($input['id']);
            if (!$item) {
                throw new \Exception('检查项不存在');
            }

            $item->delete();
            $item->option->delete();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '删除成功'));
    }

    /**
     * 保存选项.
     */
    private function saveOption(ModelCheckItem $check_item, $option)
    {
        if ($check_item->type != 'number' && empty($option)) {
            throw new \Exception('选项必须填写');
        }

        $old_optionids = $check_item->option()->column('id');
        $new_optionids = array_column($option, 'id');
        $delete_optionids = array_values(array_diff($old_optionids, $new_optionids));

        $data = [];
        foreach ($option as $k => $v) {
            if (empty($v['content'])) {
                throw new \Exception('选项内容不能为空');
            }

            $tmp = [
                'check_itemid' => $check_item->id,
                'content' => $v['content'],
                'health' => $v['health'],
            ];
            if (!empty($v['id']) && $v['id']) {
                $tmp['id'] = $v['id'];
            }
            $data[] = $tmp;
        }

        //新增或者修改
        $model = new ModelCheckItemOption();
        $model->saveAll($data);

        //删除
        ModelCheckItemOption::destroy($delete_optionids);
    }
}
