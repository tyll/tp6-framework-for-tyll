<?php

declare(strict_types=1);

namespace app\controller\admin;

use app\BaseController;
use app\model\Conf as ModelConf;

/**
 * 基础api控制器.
 *
 * @Author: godliu
 * @Date: 2022-03-26 10:00:00
 */
class Base extends BaseController
{
    public function __construct()
    {
        parent::__construct(app());
    }

    /**
     * 获取上传图片签名.
     */
    public function getUploadPicSign()
    {
        $params = $this->request->param();

        $maps = [];
        $maps[] = ['key', 'in', ['tencent_secret_id', 'tencent_secret_key']];
        $conf_info = ModelConf::getConfInfo();

        $sign = get_authorization($params['method'], $params['pathname'], $conf_info['tencent_secret_id'], $conf_info['tencent_secret_key']);

        return $sign;
    }
}
