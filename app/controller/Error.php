<?php

declare(strict_types=1);

namespace app\controller;

class Error
{
    public function __call($method, $args)
    {
        return json(make_return_arr(0, 'resource not found'));
    }
}
