<?php

declare(strict_types=1);

namespace app\controller\index;

/**
 * 字典表api控制器.
 *
 * @Author: godliu
 * @Date: 2021-04-01 10:00:00
 */
class Dict extends Base
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取入住合同字典.
     */
    public function getContractRz()
    {
        $list = \think\facade\Config::get('tyll.dict.contract_ruzhu_type');
        $ruzhu_type_list = $temp_arr = [];
        foreach ($list as $k => $v) {
            $temp_arr['id'] = $k;
            $temp_arr['name'] = $v;
            $ruzhu_type_list[] = $temp_arr;
        }
        $list = \think\facade\Config::get('tyll.dict.contract_ruzhu_nsr_type');
        $ruzhu_nsr_type_list = $temp_arr = [];
        foreach ($list as $k => $v) {
            $temp_arr['id'] = $k;
            $temp_arr['name'] = $v;
            $ruzhu_nsr_type_list[] = $temp_arr;
        }
        $list = \think\facade\Config::get('tyll.dict.contract_ruzhu_pay_jiezou');
        $ruzhu_pay_jiezou_list = $temp_arr = [];
        foreach ($list as $k => $v) {
            $temp_arr['id'] = $k;
            $temp_arr['name'] = $v;
            $ruzhu_pay_jiezou_list[] = $temp_arr;
        }
        $data['ruzhu_type_list'] = $ruzhu_type_list;
        $data['ruzhu_nsr_type_list'] = $ruzhu_nsr_type_list;
        $data['ruzhu_pay_jiezou_list'] = $ruzhu_pay_jiezou_list;

        return json(make_return_arr(1, 'OK', $data));
    }
}
