<?php

declare(strict_types=1);

namespace app\controller\index;

use app\common\WeChatApplet;
use app\middleware\TokenAuth;
use app\model\Conf as ModelConf;
use app\model\User as ModelUser;
use app\model\Visitor as ModelVisitor;
use think\exception\ValidateException;

/**
 * 小程序.
 */
class Wechat extends Base
{
    protected $middleware = [
        TokenAuth::class => ['except' => ['openid', 'index']],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录.
     */
    public function openid()
    {
        $params = $this->request->param();

        try {
            // 检查登录规则
            $r = $this->checkOpenidRule($params);
            $code = $r['data']['code'];

            // 获取openid
            $r = $this->getOpenid($code);
            $openid = $r['data']['openid'];
            $session_key = $r['data']['session_key'];

            // 获取token
            $tt = new \app\common\TyToken();
            $tt->setTokenid($openid);
            $tt->setDay(1);
            $token = $tt->getToken();

            //初始化微信用户信息
            $this->createVisitor($openid);
            //初始化信用户信息
            //$this->createUser($openid);
        } catch (\Throwable $th) {
            return json(make_return_arr($th->getCode(), $th->getMessage()));
        }

        $data['token'] = $token;
        $data['openid'] = $openid;
        $data['session_key'] = $session_key;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 保存信息.
     */
    public function saveInfo()
    {
        $params = $this->request->param();
        $openid = $this->request->tokenid;

        try {
            // 保存微信用户
            $r = $this->saveVisitor($openid, $params);

            // 保存用户
            //$r = $this->saveUser($openid);
        } catch (\Throwable $th) {
            return json(make_return_arr($th->getCode(), $th->getMessage()));
        }

        return json(make_return_arr(1, 'OK'));
    }

    /**
     * 获取电话号码
     */
    public function getPhoneNumber()
    {
        $adapter = ['code'];
        $input = $this->request->only($adapter);

        try {
            $openid = $this->request->tokenid;

            $phone_info = WeChatApplet::getInstance()->getPhoneNumber($input['code']);

            if (empty($phone_info->purePhoneNumber)) {
                throw new \Exception('获取手机号失败,请重新获取');
            }
            $tel = $phone_info->purePhoneNumber;

            // $user = ModelUser::where('tel', $phone_info->purePhoneNumber)->find();
            // if ($user) {
            //     $user->openid = $openid;
            //     $user->save();
            // } else {
            //     $data = [
            //         'openid' => $openid,
            //         'tel' => $phone_info->purePhoneNumber,
            //     ];
            //     $r = ModelUser::create($data);
            //     $user = ModelUser::find($r->id);
            // }
            // $user->append(['rz_status', 'headimgurl', 'dept_name', 'position_name', 'rz_status_zw']);

            //保存visitor电话号
            $this->saveVisitorInfo($openid, ['tel' => $tel]);

            //查找用户
            $user = ModelUser::where('openid', $openid)->find();
            if (empty($user)) {
                $user = ModelUser::where('tel', $tel)->find();
                if (empty($user)) {
                    $data = [
                        'openid' => $openid,
                        'tel' => $tel,
                    ];
                    $user = ModelUser::create($data);
                    $user = ModelUser::find($user->id);
                } else {
                    $user->openid = $openid;
                    $user->save();
                }
            }

            $user->append(['rz_status', 'headimgurl', 'dept_name', 'position_name', 'rz_status_zw', 'permition_arr'])
            ->hidden(['c_time', 'delete_time', 'password', 'tongxunlu']);
            $user->deptid = $user->tongxunlu->deptid ?? 0;
            $user->positionid = $user->tongxunlu->positionid ?? 0;
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $user));
    }

    /**************************************************逻辑函数**************************************************/

    /**
     * 初始化visitor.user.
     */
    private function createVisitor($openid)
    {
        $data = [
            'openid' => $openid,
        ];
        $visitor = ModelVisitor::where($data)->find();
        if (!$visitor) {
            ModelVisitor::create($data);
        }

        return true;
    }

    /**
     * 获取openid.
     */
    private function getOpenid($code)
    {
        $maps = [];
        $maps[] = ['k', 'in', ['xcx_appid', 'xcx_appsecret']];
        $conf_info = ModelConf::getConfInfo($maps);

        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$conf_info['xcx_appid'].
            '&secret='.$conf_info['xcx_appsecret'].
            '&js_code='.$code.
            '&grant_type=authorization_code';

        $tc = new \app\common\TyCurl();
        $tc->setUrl($url);
        $r = $tc->getUseCurl();
        $r_arr = json_decode($r, true);
        if (!isset($r_arr['openid'])) {
            throw new \Exception('请求失败，请重新进入', 0);
        }
        $openid = $r_arr['openid'];
        $session_key = $r_arr['session_key'];

        $data['openid'] = $openid;
        $data['session_key'] = $session_key;

        return make_return_arr(1, 'OK', $data);
    }

    /**
     * 保存微信用户.
     */
    private function saveVisitor($openid, $params)
    {
        try {
            $this->validate($params, 'Visitor.add');
        } catch (ValidateException $th) {
            throw new \Exception($th->getError(), 0);
        }
        try {
            $v_data = [];
            $v_data['nickname'] = $params['nickname'];
            $v_data['headimgurl'] = $params['headimgurl'];

            $this->saveVisitorInfo($openid, $v_data);
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }

        return make_return_arr(1, 'OK');
    }

    /**
     * 保存微信信息.
     */
    private function saveVisitorInfo($openid, $data)
    {
        $visitor = ModelVisitor::where('openid', $openid)->find();
        if ($visitor) {
            $data['id'] = $visitor->id;
            ModelVisitor::update($data);
        } else {
            $data['openid'] = $openid;
            ModelVisitor::create($data);
        }

        return true;
    }

    /**
     * 检查登录规则.
     */
    private function checkOpenidRule($params)
    {
        if (empty($params['code'])) {
            throw new \Exception('code参数错误', 0);
        }

        $data['code'] = $params['code'];

        return make_return_arr(1, 'OK', $data);
    }
}
