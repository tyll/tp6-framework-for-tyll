<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Corp as ModelCorp;
use app\model\Dept as ModelDept;
use app\model\Tongxunlu as ModelTongxunlu;

/**
 * @Author: godliu
 * @Date: 2021-03-02 10:00:00
 */
class Index extends Base
{
    protected $middleware = [
        TokenAuth::class => ['only' => ['getUserInfo', 'getCorpInfo']],
        UserAuth::class => ['only' => ['getUserInfo', 'getCorpInfo']],
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取用户信息.
     */
    public function getUserInfo()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;
        $data['user_info'] = $user_info;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取企业信息.
     */
    public function getCorpInfo()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        // writeLog(json_encode($params, JSON_UNESCAPED_UNICODE), 'notice');

        $corpid = isset($params['corpid']) ? $params['corpid'] : 0;

        $maps = [];
        $maps[] = ['id', '=', $corpid];
        $corp_detail = ModelCorp::getDetail($maps, 'id,name');

        $maps = [];
        $maps[] = ['corpid', '=', $corpid];
        $deptid_arr = ModelDept::getColumn($maps, 'id');
        $maps = [];
        $maps[] = ['userid', '=', $user_info['id']];
        $maps[] = ['deptid', 'in', $deptid_arr];
        $txd_detail = ModelTongxunlu::getDetail($maps, 'id,position');
        $corp_detail['position'] = !empty($txd_detail) ? $txd_detail['position'] : '';

        $maps = [];
        $maps[] = ['userid', '=', $user_info['id']];
        $maps[] = ['status', '=', 1];
        $txl_deptid_arr = ModelTongxunlu::getColumn($maps, 'deptid');
        $maps = [];
        $maps[] = ['id', 'in', $txl_deptid_arr];
        $dept_corpid_arr = ModelDept::getColumn($maps, 'corpid');
        $corp_detail['rz_corpid_arr'] = $dept_corpid_arr;

        // 获取入职的孵化器企业
        $maps = [];
        $maps[] = ['id', 'in', $dept_corpid_arr];
        $maps[] = ['type', '=', 1];
        $md_corp_list = ModelCorp::getList($maps, 'id,name');
        $corp_detail['md_corp_list'] = $md_corp_list;

        $data['corp_info'] = $corp_detail;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**************************************************逻辑函数**************************************************/
}
