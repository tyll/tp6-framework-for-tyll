<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\CheckTaskGroup as ModelCheckTaskGroup;
use app\model\Dept as ModelDept;
use app\model\Position as ModelPosition;
use app\model\Tongxunlu as ModelTongxunlu;
use app\model\User as ModelUser;
use app\model\Visitor as ModelVisitor;
use think\Collection;

/**
 * 人员.
 *
 * @Author: ydd
 * @Date: 2022-06-13 10:00:00
 */
class User extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 入职.
     */
    public function ruzhi()
    {
        $adapter = ['name', 'tel', 'deptid', 'positionid', 'gonghao'];
        $input = $this->request->only($adapter);
        $user_info = $this->request->user_info;

        try {
            $this->validate($input, 'User.ruzhi');
            $this->validateRuzhi($input, $user_info);

            \think\facade\Db::startTrans();

            //保存用户信息
            $u_data = $input;
            $u_data['openid'] = $user_info->openid;
            $u_data['id'] = $user_info->id;
            $u_data['password'] = md5('123456');
            ModelUser::update($u_data);

            //保存visitor电话号码
            ModelVisitor::where('openid', $user_info->openid)->update(['tel' => $input['tel']]);

            //保存通讯录
            $this->saveTongxunlu($user_info, $input);

            \think\facade\Db::commit();
        } catch (\Throwable $th) {
            \think\facade\Db::rollback();

            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '操作成功'));
    }

    /**
     * 巡检排行榜.
     */
    public function getXunjianRank()
    {
        $adapter = ['page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $datetime = date('Y-m-d H:i:s');
            $list = ModelUser::field('id,name user_name,openid')->append(['headimgurl'])->hidden(['openid'])->select();
            $num = ModelUser::count();

            foreach ($list as $k => $v) {
                $task_group = ModelCheckTaskGroup::where("JSON_CONTAINS(canyu_userids,'$v->id')")
                    ->where("expect_start_time<='$datetime' and expect_end_time>='$datetime'")
                    ->order('expect_start_time asc')->find();
                if (!$task_group) {
                    unset($list[$k]);
                    continue;
                }
                $total_task = $task_group->task->count();
                $v->done_num = $task_group->task()->where('done_userid', '>', 0)->count();
                $v->done_percent = ($total_task ? round($v->done_num / $total_task * 100) : 0);
            }

            $list = array_values($list->order('done_percent', 'desc')->toArray());

            if ($input['rows'] > 0) {
                $offset = ($input['page'] - 1) * $input['rows'];
                $list = array_slice($list, $offset, $input['rows']);
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', ['total' => $num, 'page' => $input['page'], 'list' => $list]));
    }

    /**************************************************逻辑函数**************************************************/

    /**
     * 排序.
     *
     * @param Collection|array $list 数据集或二维数组
     */
    private function sort($list, $field, $order = 'asc')
    {
        if ($list instanceof Collection) {
            $list = $list->toArray();
        }

        usort($list, fn ($a, $b) => $order == 'desc' ? intval($a[$field] < $b[$field]) : intval($a[$field] > $b[$field]));

        return $list;
    }

    /**
     * 日期转时间戳.
     *
     * @param array $date_arr 日期数组 开始与结束
     */
    private function dateStrToTime(array $date_arr)
    {
        foreach ($date_arr as $k => &$v) {
            $his = $k == 'end_date' ? '23:59:59' : '';
            $v = strtotime($v.' '.$his);
        }
        unset($v);

        return $date_arr;
    }

    /**
     * 验证入职参数.
     */
    private function validateRuzhi($input, $user)
    {
        if ($user->rz_status == 1) {
            throw new \Exception('您已入职,请勿重复提交');
        }

        $maps = [
            ['tel', '=', $input['tel']],
        ];
        if ($user) {
            $maps[] = ['id', '<>', $user->id];
        }
        $tmp = ModelUser::where($maps)->find();
        if ($tmp) {
            throw new \Exception('手机号已存在');
        }
        /*  $maps = [
             ['gonghao', '=', $input['gonghao']],
         ];
         if ($user) {
             $maps[] = ['id', '<>', $user->id];
         }
         $tmp = ModelUser::where($maps)->find();
         if ($tmp) {
             throw new \Exception('工号已存在');
         } */

        $dept = ModelDept::find($input['deptid']);
        if (!$dept) {
            throw new \Exception('部门不存在');
        }
        $position = ModelPosition::find($input['positionid']);
        if (!$position) {
            throw new \Exception('岗位不存在');
        }
        if ($position->deptid != $dept->id) {
            throw new \Exception('岗位选择错误');
        }

        return true;
    }

    /**
     * 保存通讯录.
     */
    private function saveTongxunlu(ModelUser $user, $input)
    {
        $t_data = [
            'userid' => $user->id,
            'deptid' => $input['deptid'],
            'positionid' => $input['positionid'],
            'status' => 0,
            'rz_time' => '0000-00-00 00:00:00',
            'lz_time' => '0000-00-00 00:00:00',
        ];

        if ($user->tongxunlu) {
            $t_data['id'] = $user->tongxunlu->id;
            ModelTongxunlu::update($t_data);
        } else {
            ModelTongxunlu::create($t_data);
        }
    }
}
