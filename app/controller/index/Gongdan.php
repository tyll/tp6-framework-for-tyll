<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Gongdan as ModelGongdan;

/**
 * 工单
 * @Author: godliu
 * @Date: 2022-06-23 10:00:00
 */
class Gongdan extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取工单
     */
    public function getGongdan()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            $params['userid'] = $user_info['id'];

            $maps = $this->getSearchGongdanMaps($params);
            $limit = ($params['page'] - 1) * $params['rows'];
            $list = ModelGongdan::getList($maps, 'id,name,status,c_time', 'id DESC', '', $limit, $params['rows']);
            foreach ($list as $k => $v) {
                $list[$k]['status_name'] = $v->status_zw;
                $list[$k]['c_time'] = date('Y-m-d', strtotime($v['c_time']));
                $list[$k]['name'] = mb_substr($v['name'], 5, -3);
            }
            $total = ModelGongdan::getCount($maps, 'id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;
        return json(make_return_arr(1, 'OK', $data));
    }

    /**************************************************逻辑函数**************************************************/
    /**
     * 获取工单查询条件
     */
    private function getSearchGongdanMaps($params)
    {
        $maps = [];
        $maps[] = ['target', '=', 'device'];
        $maps[] = ['targetid', '=', $params['targetid']];
        return $maps;
    }
}
