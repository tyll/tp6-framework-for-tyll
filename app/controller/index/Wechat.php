<?php

declare(strict_types=1);

namespace app\controller\index;

use app\common\TyCurl;
use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Conf as ModelConf;
use app\model\Login as ModelLogin;
use app\model\User as ModelUser;
use Exception;

/**
 * 微信公众号.
 */
class Wechat extends Base
{
    protected $middleware = [
        TokenAuth::class => ['only' => ['bind', 'isRuzhi', 'getUserToken']],
        UserAuth::class => ['only' => ['isRuzhi']],
    ];
    private $minute = 5;

    protected $tywyx = null;
    protected $subscribe = 0;

    protected function initialize()
    {
        if (empty($this->tywyx)) {
            $this->tywyx = \think\facade\Config::get('tyll.tywyx');
        }
    }

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function index()
    {
        return redirect(\think\facade\Config::get('tyll.tywyx_openid_url'));
    }

    /**
     * 获取openid.
     */
    public function openid()
    {
        $params = $this->request->only(['gen_time', 'sign', 'openid', 'access_token']);
        try {
            $this->validate($params, 'Wechat.openid');

            $r = $this->checkApiSign((int) $params['gen_time'], $params['sign']);
            if (!$r) {
                $params_json = json_encode($params);
                writeLog("桃园微营销回调签名验证失败\n{$params_json}");

                return $this->index();
            }
            //openid
            $openid = $params['openid'];
            $openid = str_replace('我', '-', $openid);

            //检查重定向前端地址
            $d = [];
            $fe_url = $this->request->root(true).\think\facade\Config::get('tyll.fe.web_path');
            //获取token
            $th = new \app\common\TyToken();
            $th->setTokenid($openid);
            $th->setDay(1);
            $token = $th->getToken();
            $d['token'] = $token;
            $d['expire_time'] = $expire_time = time() + 300;
            $d['sign'] = $sign = md5($token.$expire_time);
            $dest_url = $fe_url.'?'.$this->dealUrlParams($d);
            //日志
            $url = $this->request->url(true);
            writeLog("桃园微营销回调地址{$url}-------token:{$token}");
        } catch (Exception $e) {
            return json(make_return_arr(0, $e->getMessage()));
        }

        return redirect($dest_url);
    }

    /**
     * 获取usertoken.
     */
    public function getUserToken()
    {
        try {
            $user = ModelUser::where('openid', $this->request->tokenid)->find();
            if ($user) {
                $th = new \app\common\TyToken();
                $th->setTokenid($user->id);
                $th->setDay(1);
                $token = $th->getToken();
            } else {
                $token = input('token');
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', ['token' => $token]));
    }

    /**
     * 获取签名.
     *
     * @return \think\response\Json
     */
    public function getSign()
    {
        $params = $this->request->param();

        $url = $params['url']; //当前完整域名

        $wxdata = $this->genJsSdkSignature($url);

        return json(make_return_arr(1, 'OK', $wxdata));
    }

    /**
     * 绑定用户.
     */
    public function bind()
    {
        $adapter = ['tel', 'code'];
        $input = $this->request->only($adapter);

        try {
            $this->validate($input, 'Wechat.bind');

            //验证短信验证码
            $maps = [
                ['type', '=', 3],
                ['tel', '=', trim((string) $input['tel'])],
                ['code', '=', trim((string) $input['code'])],
                ['expire_time', '>', time()],
                ['status', '=', 0],
            ];
            $code = ModelLogin::where($maps)->find();
            if (!$code) {
                throw new \Exception('手机验证码错误', 0);
            }

            //注册或绑定用户
            $user = ModelUser::where('tel', $input['tel'])->find();
            //查询openid是否被绑定过
            $tmp = ModelUser::where('openid', $this->request->tokenid)->find();
            if ($tmp && $tmp->tel != $input['tel']) {
                throw new Exception('您已绑定过手机号', 0);
            }
            if ($user) {
                $user->openid = $this->request->tokenid;
                $user->save();
            } else {
                $u_data = [
                    'tel' => $input['tel'],
                    'idcard_photo' => '{"zm":"","fm":""}',
                    'openid' => $this->request->tokenid,
                ];
                $user = ModelUser::create($u_data);
            }
            ///更新手机验证码状态
            $code->status = 1;
            $code->save();
            //获取token
            $th = new \app\common\TyToken();
            $th->setTokenid($user->id);
            $th->setDay(1);
            $token = $th->getToken();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '绑定成功', ['token' => $token]));
    }

    public function getUserinfo()
    {
        try {
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok'));
    }

    /*****************************************************************************************. */

    /**
     * 获取appid.
     *
     * @return mixed
     */
    protected function getAppid()
    {
        $maps = [];
        $maps[] = ['key', '=', 'gzh_appid'];
        $appid = ModelConf::where($maps)->value('value');

        return $appid;
    }

    /**
     * 获取js_ticket.
     *
     * @return mixed
     */
    protected function getJsticket()
    {
        // 1.从缓存中取出ticket（数组）
        // 2、如果为空则从桃园微营销获取，并写入缓存（指定过期时间），并赋值给中间变量以便后用
        // 3、返回
        $ticket = \think\facade\Cache::get('ticket', false);
        if ($ticket) {
            return $ticket['jsticket'];
        }
        $url = \think\facade\Config::get('tyll.tywyx.jsticket_url');
        $curl = new  TyCurl();
        $data = json_decode($curl->get($url), true);
        if (!$data || $data['errcode'] != 0) {
            throw new Exception('获取ticket错误', 0);
        }
        \think\facade\Cache::set('ticket', $data, $data['jsticket_deadtime'] - time());
        //缓存data
        return $data['jsticket'];
    }

    /**
     * 生成随机字符串.
     *
     * @param int $length
     *
     * @return string
     */
    protected function genRandStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $str = '';
        for ($i = 0; $i < $length; ++$i) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }

        return $str;
    }

    /**
     * 生成分享签名.
     *
     * @param $url
     *
     * @return mixed
     */
    protected function genJsSdkSignature($url = '')
    {
        $url = explode('#/', $url)[0];
        $jsapi_ticket = $this->getJsticket();
        $data['jsapi_ticket'] = $jsapi_ticket;
        $data['noncestr'] = $this->genRandStr();
        $data['timestamp'] = time();
        $data['url'] = $url;
        $data['signature'] = sha1($this->dealUrlParams($data));
        $data['appid'] = $this->getAppid();

        return $data;
    }

    /**
     * 检查发送手机验证码规则.
     */
    private function checkSendCodeRule($params)
    {
        try {
            $this->validate($params, 'Wechat.send_code');
        } catch (Exception $th) {
            throw new \Exception($th->getMessage(), 0);
        }

        $now_time = date('Y-m-d H:i');

        // 检查手机号码每分钟发送数量
        $l_maps = [];
        $l_maps[] = ['tel', '=', trim($params['tel'])];
        $l_maps[] = ['c_time', 'like', $now_time.'%'];
        $l_num = ModelLogin::getCount($l_maps, 'id');
        if ($l_num >= 1) {
            throw new \Exception('操作频繁，请稍后再试', 0);
        }

        return make_return_arr(1, 'OK');
    }

    /**
     * 检查签名.
     */
    protected function checkApiSign(int $gen_time, string $sign)
    {
        $my_time = time();
        if ($my_time - $gen_time > 600) {
            return 0;
        }
        $my_sign = md5($gen_time.$gen_time);
        if ($my_sign == $sign) {
            return 1;
        }

        return 0;
    }

    /**
     * 处理url参数.
     *
     * @param $url_params
     *
     * @return string
     */
    protected function dealUrlParams($url_params)
    {
        $buff = '';
        foreach ($url_params as $k => $v) {
            if ($k != 'signature') {
                $buff .= $k.'='.$v.'&';
            }
        }
        $buff = trim($buff, '&');

        return $buff;
    }
}
