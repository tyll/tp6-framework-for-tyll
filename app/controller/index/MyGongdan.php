<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Gongdan as ModelGongdan;
use app\model\GongdanFlow as ModelGongdanFlow;
use app\model\User as ModelUser;

/**
 * 我的工单
 * @Author: godliu
 * @Date: 2022-06-17 10:00:00
 */
class MyGongdan extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取统计
     */
    public function getTotal()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        $params['userid'] = $user_info['id'];

        $params['label'] = 'wait';
        $maps = $this->getSearchGongdanMaps($params);
        $wait_num = ModelGongdan::getCount($maps, 'id');

        $params['label'] = 'dealing';
        $maps = $this->getSearchGongdanMaps($params);
        $dealing_num = ModelGongdan::getCount($maps, 'id');

        $params['label'] = 'end';
        $maps = $this->getSearchGongdanMaps($params);
        $end_num = ModelGongdan::getCount($maps, 'id');

        $params['label'] = 'all';
        $maps = $this->getSearchGongdanMaps($params);
        $all_num = ModelGongdan::getCount($maps, 'id');

        $data['wait_num'] = $wait_num;
        $data['dealing_num'] = $dealing_num;
        $data['end_num'] = $end_num;
        $data['all_num'] = $all_num;
        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取工单
     */
    public function getGongdan()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            $params['userid'] = $user_info['id'];

            $maps = $this->getSearchGongdanMaps($params);
            $limit = ($params['page'] - 1) * $params['rows'];
            $list = ModelGongdan::getList($maps, 'id,name,from_userid,status,c_time', 'id DESC', '', $limit, $params['rows']);
            foreach ($list as $k => $v) {
                $list[$k]['status_name'] = $v->status_zw;

                if ($v['from_userid'] > 0) {
                    $u_maps = [];
                    $u_maps[] = ['id', '=', $v['from_userid']];
                    $u_detail = ModelUser::getDetail($u_maps, 'id,name');
                    $list[$k]['from_username'] = !empty($u_detail) ?  $u_detail['name'] : '';
                } else {
                    $list[$k]['from_username'] = '系统';
                }
            }
            $total = ModelGongdan::getCount($maps, 'id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;
        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取工单详情
     */
    public function getGongDanDetail()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            // 检查获取工单详情规则
            $r = $this->checkGetGongdanDetailRule($params);
            $g_detail = $r['data']['g_detail'];

            $gongdan_info = [];
            $gongdan_info['gongdanid'] = 1;
            $gongdan_info['status'] = $g_detail['status'];

            $target_params = [];
            $target_params['target'] = $g_detail['target'];
            $target_params['targetid'] = $g_detail['targetid'];
            $target_params['check_taskid'] = $g_detail['check_taskid'];
            $target_params['name'] = $g_detail['name'];
            $flow_params = [];
            $flow_params['gongdanid'] = $g_detail['id'];
            $action_params = [];
            $action_params['gongdanid'] = $g_detail['id'];
            $action_params['status'] = $g_detail['status'];
            $g = new \app\obj\Gongdan();
            $g->setTargetParams($target_params);
            $g->setFlowParams($flow_params);
            $g->setActionParams($action_params);

            $r = $g->getTarget();
            $target_info = $r['target_info'];

            $r = $g->getFlow();
            $flow_info = $r['flow_info'];

            $r = $g->getAction();
            $action_info = $r['action_info'];
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['gongdan_info'] = $gongdan_info;
        $data['target_info'] = $target_info;
        $data['flow_info'] = $flow_info;
        $data['action_info'] = $action_info;
        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 处理工单
     */
    public function dealGongdan()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            $params['userid'] = $user_info['id'];

            // 处理工单
            $g = new \app\obj\Gongdan();
            $g->setGongdanParams($params);
            $r = $g->dealGongdan();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        return json(make_return_arr(1, 'OK'));
    }

    /**************************************************逻辑函数**************************************************/
    /**
     * 获取工单查询条件
     */
    private function getSearchGongdanMaps($params)
    {
        $gf_maps = [];
        $gf_maps[] = ['oper_userid', '=', $params['userid']];
        $gongdanid_arr = ModelGongdanFlow::getColumn($gf_maps, 'DISTINCT gongdanid');
        // $gongdanid_arr = [5, 4, 3, 2, 1];
        $gongdanid_str = !empty($gongdanid_arr) ? implode(',', $gongdanid_arr) : 0;

        $maps = '';
        if ($params['label'] == 'wait') {
            $maps .= 'JSON_CONTAINS(`curr_userids`,\'[' . $params['userid'] . ']\')';
            $maps .= ' AND `status` != 100';
        } elseif ($params['label'] == 'dealing') {
            $maps .= '`id` IN (' . $gongdanid_str . ')';
            $maps .= ' AND `status` = 30';
        } elseif ($params['label'] == 'end') {
            $maps .= '`id` IN (' . $gongdanid_str . ')';
            $maps .= ' AND `status` = 100';
        } elseif ($params['label'] == 'all') {
            $maps .= '`id` IN (' . $gongdanid_str . ')';
        } else {
            $maps .= '`id` = 0';
        }
        return $maps;
    }

    /**
     * 检查获取工单详情规则
     */
    private function checkGetGongdanDetailRule($params)
    {
        $maps = [];
        $maps[] = ['id', '=', $params['gongdanid']];
        $g_detail = ModelGongdan::getDetail($maps, 'id,target,targetid,check_taskid,name,status');
        if (empty($g_detail)) {
            throw new \Exception('该工单不存在', 0);
        }
        $data['g_detail'] = $g_detail;
        return make_return_arr(1, 'OK', $data);
    }
}
