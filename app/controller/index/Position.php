<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\model\Position as ModelPosition;

/**
 * 岗位.
 *
 * @Author: ydd
 * @Date: 2022-06-13 10:00:00
 */
class Position extends Base
{
    protected $middleware = [
        TokenAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 显示资源列表.
     */
    public function index()
    {
        $adapter = ['deptid' => -1, 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [
                'deptid' => $input['deptid'],
            ];

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelPosition::field('id,name,deptid')
                ->where($maps)
                ->order('id asc')
                ->limit($offset, $input['rows'])
                ->append([])
                ->hidden([])
                ->select();
            $num = ModelPosition::where($maps)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**************************************************逻辑函数**************************************************/
}
