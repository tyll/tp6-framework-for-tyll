<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Download as ModelDownload;

/**
 * 下载api控制器
 * @Author: godliu
 * @Date: 2021-03-22 10:00:00
 */
class Download extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取下载
     */
    public function getDownload()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        $maps = $this->getSearchDownloadMaps($user_info['id']);
        $limit = ($params['page'] - 1) * $params['rows'];
        $list = ModelDownload::getList($maps, 'id,name,url,status,c_time', 'id desc', '', $limit, (int)$params['rows']);
        foreach ($list as $k => $v) {
            $list[$k]['status_zw'] = $v->status_name;
        }

        $data['list'] = $list;
        $data['page'] = $params['page'];
        return json(make_return_arr(1, 'OK', $data));
    }

    /**************************************************逻辑函数**************************************************/
    /**
     * 获取下载查询条件
     */
    private function getSearchDownloadMaps($userid)
    {
        $maps = [];
        $maps[] = ['owner', '=', $userid];

        $start_date = date('Y-m-d', strtotime('-7 day')) . ' 00:00:00';
        $end_date = date('Y-m-d') . ' 23:59:59';
        $maps[] = ['c_time', 'between', [$start_date, $end_date]];
        return $maps;
    }
}
