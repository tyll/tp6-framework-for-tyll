<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;

/**
 * 登录api控制器.
 *
 * @Author: godliu
 * @Date: 2021-03-02 10:00:00
 */
class Login extends Base
{
    private $minute = 5;

    protected $middleware = [
        TokenAuth::class => ['only' => ['refreshToken']],
        UserAuth::class => ['only' => ['refreshToken']],
    ];

    public function __construct()
    {
        parent::__construct();
    }
}
