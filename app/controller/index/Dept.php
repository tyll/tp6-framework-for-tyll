<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\model\Dept as ModelDept;
use app\model\Tongxunlu as ModelTongxunlu;

/**
 * 部门.
 *
 * @Author: ydd
 * @Date: 2022-06-13 10:00:00
 */
class Dept extends Base
{
    protected $middleware = [
        TokenAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 显示资源列表.
     */
    public function index()
    {
        $adapter = ['parentid', 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = [];
            $maps[] = ['id', '<>', 1];

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelDept::field('id,name')
                ->where($maps)
                ->order('id asc')
                ->limit($offset, $input['rows'])
                ->select();
            $total = ModelDept::where($maps)->count('id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取通讯录部门.
     */
    public function getTxlDept()
    {
        $adapter = ['page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $maps = $this->getSearchTxlDeptMaps($input);
            $offset = ($input['page'] - 1) * $input['rows'];

            $list = ModelDept::field('id,name')
                ->where($maps)
                ->order('id asc')
                ->limit($offset, $input['rows'])
                ->select();
            foreach ($list as $k => $v) {
                $txl_maps = [];
                $txl_maps[] = ['deptid', '=', $v['id']];
                $txl_maps[] = ['status', '=', 1];
                $txl_num = ModelTongxunlu::where($txl_maps)->count('id');
                $list[$k]['txl_num'] = $txl_num;
            }
            $total = ModelDept::where($maps)->count('id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**************************************************逻辑函数**************************************************/

    /**
     * 获取通讯录部门查询条件.
     */
    private function getSearchTxlDeptMaps()
    {
        $maps = [];

        return $maps;
    }
}
