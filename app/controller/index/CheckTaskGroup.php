<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Area as ModelArea;
use app\model\CheckItem as ModelCheckItem;
use app\model\CheckTask as ModelCheckTask;
use app\model\CheckTaskGroup as ModelCheckTaskGroup;
use app\model\CheckTaskTargetGroup as ModelCheckTaskTargetGroup;
use app\model\Device as ModelDevice;

/**
 * 巡检任务组.
 *
 * @Author: godliu
 * @Date: 2022-06-14 10:00:00
 */
class CheckTaskGroup extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class,
    ];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取巡检任务组.
     */
    public function getCheckTaskGroup()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            $params['userid'] = $user_info['id'];

            $maps = $this->getSearchCheckTaskGroupMaps($params);
            $limit = ($params['page'] - 1) * $params['rows'];
            $list = ModelCheckTaskGroup::getList($maps, 'id,title,expect_start_time', 'expect_start_time ASC', '', $limit, $params['rows']);
            foreach ($list as $k => $v) {
                $list[$k]['date'] = date('Y-m-d', strtotime($v['expect_start_time']));
            }

            $total = ModelCheckTaskGroup::getCount($maps, 'id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取巡检任务组及设备.
     */
    public function getCheckTaskGroupDevice()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            $params['userid'] = $user_info['id'];

            $maps = $this->getSearchCheckTaskGroupDeviceMaps($params);
            $limit = ($params['page'] - 1) * $params['rows'];
            $list = ModelCheckTaskGroup::getList($maps, 'id,title', 'expect_start_time ASC', '', $limit, $params['rows']);
            foreach ($list as $k_ctg => $v_ctg) {
                $device_list = [];

                $cttg_maps = [];
                $cttg_maps[] = ['task_groupid', '=', $v_ctg['id']];
                $cttg_list = ModelCheckTaskTargetGroup::getList($cttg_maps, 'id,targetid');
                foreach ($cttg_list as $k_cttg => $v_cttg) {
                    $d_maps = [];
                    $d_maps[] = ['id', '=', $v_cttg['targetid']];
                    $d_detail = ModelDevice::getDetail($d_maps, 'id,name,areaid');
                    $deviceid = !empty($d_detail) ? $d_detail['id'] : 0;
                    $device_zw = !empty($d_detail) ? $d_detail['name'] : '';
                    $device_areaid = !empty($d_detail) ? $d_detail['areaid'] : 0;

                    $a_maps = [];
                    $a_maps[] = ['id', '=', $device_areaid];
                    $a_detail = ModelArea::getDetail($a_maps, 'id,name');
                    $area_zw = !empty($a_detail) ? $a_detail['name'] : '';

                    $ci_maps = [];
                    $ci_maps[] = ['target', '=', 'device'];
                    $ci_maps[] = ['pk', '=', $deviceid];
                    $check_itemid_arr = ModelCheckItem::getColumn($ci_maps, 'id');
                    $ct_maps = [];
                    $ct_maps[] = ['task_groupid', '=', $v_ctg['id']];
                    $ct_maps[] = ['check_itemid', 'in', $check_itemid_arr];
                    $ct_maps[] = ['done_userid', '=', 0];
                    $ct_detail = ModelCheckTask::getDetail($ct_maps, 'id');
                    $is_finish = empty($ct_detail) ? 1 : 0;

                    $device_list[$k_cttg]['id'] = $deviceid;
                    $device_list[$k_cttg]['name'] = $device_zw;
                    $device_list[$k_cttg]['area_zw'] = $area_zw;
                    $device_list[$k_cttg]['is_finish'] = $is_finish;
                }
                $list[$k_ctg]['device_list'] = $device_list;
            }
            $total = ModelCheckTaskGroup::getCount($maps, 'id');
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['list'] = $list;
        $data['total'] = $total;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 获取巡检任务组详情.
     */
    public function getCheckTaskGroupDetail()
    {
        $params = $this->request->param();
        $user_info = $this->request->user_info;

        try {
            // 检查获取巡检任务组详情规则
            $r = $this->checkGetCheckTaskGroupDetailRule($params);
            $ctg_detail = $r['data']['ctg_detail'];
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }
        $data['detail'] = $ctg_detail;

        return json(make_return_arr(1, 'OK', $data));
    }

    /**
     * 显示资源列表.
     */
    public function index()
    {
        $adapter = ['period', 'date', 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $user = $this->request->user_info;

            $maps = [];
            if (!empty($input['date'])) {
                $month_date = get_month_date($input['date']);
                $maps[] = ['expect_start_time', 'between', array_values($month_date)];
            }

            $sql = $this->getListSql($user, $input);

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelCheckTaskGroup::field('id,title,expect_start_time,expect_end_time,status')
                ->where($maps)
                ->where($sql)
                ->order('expect_start_time asc,id asc')
                ->limit($offset, $input['rows'])
                ->append(['status_zw', 'is_yuqi'])
                ->hidden([])->fetchSql(false)
                ->select();
            $num = ModelCheckTaskGroup::where($maps)->where($sql)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 统计数量.
     */
    public function getTotalNum()
    {
        try {
            $user = $this->request->user_info;
            $date = date('Y-m-d');
            $time = date('H:i:s');
            $day_date = get_day_date();
            $datetime = date('Y-m-d H:i:s');
            $base_sql = "JSON_CONTAINS(canyu_userids,'$user->id')";

            $yq_sql = $base_sql." and status in (0,2) and expect_end_time < '$datetime'";
            $yq_num = ModelCheckTaskGroup::where($yq_sql)->count();

            $jt_sql = $base_sql." and ('{$datetime}' between expect_start_time and expect_end_time or ".
                "expect_start_time between '{$day_date['start_time']}' and '{$day_date['end_time']}' or ".
                "expect_end_time between '{$day_date['start_time']}' and '{$day_date['end_time']}'"
                .')';
            $jt_num = ModelCheckTaskGroup::where($jt_sql)->count();

            $tomorrow = date('Y-m-d', strtotime('+1 day'));
            $tomorrow_date = get_day_date($tomorrow);
            $mt_sql = $base_sql." and ('{$tomorrow}' between expect_start_time and expect_end_time or ".
                "expect_start_time between '{$tomorrow_date['start_time']}' and '{$tomorrow_date['end_time']}' or ".
                "expect_end_time between '{$tomorrow_date['start_time']}' and '{$tomorrow_date['end_time']}'"
                .')';
            $mt_num = ModelCheckTaskGroup::where($mt_sql)->count();

            $last_date = date('Y-m-d', strtotime('+7 day'));
            $qtn_sql = $base_sql." and (`expect_start_time` between '$tomorrow 00:00:00' and '$last_date 23:59:59' or".
                " `expect_end_time` between '$tomorrow 00:00:00' and '$last_date 23:59:59')";
            $qtn_num = ModelCheckTaskGroup::where($qtn_sql)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', ['yq_num' => $yq_num, 'jt_num' => $jt_num, 'mt_num' => $mt_num, 'qtn_num' => $qtn_num]));
    }

    /**
     * 巡检记录.
     */
    public function getXunjianRecord()
    {
        $adapter = ['page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $user = $this->request->user_info;
            $maps = [
                'status' => 1,
            ];
            $sql = "JSON_CONTAINS(canyu_userids,'$user->id')";

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelCheckTaskGroup::field('id,title,fact_userids,fact_start_time,fact_end_time,status')
                ->where($maps)
                ->where($sql)
                ->limit($offset, $input['rows'])
                ->append(['fact_user_arr', 'status_zw'])
                ->select();

            $num = ModelCheckTaskGroup::where($maps)->where($sql)->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', ['total' => $num, 'list' => $list]));
    }

    /**
     * 巡检记录详情.
     */
    public function getRecordDetail()
    {
        $adapter = ['id'];
        $input = $this->request->only($adapter);

        try {
            $group = ModelCheckTaskGroup::field('id,title,fact_userids,fact_start_time,fact_end_time,status')->find($input['id']);
            if (!$group) {
                throw new \Exception('记录不存在');
            }
            //检查任务
            $check_tasks = ModelCheckTask::where('task_groupid', $group->id)
                ->field('id,check_itemid,answer,done_time,health,xunjian_jlpc')
                ->append(['item_target', 'item_targetid', 'item_content', 'done_time_str'])
                ->hidden(['checkItem'])
                ->select();

            $check_itemids = $check_tasks->column('check_itemid');
            $deviceids = ModelCheckItem::withTrashed()->where('id', 'in', $check_itemids)->where('target', 'device')->column('pk');

            //设备
            $devices = ModelDevice::withTrashed()->where('id', 'in', $deviceids)->field('id,name,areaid')->select();
            //区域
            $areas = ModelArea::withTrashed()->where('id', 'in', $devices->column('areaid'))->field('id,name')->select();

            //区域设备
            foreach ($areas as $k => $v) {
                $area_devices = $devices->where('areaid', $v->id);
                //设备检查项
                foreach ($area_devices as $k2 => $v2) {
                    $device_check_items = $check_tasks->where('item_target', 'device')->where('item_targetid', $v2->id);

                    $v2->check_items = array_values($device_check_items->toArray());
                }
                $v->devices = array_values($area_devices->toArray());
            }

            $group->areas = $areas->toArray();
            $group->append(['fact_user_arr', 'status_zw']);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', $group));
    }

    /**************************************************逻辑函数**************************************************/

    /**
     * 获取巡检任务组查询条件.
     */
    private function getSearchCheckTaskGroupMaps($params)
    {
        $maps = '';
        $maps .= '`status` IN (0,2)';
        $maps .= ' AND `check_scheduleid` = '.$params['check_scheduleid'];
        $maps .= ' AND JSON_CONTAINS(`canyu_userids`,\'['.$params['userid'].']\')';

        return $maps;
    }

    /**
     * 获取巡检任务组设备查询条件.
     */
    private function getSearchCheckTaskGroupDeviceMaps($params)
    {
        $now_time = date('Y-m-d H:i:s');

        $maps = '';
        $maps .= 'JSON_CONTAINS(`canyu_userids`,\'['.$params['userid'].']\')';
        $maps .= ' AND expect_start_time < "'.$now_time.'"';
        $maps .= ' AND expect_end_time > "'.$now_time.'"';

        return $maps;
    }

    /**
     * 检查获取巡检任务组详情规则.
     */
    private function checkGetCheckTaskGroupDetailRule($params)
    {
        $maps = [];
        $maps[] = ['id', '=', $params['task_groupid']];
        $ctg_detail = ModelCheckTaskGroup::getDetail($maps, 'id,title,fact_start_time,fact_end_time');
        if (empty($ctg_detail)) {
            throw new \Exception('该巡检任务组不存在', 0);
        }
        $data['ctg_detail'] = $ctg_detail;

        return make_return_arr(1, 'OK', $data);
    }

    /**
     * 列表sql.
     */
    private function getListSql($user, $input)
    {
        $sql = "JSON_CONTAINS(canyu_userids,'$user->id')";
        if (!empty($input['period'])) { //yq   jt  mt  qtn
            // $date = date('Y-m-d');
            $day_date = get_day_date();
            $datetime = date('Y-m-d H:i:s');
            if ($input['period'] == 'yq') {
                $sql .= " and status in (0,2) and expect_end_time < '$datetime'";
            } elseif ($input['period'] == 'jt') {
                // $day = get_day_date();
                // $sql .= " and `expect_end_time` between '{$day['start_time']}' and '{$day['end_time']}'";
                $sql .= " and ('{$datetime}' between expect_start_time and expect_end_time or ".
                    "expect_start_time between '{$day_date['start_time']}' and '{$day_date['end_time']}' or ".
                    "expect_end_time between '{$day_date['start_time']}' and '{$day_date['end_time']}'"
                    .')';
            } elseif ($input['period'] == 'mt') {
                // $tomorrow = get_day_date(date('Y-m-d', strtotime('+1 day')));
                // $sql .= " and `expect_end_time` between '{$tomorrow['start_time']}' and '{$tomorrow['end_time']}'";
                $tomorrow = date('Y-m-d', strtotime('+1 day'));
                $tomorrow_date = get_day_date($tomorrow);
                $sql .= " and ('{$tomorrow}' between expect_start_time and expect_end_time or ".
                    "expect_start_time between '{$tomorrow_date['start_time']}' and '{$tomorrow_date['end_time']}' or ".
                    "expect_end_time between '{$tomorrow_date['start_time']}' and '{$tomorrow_date['end_time']}'"
                    .')';
            } elseif ($input['period'] == 'qtn') {
                $tomorrow = date('Y-m-d', strtotime('+1 day'));
                $last_date = date('Y-m-d', strtotime('+7 day'));
                $sql .= " and (`expect_start_time` between '$tomorrow 00:00:00' and '$last_date 23:59:59' or".
                    " `expect_end_time` between '$tomorrow 00:00:00' and '$last_date 23:59:59')";
            }
        }

        return $sql;
    }
}
