<?php

declare(strict_types=1);

namespace app\controller\index;

use app\middleware\TokenAuth;
use app\middleware\UserAuth;
use app\model\Message as ModelMessage;

/**
 * 消息通知.
 */
class Message extends Base
{
    protected $middleware = [
        TokenAuth::class,
        UserAuth::class,
    ];

    /**
     * 显示资源列表.
     *
     * @return \think\Response
     */
    public function getTopicList()
    {
        try {
            $user = $this->request->user_info;

            //,count(case when read_time=0 then read_time end) unread_num
            $list = ModelMessage::where('to_userid', $user->id)
                ->field('topic')
                ->group('topic')
                ->select();

            foreach ($list as $k => $v) {
                $maps = [
                    'to_userid' => $user->id,
                    'topic' => $v->topic,
                    'read_time' => 0,
                ];
                $v->unread_num = ModelMessage::where($maps)->count();
            }
            $num = ModelMessage::where('to_userid', $user->id)->group('topic')->count();
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function getMessageList()
    {
        $adapter = ['topic', 'page' => 0, 'rows' => 0];
        $input = $this->request->only($adapter);

        try {
            $user = $this->request->user_info;

            $maps = [];
            if (!empty($input['topic'])) {
                $maps[] = ['topic', '=', $input['topic']];
            }
            $maps[] = ['to_userid', '=', $user->id];

            $offset = ($input['page'] - 1) * $input['rows'];
            $list = ModelMessage::where($maps)
                ->field('id,topic,to_userid,title,content,read_time')
                ->order('id desc')
                ->limit($offset, $input['rows'])
                ->append([])
                ->hidden([])
                ->select();
            $num = ModelMessage::where($maps)->count();

            //改为已读
            $list->where('read_time', 0)->update(['read_time' => time()]);
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, 'ok', [
            'total' => $num,
            'list' => $list,
        ]));
    }
}
