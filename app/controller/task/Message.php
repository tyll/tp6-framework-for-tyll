<?php

declare(strict_types=1);

namespace app\controller\task;

use app\common\WeChatApplet;
use app\model\MessageDelivery as ModelMessageDelivery;

/**
 * 消息通知.
 */
class Message
{
    /**
     * 发送微信模板消息.
     *
     * @return \think\Response
     */
    public function sendWxGzhMsg($rows = 30)
    {
        try {
            //找出未发送成功且ttl不为零的待发消息
            $maps = [
                ['real_send_time', '=', 0],
                ['channel', '=', 'wxGzhMsg'],
                ['ttl', '>', 0],
                ['expect_send_time', '<', time()],
            ];
            $list = ModelMessageDelivery::where($maps)->limit($rows)->select();
            if ($list->isEmpty()) {
                return json(make_return_arr(0, '本次轮空'));
            }

            //发送消息
            foreach ($list as $k => $v) {
                $res = WeChatApplet::getInstance()->sendTemplateMsg($v->to_user, $v->templateid, $v->content_arr, $v->dest);
                //如果失败则减少ttl
                if ($res->errcode && $res->errcode != 0) {
                    --$v->ttl;
                } else {//发送成功则更新送达时间
                    $v->real_send_time = time();
                }
                //保存返回结果
                $v->result = mb_substr(json_encode($res, JSON_UNESCAPED_UNICODE), 0, 255);
                $v->save();
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '处理完成,本次处理'.$list->count().'条', $list->column('id')));
    }
}
