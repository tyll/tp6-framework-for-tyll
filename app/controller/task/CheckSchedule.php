<?php

declare(strict_types=1);

namespace app\controller\task;

use app\model\CheckItem as ModelCheckItem;
use app\model\CheckSchedule as ModelCheckSchedule;
use app\model\CheckTask as ModelCheckTask;
use app\model\CheckTaskGroup as ModelCheckTaskGroup;
use app\model\CheckTaskTargetGroup as ModelCheckTaskTargetGroup;
use app\model\Conf as ModelConf;
use app\model\Message as ModelMessage;
use app\model\MessageDelivery as ModelMessageDelivery;
use app\model\RItemSchedule as ModelRItemSchedule;
use app\model\User as ModelUser;
use think\model\Collection;

/**
 * 检查计划.
 */
class CheckSchedule
{
    protected $week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    /**
     * 生成检查任务.
     *
     * @param int|string $days 生成多少天的任务
     *
     * @return \think\Response
     */
    public function genCheckTask($days = 7)
    {
        try {
            set_time_limit(0);
            $this->date = date('Y-m-d');
            $this->lastDay = date('Y-m-d', strtotime("+$days day"));

            $maps = [
                'enable' => 1,
            ];
            $list = ModelCheckSchedule::where($maps)->select();
            if ($list->isEmpty()) {
                return json(make_return_arr(0, '没有待处理计划,本次轮空'));
            }

            foreach ($list as $k => $schedule) {
                //根据计划时间段生成任务和任务组
                foreach ($schedule->time_arr as $k2 => $time) {
                    $this->saveGroupOfTime($schedule, $time);
                }
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '全部处理完成', $list->column('title')));
    }

    /**
     * 设置计划提醒消息.
     */
    public function setNotifyMessage()
    {
        try {
            $start_time = date('Y-m-d H:i:00');
            $end_time = date('Y-m-d H:i:59');

            $maps = [
                ['status', '=', 0],
                ['expect_start_time', 'between', [$start_time, $end_time]],
            ];
            $list = ModelCheckTaskGroup::where($maps)->select();
            if ($list->isEmpty()) {
                return json(make_return_arr(0, '没有待处理计划'));
            }

            //保存消息记录
            foreach ($list as $k => $v) {
                $this->saveMessage($v);
            }
        } catch (\Throwable $th) {
            return json(make_return_arr(0, $th->getMessage()));
        }

        return json(make_return_arr(1, '本次处理'.$list->count().'条', $list->column('id')));
    }

    /************************************************************************. */

    /**
     * 保存消息.
     */
    private function saveMessage(ModelCheckTaskGroup $check_task_group)
    {
        //为每一个参与人创建消息
        foreach ($check_task_group->canyu_userid_arr as $k => $v) {
            $user = ModelUser::find($v);
            if (!$user) {
                continue;
            }
            $data = [
                'topic' => '巡检提醒',
                'to_userid' => $v,
                'title' => $check_task_group->title,
                'content' => $check_task_group->date.' '.$check_task_group->expect_start_time.'-'.$check_task_group->expect_end_time,
            ];
            $message = ModelMessage::create($data);
            //保存消息分发表
            $this->saveMessageByWxGzh($user, $message, $check_task_group);
        }

        return true;
    }

    /**
     * 保存分发记录.
     *
     * @param ModelUser           $user             接收人
     * @param ModelMessage        $message          消息
     * @param ModelCheckTaskGroup $check_task_group 任务组
     */
    private function saveMessageByWxGzh(ModelUser $user, ModelMessage $message, ModelCheckTaskGroup $check_task_group)
    {
        //通知模板id
        $templateid = $this->getWxTemplateid();
        //模板参数
        $wx_data = $this->getWxMsgData($check_task_group);

        $data = [
            'channel' => 'wxGzhMsg',
            'messageid' => $message->id,
            'to_user' => $user->openid,
            'title' => '',
            'content' => json_encode($wx_data, JSON_UNESCAPED_UNICODE),
            'expect_send_time' => strtotime($check_task_group->date.' '.$check_task_group->expect_start_time),
            'templateid' => $templateid,
        ];
        ModelMessageDelivery::create($data);

        return true;
    }

    /**
     * 微信消息参数.
     */
    private function getWxMsgData(ModelCheckTaskGroup $check_task_group): array
    {
        $data = [
            'first' => [
                'value' => '',
                'color' => '#ccc',
            ],
            'keyword1' => [
                'value' => $check_task_group->title,
                'color' => '#173177',
            ],
            'keyword2' => [
                'value' => implode(',', $check_task_group->canyu_user_names),
                'color' => '#173177',
            ],
            'keyword3' => [
                'value' => $check_task_group->date.' '.$check_task_group->expect_start_time.'-'.$check_task_group->expect_end_time,
                'color' => '#173177',
            ],
            'keyword4' => [
                'value' => '重庆宝丞炭材有限公司',
                'color' => '#173177',
            ],
            'remark' => [
                'value' => '请尽快进行巡检',
                'color' => '#173177',
            ],
        ];

        return $data;
    }

    /**
     * 微信模板id.
     */
    private function getWxTemplateid()
    {
        $res = json_decode(ModelConf::where('k', 'wx_templateid')->value('v'), true);

        return $res['notify'];
    }

    /**
     * 每个时间段生成任务组.
     */
    private function saveGroupOfTime($schedule, $time)
    {
        //时间段相同的次数
        $same_num = $this->sameTimesNum($schedule->time_arr, $time);
        $gen_num = $same_num;
        //每人都需要完成则每次生成参数人数量的任务组
        if ($schedule->type == 1) {
            $gen_num = $same_num * count($schedule->canyu_userids);
        }

        //生成指定时间内的任务和任务组
        $date = $this->date;
        while ($date <= $this->lastDay) {
            //周期的开始与结束日期
            $period_date = $this->getPeriodDate($schedule, $time, $date);

            //没有生成过  且  日期的天在计划周期内则生成
            if ($this->inPeriod($schedule, $time, $date) && !$this->isGenerate($schedule, $period_date, $gen_num)) {
                $this->saveTaskGroup($schedule, $period_date, $same_num);
            }

            //下个日期
            $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
        }
    }

    /**
     * 相同时间段次数.
     *
     * @param Collection|array              $time_arr 计划的所有时间段
     * @param ModelCheckScheduleTimes|array $time     单个时间段
     */
    private function sameTimesNum($time_arr, $time)
    {
        $num = 0;
        foreach ($time_arr as $k => $v) {
            if (
                $v['start_sn_day'] == $time['start_sn_day'] &&
                $v['end_sn_day'] == $time['end_sn_day'] &&
                $v['start_time'] == $time['start_time'] &&
                $v['end_time'] == $time['end_time']
            ) {
                ++$num;
            }
        }

        return $num;
    }

    /**
     * 是否已生成.
     */
    private function isGenerate(ModelCheckSchedule $schedule, $period_date, $num, $canyu_userid_arr = [])
    {
        //已经生成过的任务组日期
        $task_group_dates = ModelCheckTaskGroup::where('check_scheduleid', $schedule->id)
        ->field('canyu_userids,expect_start_time,expect_end_time')
        ->select();

        //$tmp && $tmp->expect_end_time == $period_date['end_time']
        $gen_num = $task_group_dates->where('expect_start_time', $period_date['start_time'])
            ->where('expect_end_time', $period_date['end_time']);
        //增加参与人的条件
        if (!empty($canyu_userid_arr)) {
            $gen_num = $gen_num->where('canyu_userids', json_encode($canyu_userid_arr));
        }
        $gen_num = $gen_num->count();

        return $gen_num >= $num;
    }

    /**
     * 周期的开始与结束时间.
     */
    private function getPeriodDate(ModelCheckSchedule $schedule, $time, $date)
    {
        $start_time = '';
        $end_time = '';
        if ($schedule->period == 'day') {
            $start_time = $date;
            $end_time = $date;
        } elseif ($schedule->period == 'week') {
            $week_day = get_week_date($date);
            //结束日期大于7则使用7
            $time['end_sn_day'] = $time['end_sn_day'] > 7 ? 7 : $time['end_sn_day'];

            $start_time = date('Y-m-d', strtotime('+'.($time['start_sn_day'] - 1).' day', strtotime($week_day['start_date'])));
            $end_time = date('Y-m-d', strtotime('+'.($time['end_sn_day'] - 1).' day', strtotime($week_day['start_date'])));
        } elseif ($schedule->period == 'month') {
            $month_day = get_month_date($date, true);
            $month_last_day = date('t', strtotime($date));
            //日期大于当月最大日期则采用当月最大日期  (比如设置结束日期为30,在2月只有28天则采用28作为结束日期)
            $time['start_sn_day'] = $time['start_sn_day'] > $month_last_day ? $month_last_day : $time['start_sn_day'];
            $time['end_sn_day'] = $time['end_sn_day'] > $month_last_day ? $month_last_day : $time['end_sn_day'];

            $start_time = date('Y-m-d', strtotime('+'.($time['start_sn_day'] - 1).' day', strtotime($month_day['start_date'])));
            $end_time = date('Y-m-d', strtotime('+'.($time['end_sn_day'] - 1).' day', strtotime($month_day['start_date'])));
        }

        return ['start_time' => $start_time.' '.$time['start_time'], 'end_time' => $end_time.' '.$time['end_time']];
    }

    /**
     * 是否在时间段周期中.
     *
     * @param ModelCheckSchedule $schedule 检查计划
     * @param Collection|array   $time
     * @param string             $date     日期
     */
    private function inPeriod(ModelCheckSchedule $schedule, $time, string $date)
    {
        //当前日期是周期的第几天
        $sn_day = $this->getSnDay($schedule, $date);

        if ($sn_day >= $time['start_sn_day'] && $sn_day <= $time['end_sn_day']) {
            return true;
        }

        return false;
    }

    /**
     * 当前日期是周期的第几天.
     */
    private function getSnDay(ModelCheckSchedule $schedule, string $date)
    {
        $sn_day = 0;
        if ($schedule->period == 'week') {
            $sn_day = date('w', strtotime($date)) ?: 7;
        } elseif ($schedule->period == 'month') {
            $sn_day = date('j', strtotime($date));
        }

        return $sn_day;
    }

    /**
     * 保存任务组.
     */
    private function saveTaskGroup($schedule, $period_date, $same_num)
    {
        if ($schedule->type == 1) {
            $this->saveGroupOfEvery($schedule, $period_date, $same_num);
        } elseif ($schedule->type == 2) {
            $this->saveGroupOfCanyu($schedule, $period_date);
        }

        return true;
    }

    /**
     * 每个参与人都生成任务组.
     */
    private function saveGroupOfEvery($schedule, $period_date, $same_num)
    {
        foreach ($schedule->canyu_userids as $k => $v) {
            if (!$this->isGenerate($schedule, $period_date, $same_num, [$v])) {
                $this->saveGroupOfCanyu($schedule, $period_date, [$v]);
            }
        }

        return true;
    }

    /**
     * 参与人统一生成任务组.
     */
    private function saveGroupOfCanyu($schedule, $period_date, array $canyu_userids = null)
    {
        $group_data = [
            'check_scheduleid' => $schedule->id,
            'title' => $schedule->title,
            'canyu_userids' => json_encode($canyu_userids ?? $schedule->canyu_userids),
            // 'date' => $date,
            'expect_start_time' => $period_date['start_time'],
            'expect_end_time' => $period_date['end_time'],
            'fact_userids' => '[]',
            'fact_start_time' => '0000-00-00 00:00:00',
            'fact_end_time' => '0000-00-00 00:00:00',
        ];
        $group = ModelCheckTaskGroup::create($group_data);

        //保存目标任务组
        $this->saveTaskTargetGroup($group, $schedule);

        //保存任务
        $this->saveTask($group, $schedule);

        return true;
    }

    /**
     * 保存目标任务组.
     */
    private function saveTaskTargetGroup($group, $schedule)
    {
        $itemids = ModelRItemSchedule::where('check_scheduleid', $schedule->id)
            ->order('sort asc,id desc')
            ->column('check_itemid');
        $items = ModelCheckItem::where('id', 'in', $itemids)->group('target,pk')->select();

        $data = [];
        foreach ($items as $k => $v) {
            $tmp = [
                'task_groupid' => $group->id,
                'target' => $v['target'],
                'targetid' => $v['pk'],
                'fact_userids' => '[]',
            ];
            $data[] = $tmp;
        }
        $model = new ModelCheckTaskTargetGroup();
        $model->saveAll($data);

        return true;
    }

    /**
     * 保存任务
     */
    private function saveTask($group, $schedule)
    {
        $itemids = ModelRItemSchedule::where('check_scheduleid', $schedule->id)
            ->order('sort asc,id desc')
            ->column('check_itemid');

        $data = [];
        foreach ($itemids as $k => $v) {
            $tmp = [
                'task_groupid' => $group->id,
                'check_itemid' => $v,
                'canyu_userids' => $group->canyu_userids,
                // 'date' => $group->date,
                'expect_start_time' => $group->expect_start_time,
                'expect_start_timestamp' => strtotime($group->expect_start_time),
                'expect_end_time' => $group->expect_end_time,
                'fujian' => '[]',
            ];
            $data[] = $tmp;
        }

        $model = new ModelCheckTask();
        $model->saveAll($data);

        return true;
    }

    /*
     * 生成月计划.
     */
    /* private function genCheckTaskByMonth($days)
    {
        $maps = [
            'period' => 'month',
            'enable' => 1,
        ];
        $list = ModelCheckSchedule::where($maps)->select();
        if ($list->isEmpty()) {
            return false;
        }

        foreach ($list as $k => $v) {
            //已经生成过的任务组日期
            $task_group_dates = ModelCheckTaskGroup::where('check_scheduleid', $v->id)->column('date');

            $date = $this->date;
            while ($date <= $this->lastDay) {
                $month_day = date('j', strtotime($date));

                //没有生成过且   日期的天与计划天一致则生成(为了看当天是否是计划指定day)
                if (!in_array($date, $task_group_dates) && $month_day == $v->sn_day) {
                    $group = $this->saveTaskGroup($date, $v);

                    $this->saveTask($group, $v);
                }

                //下个月的指定日期
                // $sn_day = date('t', strtotime($date)) >= $v->sn_day ? $v->sn_day : date('t', strtotime($date));
                // $date = date("Y-m-$sn_day", strtotime('+1 month', strtotime(date('Y-m', strtotime($date)))));
                $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            }
        }
    } */

    /*
     * 生成周计划.
     */
    /* private function genCheckTaskByWeek($days)
    {
        $maps = [
            'period' => 'week',
            'enable' => 1,
        ];
        $list = ModelCheckSchedule::where($maps)->select();
        if ($list->isEmpty()) {
            return false;
        }

        foreach ($list as $k => $v) {
            //已经生成过的任务组日期
            $task_group_dates = ModelCheckTaskGroup::where('check_scheduleid', $v->id)->column('date');

            $date = $this->date;
            while ($date <= $this->lastDay) {
                $week_day = date('w', strtotime($date)) ?: 7;
                //没有生成过且  日期的星期与计划星期一致则生成(为了看当天是否是计划指定day)
                if (!in_array($date, $task_group_dates) && $week_day == $v->sn_day) {
                    $group = $this->saveTaskGroup($date, $v);

                    $this->saveTask($group, $v);
                }

                //下载的指定星期
                // $week_en = $this->week[$v->sn_day - 1];
                // $date = date('Y-m-d', strtotime("next $week_en", strtotime($date)));
                $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            }
        }
    } */

    /*
     * 生成日计划.
     */
    /* private function genCheckTaskByDay($days)
    {
        $maps = [
            'period' => 'day',
            'enable' => 1,
        ];
        $list = ModelCheckSchedule::where($maps)->select();
        if ($list->isEmpty()) {
            return false;
        }

        foreach ($list as $k => $v) {
            //已经生成过的任务组日期
            $task_group_dates = ModelCheckTaskGroup::where('check_scheduleid', $v->id)->column('date');

            $date = $this->date;
            while ($date <= $this->lastDay) {
                //没有生成过则生成
                if (!in_array($date, $task_group_dates)) {
                    $group = $this->saveTaskGroup($date, $v);

                    $this->saveTask($group, $v);
                }

                $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
            }
        }
    } */
}
