<?php

namespace app\common;

 /**
  * 本类为common目录中的基类，希望将来大家在common中的类都能继承这个类，这样会自动变成单例模式.
  *
  *请再子类中重写getInstance方法，代码参考本类中注释的getInstance方法
  */
 abstract class Base
 {
     protected static $instance;

     protected function __construct()
     {
     }

     abstract public static function getInstance();

     //下面注释的这个getInstance请一定要拷贝到子类中

     //  public static function getInstance()
    //  {
    //      if (empty(self::$instance)) {
    //          self::$instance = new self();
    //      }

    //      return self::$instance;
    //  }
 }
