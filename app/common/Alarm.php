<?php

namespace app\common;

use Exception;

/**
 * 告警类，使用方法请参考本目录下md文档.
 */
class Alarm extends Base
{
    /**接收人邮箱 */
    protected $receivers = ['gyl@tyll.net.cn'];
    /**项目名称 */
    protected $projectName = 'xxxx';

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    //告警，目前只有邮件告警
    public function send(string $function, array $data, Exception $e, string $title)
    {
        $time = date('Y-m-d H:i:s');
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $errData = json_encode($e->getTrace()[0], JSON_UNESCAPED_UNICODE);
        $url = \request()->url(true);
        $p = json_encode(input(), JSON_UNESCAPED_UNICODE);
        $sever_code = config('tyll.server_code');
        $body = <<<EOT
        所在主机：{$sever_code}，<br>
        报警方法：{$function}，<br>
        报警产生时间：{$time}，<br>
        原始url：{$url}，<br>
        接收参数：{$p}，<br>
        产生异常的数据信息：{$data}， <br>
        产生异常的上下文信息：{$errData}， <br>
        产生异常的提示信息：{$e->getMessage()}，    
EOT;

        foreach ($this->receivers as $mail) {
            $r = Mail::getInstance()->sendMail(
                $mail,
                $this->projectName.'项目组',
                "【{$this->projectName}】触发告警-".$sever_code.'-'.$title,
                $body
            );
        }

        return $r;
    }
}
