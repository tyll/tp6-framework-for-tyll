<?php

namespace app\common;

// use esign\esign_utils\HttpHelper;
// use esign\esign_constant\Config;
// use app\model\Corp as ModelCorp;
// use app\model\ContractTemplate as ModelContractTemplate;
// use app\model\User as ModelUser;
// use Exception;
use Stomp\Client;
use Stomp\Exception\StompException;
use Stomp\Network\Observer\ServerAliveObserver;
use Stomp\StatefulStomp;

/**
 * 卓虎智能继电器.
 *
 * 代码示例见app/command/amqp.php
 */
class Zhuohu extends Base
{
    private function __construct()
    {
        $aliyunKey = Aliyun::getInstance()->getInfo();
        $this->accessKeyId = $aliyunKey['accessKeyId'];
        $this->accessKeySecret = $aliyunKey['accessKeySecret'];
        $this->uid = $aliyunKey['uid'];
        $this->regionId = 'cn-shanghai';
        $this->amqpHost = "$this->uid.iot-amqp.$this->regionId.aliyuncs.com";
        $this->iotInstanceId = '';
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function Init()
    {
        //参数说明，请参见AMQP客户端接入说明文档。
        $accessKey = $this->accessKeyId;
        $accessSecret = $this->accessKeySecret;
        $consumerGroupId = 'DEFAULT_GROUP';
        $clientId = \think\facade\Config::get('tyll.tywyx.pid');
        //iotInstanceId：购买的实例请填写实例ID，公共实例请填空字符串""。

        $timeStamp = round(microtime(true) * 1000);
        //签名方法：支持hmacmd5，hmacsha1和hmacsha256。
        $signMethod = 'hmacsha1';
        //userName组装方法，请参见AMQP客户端接入说明文档。
        //若使用二进制传输，则userName需要添加encode=base64参数，服务端会将消息体base64编码后再推送。具体添加方法请参见下一章节“二进制消息体说明”。
        // $userName = $clientId
        //             .'|iotInstanceId='.''
        //             .',authMode=aksign'
        //             .',signMethod='.$signMethod
        //             .',consumerGroupId='.$consumerGroupId
        //             .',authId='.$accessKey
        //             .',timestamp='.$timeStamp
        //             .'|';
        $userName = $clientId.'|authMode=aksign'
                .',signMethod='.$signMethod
                .',timestamp='.$timeStamp
                .',authId='.$accessKey
                .',iotInstanceId='.$this->iotInstanceId
                .',consumerGroupId='.$consumerGroupId
                .'|';

        $signContent = 'authId='.$accessKey.'&timestamp='.$timeStamp;
        //计算签名，password组装方法，请参见AMQP客户端接入说明文档。
        $password = base64_encode(hash_hmac('sha1', $signContent, $accessSecret, $raw_output = true));
        //接入域名，请参见AMQP客户端接入说明文档。
        $client = new Client("ssl://{$this->amqpHost}:61614");
        $sslContext = ['ssl' => ['verify_peer' => true, 'verify_peer_name' => false]];
        $client->getConnection()->setContext($sslContext);

        //服务端心跳监听。
        $observer = new ServerAliveObserver();
        $client->getConnection()->getObservers()->addObserver($observer);
        //心跳设置，需要云端每30s发送一次心跳包。
        $client->setHeartbeat(0, 30000);
        $client->setLogin($userName, $password);
        try {
            $client->connect();
        } catch (StompException $e) {
            echo 'failed to connect to server, msg:'.$e->getMessage() , PHP_EOL;
        }
        //无异常时继续执行。
        $stomp = new StatefulStomp($client);
        $stomp->subscribe('/topic/#');

        return $stomp;
    }
}
