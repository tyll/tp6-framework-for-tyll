<?php

namespace app\common;

use app\model\Token as ModelToken;
use GuzzleHttp\Client;
use think\facade\Cache;
/**
 * 关于与微信的对接分两种
 * 1、获得用户的openid，这时候操作的自我代入设定是用户，换句话这里微信是考虑的Oauth2.0，交互的过程有3方,这时候交互的对象是oauth
 * 2、我方业务服务器与微信服务器通信调用微信的一些能力。这时候的交互是与abilityBaseUri交互
 */
class WeChat extends Base
{
    protected $appid;
    protected $appSecret;
    protected $tokenCacheKey='WeChatToken';

    protected $abilityBaseUri = "https://api.weixin.qq.com/cgi-bin/";
   
    protected function __construct()
    {
        parent::__construct();
        $this->appid = "";
        $this->appSecret = "";

    }
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 获取token
     */
    protected function getAccessToken()
    {
        return Cache::get($this->tokenCacheKey)??$this->resetToken();
    }


        /**
     * 充值token，直接向微信服务器充值
     */
    protected function resetToken()
    {
        $client = new Client([
            'base_uri' => $this->abilityBaseUri,
        ]);
        
        $query=["grant_type"=>'client_credential', "appid"=>$this->appid, "secret"=>$this->appSecret];
        $response = $client->get("token" , ['query'=>$query]);
        $body = $response->getBody()->getContents();//返回值是string
        echo $body;
        $body=json_decode($body);
        $this->isErr($body);
        $this->saveToken($body);
        return $body->access_token;
    }

    /**
     * 获取用户信息
     */
    public function getUserInfo(string $openid)
    {
       

        // $this->isErr($result);

        // return $result;
    }

        /**
     * 生成带参数的二维码
     * 
     * 返回的示例结果
     * "{"ticket":"gQG98DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyNHQxSm9oZy04NlAxUm9XUmh3MU8AAgTYP_xgAwSAOgkA",
     * "expire_seconds":604800,"url":"http://weixin.qq.com/q/024t1Johg-86P1RoWRhw1O"}"
     */
    public function makeGuanzhuQrcode(string $scene_str)
    {
        $client = new Client([
            'base_uri' => $this->abilityBaseUri,
        ]);
        
        $body=["expire_seconds"=>604800, "action_name"=>"QR_STR_SCENE", "action_info"=>["scene"=>["scene_str"=> 123]]];
        $header=['content-type'=>'application/json'];
        $response = $client->post("qrcode/create?access_token=".$this->getAccessToken() ,
             ['body'=>json_encode($body),'header'=>$header]);
        $body = $response->getBody()->getContents();
        return $body;
    }

  




    /*******************************************************************************************************************************************************************************/


    /**
     * 判断返回值
     */
    public function isErr($result)
    {
        if (isset($result->errcode) && $result->errcode != 0) {
            throw new \Exception('微信服务器报错：'.$result->errcode . ":" . $result->errmsg, 0);
        }

        return true;
    }
    /**
     * 保存token,此处没有保存数据库，通过缓存保存，如有需要保存数据库自行扩展
     */
    protected function saveToken(object $result)
    {
        Cache::set($this->tokenCacheKey, $result->access_token, $result->expires_in);
        return true;
    }

}
