<?php

namespace app\common;

/**
 * 桃园Curl类
 * @Author: godliu
 * @Date: 2020-09-01 11:56:00
 */
class TyCurl
{
    /**************************************************属性：允许外部设置**************************************************/
    // 地址
    private $url = '';

    // 参数
    private $params = '';
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    /**************************************************属性：不允许外部设置************************************************/

    /**
     * 设置地址
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * 设置参数
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * post发送数据
     */
    public function postUseCurl()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->params);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    /**
     * get发送数据
     */
    public function getUseCurl()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
    /**
     * get发送数据.
     */
    public  function get($url)
    {
        $this->url = $url ?? $this->url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
