<?php

namespace app\common;

use app\model\Conf as ModelConf;
use Qcloud\Cos\Client;

/**
 * 腾讯cos.
 *
 * 安装sdk   `composer require qcloud/cos-sdk-v5`
 */
class TencentCos
{
    const SECRETID = 'AKIDSr5IKCwy770iEJnCxb4TgigXHUXQelAP';
    const SECRETKEY = 'CC2hARWP9hUFtSr693mJesulRwX4Tmz3';

    const REGION = 'ap-chongqing';
    /**存储桶 */
    const BUCKET = 'bcxj';
    /**appid */
    const APPID = '1255346517';

    protected static $instance;

    /**秘钥信息 */
    protected $secretInfo;

    protected $client;

    public function __construct()
    {
        //设置秘钥信息
        $this->setSecretInfo();

        //初始化
        $this->client = new Client([
            'region' => self::REGION,
            'schema' => 'https', //协议头部，默认为http
            'credentials' => [
                'secretId' => $this->secretInfo['tencent_secret_id'],
                'secretKey' => $this->secretInfo['tencent_secret_key'],
            ],
        ]);
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 上传文件.
     *
     * @param string $path      上传的文件路径
     * @param string $save_path cos存储路径
     */
    public function upload(string $path, string $save_path): array
    {
        //存储桶名称
        $bucket_name = $this->getBucketName();
        //读取文件流
        $file = file_get_contents($path);

        //上传
        $res = $this->client->upload($bucket_name, $save_path, $file);

        $data = [];
        foreach ($res as $k => $v) {
            $data[$k] = $v;
        }
        $data['url'] = 'https://'.$data['Location'];

        return $data;
    }

    /**********************************************************************. */

    /**
     * 存储桶名称.
     */
    public function getBucketName()
    {
        return self::BUCKET.'-'.self::APPID;
    }

    /**
     * 设置秘钥信息.
     */
    private function setSecretInfo()
    {
        if (!empty(self::SECRETID) && !empty(self::SECRETKEY)) {
            $this->secretInfo = [
                'tencent_secret_id' => self::SECRETID,
                'tencent_secret_key' => self::SECRETKEY,
            ];
        } else {
            $secret_info = $this->getSecretInfo();
            $this->secretInfo = $secret_info;
        }
    }

    /**
     * 秘钥信息.
     */
    private function getSecretInfo()
    {
        $keys = ['tencent_secret_id', 'tencent_secret_key'];
        $res = ModelConf::where('k', 'in', $keys)->select();

        $data = [];
        foreach ($res as $k => $v) {
            $data[$v->k] = $v->v;
        }

        return $data;
    }
}
