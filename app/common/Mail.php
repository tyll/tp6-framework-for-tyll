<?php

namespace app\common;

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    protected $host = 'smtp.mxhichina.com';
    protected $fromAddress = 'service@tyll.net.cn';
    protected $fromName = '桃园立林';
    protected $fromPassword = '';

    public static $instance;

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 发送邮件.
     *
     * @param $tomail  接收邮件者邮箱
     * @param $name    接收邮件者名称
     * @param $subject 邮件主题
     * @param $body    邮件内容
     *
     * @return bool
     */
    public function sendMail($tomail, $name, $subject, $body)
    {
        try {
            $mail = new PHPMailer();
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $this->host;                   // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $this->fromAddress;              // SMTP username
            $mail->Password = $this->fromPassword;                        // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($this->fromAddress, $this->fromName);
            $mail->addAddress($tomail, $name);                    // Add a recipient

            $mail->isHTML(true);                           // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            $r = $mail->send();
            //失败抛出异常
            if (!$r) {
                throw new \Exception($mail->ErrorInfo);
            }

            return $r;
        } catch (\Throwable $th) {
            writeLog("邮件发送失败\t".$th->getMessage());
        }
    }
}
