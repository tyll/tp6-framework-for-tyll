<?php

namespace app\common;

use app\model\Message as ModelMessage;
use app\obj\Http;

/**
 * unipush类  v2版本.
 *
 * 使用说明：
 *  1.安装sdk：使用composer require GetuiLaboratory/getui-pushapi-php-client-v2:dev-master
 *      或者composer.json的require:中添加"getuilaboratory/getui-pushapi-php-client-v2": "dev-master"
 */
class UniPush
{
    /*透传消息类型*/
    const TRANSMISSION_TYPE = 1;
    /*通知消息类型*/
    const NOTIFICATION_TYPE = 2;

    //个推对象
    protected $gtModel;
    protected $appId;
    protected $appKey;
    protected $clientId;
    protected $masterSecret;
    protected $msg;
    protected static $instance;
    protected $baseUrl = 'https://restapi.getui.com';

    protected $pushMethod = [
        'single' => ['name' => 'pushToSingleByCid', 'url' => '/push/single/cid'],
        'all' => ['name' => 'pushAll', 'url' => '/push/all'],
    ];

    protected function __construct()
    {
        $this->appId = 'yc2Lpl2WT78Uli5c7IPeI5';
        $this->appKey = 'UYAnbs4MLc6Dh6RsZTBEm2';
        $this->masterSecret = 'mpKA71YYgBAMAmGzI1l3l';

        $this->gtModel = new \GTClient($this->baseUrl, $this->appKey, $this->appId, $this->masterSecret);
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 推送消息.
     *
     * @param string       $clientid 接收消息的客户端id
     * @param ModelMessage $msg      消息的model
     */
    public function pushMsg(string $clientid, ModelMessage $msg)
    {
        try {
            $this->msg = $msg;

            //单推
            return $this->push($clientid);
        } catch (\Throwable $th) {
            \app\common\Alarm::getInstance()->send(__METHOD__, $msg, $th, 'APP消息发送异常');

            return false;
        }

        return true;
    }

    protected function push($clientid)
    {
        if (!$clientid || strlen((string) $clientid) > 32) {
            return false;
        }
        $push = new \GTPushRequest();
        $push->setRequestId(time());
        //个推设置推送参数
        $message = $this->getPushMessage(self::TRANSMISSION_TYPE);
        //推送条件设置
        $setting = $this->getSetting();
        //厂商消息内容
        $push_channel = $this->getPushChannel();

        //添加参数
        $push->setPushMessage($message); //个推消息
        $push->setPushChannel($push_channel); //厂商消息
        $push->setSettings($setting); //推送条件
        //判断单推还是全推
        if ($clientid != 'all') {
            $push->setCid($clientid); //clientid
            $method_name = 'pushToSingleByCid';
            $url = '/push/single/cid';
        } else {
            $push->setGroupName($this->msg->title); //任务组名
            $method_name = 'pushAll';
            $url = '/push/all';
        }
        //处理返回结果
        $result = $this->gtModel->pushApi()->$method_name($push);
        //Http::getInstance()->save($this->baseUrl.$url, Http::METHOD_POST, $push->getApiParam(), $result);

        //检查是否有错误
        $this->checkErr($result);

        return true;
    }

    /**
     * 错误抛出异常.
     */
    protected function checkErr(array $result)
    {
        if (isset($result['code']) && $result['code'] != 0 && $result['code'] != 20001) {
            throw new \Exception($result['code'].':'.($result['msg'] ?? '消息推送出错'));
        }

        return true;
    }

    /**
     * 个推设置推送参数.
     */
    protected function getPushMessage($type)
    {
        //个推设置推送参数
        $message = new \GTPushMessage();
        if ($type == self::TRANSMISSION_TYPE) {
            //透传消息内容
            $transmission = $this->getTransMission();
            $message->setTransmission($transmission);
        } else {
            //消息通知
            $notify = $this->getPushMessageNotification();
            $message->setNotification($notify);
        }

        return $message;
    }

    /**
     * 推送条件设置.
     */
    protected function getSetting()
    {
        //厂商通道策略
        $strategy = new \GTStrategy();
        $strategy->setDefault(\GTStrategy::STRATEGY_THIRD_FIRST); //优先厂商消息
        $setting = new \GTSettings();
        $setting->setStrategy($strategy);

        return $setting;
    }

    /**
     * 获取厂商消息内容.
     */
    protected function getPushChannel()
    {
        //厂商消息内容
        $push_channel = new \GTPushChannel();
        //设置安卓消息内容
        $android = $this->getChannelAndroid(self::NOTIFICATION_TYPE);
        $push_channel->setAndroid($android);
        //设置ios消息内容
        $ios = $this->getChannelIos();
        $push_channel->setIos($ios);

        return $push_channel;
    }

    /**
     * 获取安卓厂商消息内容.
     *
     * @param int $type 消息类型
     */
    protected function getChannelAndroid(int $type)
    {
        $ups = new \GTUps();
        if ($type == self::TRANSMISSION_TYPE) {
            //透传消息
            $transmission = $this->getTransMission();
            $ups->setTransmission($transmission);
        } else {
            //通知消息
            $notification = $this->getChannelAndroidNotification();
            //ups设置消息内容
            $ups->setNotification($notification);
        }
        //设置ups
        $android = new \GTAndroid();
        $android->setUps($ups);

        return $android;
    }

    /**
     * 获取ios厂商消息.
     */
    protected function getChannelIos()
    {
        //通知消息
        $alert = new \GTAlert();
        $alert->setTitle($this->msg->title);
        $alert->setBody($this->getMsgContent());
        //设置aps的alert和推送方式
        $aps = new \GTAps();
        $aps->setAlert($alert);
        $aps->setContentAvailable(0); //0:普通推送方式 1:静默消息推送
        //设置aps、type和payload
        $payload = json_encode($this->msg->payload, JSON_UNESCAPED_UNICODE);
        $ios = new \GTIos();
        $ios->setType('notify');
        $ios->setAps($aps);
        $ios->setPayload($payload);

        return $ios;
    }

    /**
     * 获取透传消息内容.
     */
    protected function getTransMission(): string
    {
        $data = [
            'title' => $this->msg->title,
            'content' => $this->getMsgContent(),
            'payload' => $this->msg->payload,
        ];

        return json_encode($data);
    }

    /**
     * 获取个推通知消息内容 (未使用).
     */
    protected function getPushMessageNotification()
    {
        $title = $this->msg->title;
        $content = $this->getMsgContent();
        //$intent = "intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;package=com.imandiao.app;component=com.imandiao.app/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title=cs;S.content=cs2;S.payload={a:1};end";
        $notification = new \GTNotification();
        $notification->setTitle($title);
        $notification->setBody($content);
        $notification->setClickType('startapp');
        //$notification->setIntent($intent);

        return $notification;
    }

    /**
     * 获取安卓厂家通知消息内容.
     */
    protected function getChannelAndroidNotification()
    {
        $title = $this->msg->title;
        $content = $this->getMsgContent();
        $payload = json_encode($this->msg->payload, JSON_UNESCAPED_UNICODE);
        $intent = 'intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;'.
            'package=com.imandiao.app;component=com.imandiao.app/io.dcloud.PandoraEntry;S.UP-OL-SU=true;'.
            "S.title={$title};S.content={$content};S.payload={$payload};end";
        $notification = new \GTNotification();
        $notification->setTitle($title);
        $notification->setBody($content);
        $notification->setClickType('intent');
        $notification->setIntent($intent);

        return $notification;
    }

    /**
     * 消息内容.
     */
    protected function getMsgContent()
    {
        return str_replace("\n", '', $this->msg->content);
    }
}
