<?php

namespace app\common;

use app\model\Conf as ModelConf;

/**
 * 微信小程序.
 */
class WeChatApplet
{
    //小程序信息
    const APP_ID = '';
    const APP_SECRET = '';

    /**小程序码宽度 */
    const CODE_WIDTH = 400;

    protected static $instance;

    protected $appletInfo;

    public function __construct()
    {
        if (!empty(self::APP_ID) && !empty(self::APP_SECRET)) {
            $this->appletInfo = [
                'appid' => self::APP_ID,
                'app_secret' => self::APP_SECRET,
            ];
        } else {
            $secret_info = $this->getAppletInfo();
            $this->appletInfo = $secret_info;
        }
    }

    /**
     * 小程序信息.
     */
    private function getAppletInfo()
    {
        $keys = ['xcx_appid', 'xcx_appsecret'];
        $res = ModelConf::where('k', 'in', $keys)->select();

        $data = [];
        foreach ($res as $k => $v) {
            $data[$v->k] = $v->v;
        }

        return $data;
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 获取小程序openid.
     */
    public function getOpenid($code)
    {
        $base_url = 'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code';
        $url = sprintf($base_url, $this->appletInfo['appid'], $this->appletInfo['app_secret'], $code);

        $res = $this->request('get', $url);
        if (!isset($res->openid) || empty($res->openid)) {
            throw new \Exception('请求失败，请重新进入');
        }

        $data = [
            'openid' => $res->openid,
            'session_key' => $res->session_key,
        ];

        return $data;
    }

    /**
     * 获取access_token.
     */
    public function getAccessToken()
    {
        $access_token = $this->getCacheAccessToken();
        if ($access_token) {
            return $access_token;
        }

        $base_url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s';
        $url = sprintf($base_url, $this->appletInfo['appid'], $this->appletInfo['app_secret']);

        $res = $this->request('get', $url);
        if (empty($res->access_token)) {
            throw new \Exception('access_token获取失败');
        }
        $this->saveToken($res);

        return $res->access_token;
    }

    /**
     * 获取电话号码
     */
    public function getPhoneNumber($code)
    {
        $base_url = 'https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s';
        $url = sprintf($base_url, $this->getAccessToken());

        $data = [
            'code' => $code,
        ];
        $res = $this->request('post', $url, $data);
        if (empty($res->phone_info)) {
            throw new \Exception('获取手机号失败,请重新获取');
        }

        return  $res->phone_info;
    }

    /**
     * 获取小程序码（有个数限制）.
     *
     * @param string $path   程序页面路径
     * @param array  $option 非必填参数env_version，width，auto_color，line_color，is_hyaline
     */
    public function getAppletCode(string $path, array $option = [])
    {
        $base_url = 'https://api.weixin.qq.com/wxa/getwxacode?access_token=%s';
        $url = sprintf($base_url, $this->getAccessToken());

        $data = [
            'path' => $path,
        ];
        $data = array_merge($data, $option);
        $res = $this->request('post', $url, $data);

        if (empty($res) || $res == false) {
            throw new \Exception('生成小程序码失败');
        }

        return $res;
    }

    /**
     * 获取小程序码（无个数限制）.
     *
     * @param string $scene  参数
     * @param string $page   程序页面路径
     * @param array  $option 非必填参数checkPath,env_version，width，auto_color，line_color，is_hyaline
     */
    public function getAppletCodeUnlimit(string $scene, string $page = '', array $option = [])
    {
        $base_url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s';
        $url = sprintf($base_url, $this->getAccessToken());

        $data = [
            'scene' => $scene,
        ];
        if ($page) {
            $data['page'] = $page;
        }
        if (empty($option['width'])) {
            $option['width'] = self::CODE_WIDTH;
        }
        $data = array_merge($data, $option);
        $res = $this->request('post', $url, $data);

        if (empty($res) || $res == false) {
            throw new \Exception('生成小程序码失败');
        }

        return $res;
    }

    /**
     * 发送模板消息.
     *
     * @param string $openid     小程序openid或对应公众号openid
     * @param string $templateid 消息模板id
     * @param array  $msg_data   模板参数
     * @param string $page_path  小程序页面地址
     * @param string $url        跳转url
     */
    public function sendTemplateMsg(string $openid, string $templateid, array $msg_data, string $page_path = '', string $url = '')
    {
        $base_url = 'https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token=%s';
        $url = sprintf($base_url, $this->getAccessToken());

        $data = [
            'touser' => $openid,
            'mp_template_msg' => [
                'appid' => $this->gzhAppid, //公众号appid
                'template_id' => $templateid, //模板id
                'url' => $url, //跳转url
                'miniprogram' => [
                    'appid' => $this->appletInfo['appid'], //小程序appid
                    'pagepath' => $page_path, //小程序页面
                ],
                'data' => $msg_data, //模板参数
            ],
        ];
        $res = $this->request('post', $url, $data);

        return $res;
    }

    /**********************************************************************************************. */

    /**
     * 发起curl请求
     */
    private function request($method, $url, $data = [])
    {
        $method = $method.'UseCurl';

        $curl = new TyCurl();
        $curl->setUrl($url);
        $curl->setParams($data);
        $res = $curl->$method();

        return json_decode($res) ?: $res;
    }

    /**
     * 保存access_token.
     */
    private function saveToken($token_info)
    {
        $k = 'xcx_access_token';
        $v = [
            'access_token' => $token_info->access_token,
            'expires_in' => $token_info->expires_in,
            'expire_time' => time() + $token_info->expires_in,
        ];

        $r = ModelConf::where('k', $k)->find();
        if ($r) {
            $r->v = json_encode($v, JSON_UNESCAPED_UNICODE);
            $r->save();
        } else {
            $data = [
                'k' => $k,
                'v' => json_encode($v, JSON_UNESCAPED_UNICODE),
                'remark' => '小程序access_token',
            ];
            ModelConf::create($data);
        }
    }

    /**
     * 获取缓存的token.
     */
    private function getCacheAccessToken()
    {
        $k = 'xcx_access_token';
        $r = ModelConf::where('k', $k)->value('v');
        if (!$r) {
            return false;
        }
        $r = json_decode($r);
        if ($r->expire_time < time()) {
            return false;
        }

        return $r->access_token;
    }
}
