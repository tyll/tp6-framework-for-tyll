<?php

namespace app\common;

/**
 * 桃园Token类.
 *
 * @Author: godliu
 * @Date: 2020-09-01 11:56:00
 */
class TyToken
{
    /**************************************************属性：允许外部设置**************************************************/
    // tokenid
    private $tokenid = '';

    // token
    private $token = '';

    // day
    private $day = 1;
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    // 密钥
    private $key = '';
    /**************************************************属性：不允许外部设置************************************************/

    /**
     * 设置tokenid.
     */
    public function setTokenid($tokenid)
    {
        $this->tokenid = $tokenid;
    }

    /**
     * 设置token.
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * 设置day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**************************************************业务函数***********************************************************/
    public function __construct()
    {
        $this->key = \think\facade\Config::get('tyll.token_secret_key');
    }

    /**
     * 获取token.
     */
    public function getToken()
    {
        $expire_time = time() + 3600 * 24 * $this->day;
        $sign = sha1($this->tokenid . $expire_time . $this->key);
        $token = $this->tokenid . '.' . $expire_time . '.' . $sign;
        return $token;
    }

    /**
     * 检查token.
     */
    public function checkToken()
    {
        if (empty($this->token)) {
            throw new \Exception('token缺失', -1);
        }
        $token_arr = explode('.', $this->token);
        if (count($token_arr) != 3) {
            throw new \Exception('token规则错误，请重新获取', -1);
        }
        $tokenid = $token_arr[0];
        $expire_time = $token_arr[1];
        $sign = $token_arr[2];
        $now_time = time();
        if ($now_time > $expire_time) {
            throw new \Exception('token超时，请重新获取', -1);
        }
        $my_sign = sha1($tokenid . $expire_time . $this->key);
        if ($my_sign != $sign) {
            throw new \Exception('token验证失败，请重新获取', -1);
        }
        return $tokenid;
    }
}
