<?php

namespace app\common;

use app\model\Contract as ModelContract;
use app\model\ContractTemplate as ModelContractTemplate;
use app\model\Corp as ModelCorp;
use app\model\User as ModelUser;
use app\obj\Esign as ObjEsign;
use esign\esign_constant\Config;
use esign\esign_utils\HttpHelper;
use Exception;

/**
 * e签宝.
 *
 * 使用说明见README.md
 */
class ESign
{
    protected $appid;
    protected $appSecret;
    protected $faceauthMode = ['ZHIMACREDIT', 'TENCENT', 'ESIGN'];
    protected $model = 'smlopenapi';
    /**签署顺序 */
    protected $signOrder = 1;

    protected $errMsg = [
        30503056 => '姓名或身份证号码有误',
        30501002 => '姓名、身份证、手机号匹配失败',
    ];

    /**
     * 平台方企业印章id.
     */
    protected $platform_seal_id;
    protected $flowid;
    protected $fileid;
    protected $initiatorAuthorizedAccountId; //发起方orgid
    protected $yf_posbean; //发起方印章位置

    public function __construct()
    {
        $eqb = json_decode(\app\model\Conf::where('key', 'esign')->value('value'));
        $this->appid = $eqb->appid;
        $this->appSecret = $eqb->appsecret;
        $this->model = $eqb->env == 1 ? 'smlopenapi' : 'openApiUrl';
    }

    public function getAppid()
    {
        return $this->appid;
    }

    public function getAppSecret()
    {
        return $this->appSecret;
    }

    public function getModel()
    {
        return $this->model;
    }

    /**
     * 创建个人账户.
     *
     * @param object $user 账户信息
     */
    public function createUser(object $user)
    {
        $user->third_party_userid = $user->third_party_userid ?? ($user->id.$user->name.$user->idcard_no);
        $param = [
            'thirdPartyUserId' => $user->third_party_userid,
            'name' => $user->name,
            'idType' => 'CRED_PSN_CH_IDCARD',
            'idNumber' => $user->idcard_no,
            'mobile' => $user->tel,
            'email' => '',
        ];

        $result = json_decode(HttpHelper::doPost(Config::$config[$this->model].Config::$urlMap['CREATE_ACCOUNT_BY_THIRD_USER_ID'], $param), true);
        if (isset($result['code']) && $result['code'] != 0 && $result['code'] != 53000000) {
            writeLog(__METHOD__."\tcode:".$result['code']."\tmessage:".$result['message']);
            throw new Exception('esign_'.$result['code'].'：'.$result['message'], 0);
        }

        return $result['data']['accountId'];
    }

    /**
     * 创建机构账户.
     */
    public function createOrg(object $corp)
    {
        $data = [
            'thirdPartyUserId' => $corp->third_party_userid, //创建机构账号的唯一标识
            'creator' => $corp->creator, //创建人的个人账号id
            'name' => $corp->name,
            'idType' => 'CRED_ORG_USCC',
            'idNumber' => $corp->tyshxydm,
        ];

        $result = json_decode(HttpHelper::doPost(Config::$config[$this->model].Config::$urlMap['CREATE_ORGAN_ACCOUNT_BY_THIRD_USER_ID'], $data), true);
        if (isset($result['code']) && $result['code'] != 0 && $result['code'] != 53000000) {
            throw new Exception('esign_'.$result['code'].'：'.$result['message'], 0);
        }

        return $result['data']['orgId'];
    }

    /***********************************************************************************实名认证****************************************************************************/

    /**
     * 人脸实名认证
     *
     * @param string $callbackUrl 重定向地址
     * @param int    $type        1.支付宝芝麻信用认证  2.腾讯微众银行认证  3.e签宝认证
     */
    public function face(string $accountid, int $userid, string $callbackUrl, int $type = 1)
    {
        $param = [
            'faceauthMode' => $this->faceauthMode[$type - 1], //人脸认证方式 TENCENT腾讯微众银行认证 , ZHIMACREDIT支付宝芝麻信用认证
            'repetition' => true, //是否允许账号重复实名
            'callbackUrl' => $callbackUrl, //认证完成后业务重定向地址
            'contextId' => $userid, //对接方业务上下文id，将在异步通知及跳转时携带返回对接方
            'notifyUrl' => request()->root(true).'/callback.Esign/notify', //认证结束后异步通知地址
        ];
        $result = json_decode(HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['INDIVIDUAL_API_FACE'], $accountid), $param), true);

        $this->isErr($result);

        //保存认证记录
        //ObjEsign::getInstance()->saveSmrz($userid, $accountid, $type, '', $result['data']);

        return ['url' => $result['data']['authUrl'], 'flowid' => $result['data']['flowId']];
    }

    /**
     * 运营商3要素实名认证
     *
     * @param object     $user 人员的model
     * @param int|string $tel  电话号码
     */
    public function telAuth(ModelUser $user, $tel = null)
    {
        $accountid = $this->createUser($user);
        $data = [
            'mobileNo' => $tel ?? $user->tel, //手机号,仅限中国大陆3大运营商开通的手机号
            'repetition' => true, //是否允许账号重复实名
            'contextId' => $user->id, //对接方业务上下文id
            'notifyUrl' => request()->root(true).'/callback.Esign/notify', //认证结束后异步通知地址
            'grade' => 'STANDARD', //指定是否使用运营商3要素信息比对详情版      ADVANCED-详情版   STANDARD-普通版
        ];

        $result = json_decode(HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['INDIVIDUAL_API_TELECOM3FACTOR'], $accountid), $data), true);

        $this->isErr($result);

        //保存认证记录
        //ObjEsign::getInstance()->saveSmrz($user->id, $accountid, 4, $tel ?? $user->tel, $result['data']);

        return ['flowid' => $result['data']['flowId']];
    }

    /**
     * 运营商短信验证码校验.
     *
     * @param string     $flowid 流程id
     * @param int|string $code   运营商3要素实名认证收到的6位验证码
     */
    public function telCodeAuth(string $flowid, $code)
    {
        $data = [
            'authcode' => $code,
        ];
        $result = json_decode(HttpHelper::doPut(Config::$config[$this->model].sprintf(Config::$urlMap['INDIVIDUAL_API_TELECOM3FACTOR_CODE'], $flowid), $data), true);

        $this->isErr($result);

        return $result['message'];
    }

    /**
     * 查询认证状态
     *
     * @param string $flowId 认证流程id
     */
    public function getFaceStatus(string $flowId)
    {
        $result = json_decode(HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['INDIVIDUAL_API_FACE_QUERY'], $flowId), []), true);
        if (isset($result['code']) && $result['code'] != 0) {
            throw new Exception('esign_'.$result['code'].'：'.$result['message'], 0);
        }

        return ['status' => $result['data']['status'], 'message' => $result['data']['message']];
    }

    /****************************************************************签署合同************************************************************/

    /**
     * 分步发起签署流程.
     *
     * @param ModelContract $contract    合同的model
     * @param string        $path        待签署的pdf文件路径
     * @param string        $redirectUrl 签署完成重定向地址
     */
    public function setupFlow(ModelContract $contract, string $path, $redirectUrl = null)
    {
        //线下签署不发起e签宝流程
        if ($contract->sign_channel == 'offline') {
            return ['flowid' => '', 'url' => ''];
        }
        $title = $contract->contract_name;
        //发起人账户(乙方)
        $initiatorAccountId = $this->getInitiatorAccountId($contract->create_userid);
        //发起人所属机构(乙方)
        $initiatorAuthorizedAccountId = $this->getInitiatorOrgId($contract->yf_corpid);

        //签署方(甲方)
        $signer = $this->getSignerInfo($contract->jf_corpid);
        //签署人个人账户(甲方)
        $signerAccountId = $signer['accountid'];
        //签署人所属机构(甲方)
        $authorizedAccountId = $signer['orgid'];
        $tmpid = $contract->type;
        if ($contract->type == 4 && $contract->extend_obj->contract_goods_code == 'kjwf#gxrd') {
            $tmpid = 44;
        }
        $contract_temp = ModelContractTemplate::where('id', $tmpid)->find();
        //发起方印章位置
        $yf_posbean = $contract_temp->yf_pos;
        $this->yf_posbean = $yf_posbean;
        //签署方印章位置
        $jf_posbean = $contract_temp->jf_pos;

        //创建流程
        $flowid = $this->createFlow($title, $initiatorAccountId, $initiatorAuthorizedAccountId, $contract->id, $redirectUrl);
        //上传待签署文件
        $fileid = $this->upload($path);
        $this->fileid = $fileid;
        //流程添加文档
        $this->addFile($flowid, $fileid);
        //平台方签署区域
        //$this->platformSign($flowid, $fileid, $yf_posbean);
        $this->autoSign();
        //$this->handSign($flowid, $fileid, $initiatorAccountId, $yf_posbean, $initiatorAuthorizedAccountId);
        //签署方区域
        $this->handSign($flowid, $fileid, $signerAccountId, $jf_posbean, $authorizedAccountId);

        //开启流程
        $this->flowStart($flowid);
        //获取签署地址
        $url = $this->getSignUrl($flowid, $signerAccountId);

        return ['flowid' => $flowid, 'url' => $url];
    }

    /**
     * 流程文档下载.
     *
     * @param string $flowid 流程id
     */
    public function downloadFile(string $flowid)
    {
        $result = HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['DOWNLOAD_FILE'], $flowid), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['data']['docs'][0]['fileUrl'];
    }

    /**
     * 上传待签署文件.
     *
     * @param string $file_path 要上传文件的路径
     */
    public function upload(string $file_path)
    {
        $data = [
            'fileName' => basename($file_path), //文件名称 必须带上文件扩展名
            'fileSize' => filesize($file_path), //文件大小，单位byte
            'convert2Pdf' => false, //是否转换成pdf文档，默认false
            'contentType' => 'application/pdf', //目标文件的MIME类型，支持：1.application/octet-stream 2.application/pdf
            'contentMd5' => $this->getContentBase64Md5($file_path), //先获取文件MD5的二进制数组（128位），再对这个二进制进行base64编码。
        ];
        //获取上传地址
        $return_content = HttpHelper::doPost(Config::$config[$this->model].Config::$urlMap['FILE_UPLOAD'], $data);
        $result = (array) json_decode($return_content, true);

        $this->isErr($result);

        //上传文件
        $this->upLoadFile($result['data']['uploadUrl'], $file_path);

        return $result['data']['fileId'];
    }

    /**
     * 获取pdf详情.
     */
    public function getFileDetail($fileid)
    {
        $return_content = HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['FILE_DETAIL'], $fileid), null);
        $result = (array) json_decode($return_content, true);
        $this->isErr($result);

        return $result['data'];
    }

    /**
     * 签署流程归档.
     *
     * @param string $flowid 流程id
     */
    public function flowArchive(string $flowid)
    {
        $result = HttpHelper::doPut(Config::$config[$this->model].sprintf(Config::$urlMap['FLOW_ARCHIVE'], $flowid), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result;
    }

    /**
     * 创建图片印章.
     *
     * @param string $accountid 个人/机构账户id
     * @param string $seal_url  印章图片
     */
    public function setSealImg(string $accountid, string $seal_url): string
    {
        $data = [
            'type' => 'BASE64', //印章数据类型，BASE64：base64格式
            'data' => base64_encode(file_get_contents($seal_url)), //印章数据，目前只支持png格式，base64格式字符串，不包含格式前缀
            'transparentFlag' => 'true', //是否对图片进行透明化处理，默认false。对于有背景颜色的图片，建议进行透明化处理，否则可能会遮挡文字
            /* 'height' => '160',
            'width' => '160', */
        ];
        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['IMAGE_SEAL'], $accountid), $data);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['data']['sealId'];
    }

    /**
     * 删除机构印章.
     *
     * @param string $orgid  机构id
     * @param string $sealid 印章id
     */
    public function deleteOrgSeal(string $orgid, string $sealid)
    {
        $result = HttpHelper::doDelete(Config::$config[$this->model].sprintf(Config::$urlMap['DELETE_ORG_SEAL'], $orgid, $sealid), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['message'];
    }

    /**
     * 签署流程查询.
     */
    public function querySignFlow($flowid)
    {
        $result = HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['QUERY_SIGN_FLOW'], $flowid, ), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result;
    }

    /**
     * 获取签署地址
     *
     * @param string $flowid    流程id
     * @param string $accountid 签署人的个人账户id
     */
    protected function getSignUrl(string $flowid, string $accountid)
    {
        $data = [
            'accountId' => $accountid,
            'organizeId' => 0,
        ];
        $return_content = HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['GET_SIGNURL'], $flowid).'?'.http_build_query($data), null);
        $result = (array) json_decode($return_content, true);
        $this->isErr($result);

        return $result['data']['shortUrl'];
    }

    /**
     * 创建签署流程.
     *
     * @param string $title      流程主题
     * @param string $accountid  发起人个人账户id
     * @param string $orgid      发起方机构账户id
     * @param int    $contractid 回调通知参数
     */
    protected function createFlow(string $title, string $accountid, string $orgid = null, int $contractid, string $redirectUrl = null)
    {
        $data = [
            'businessScene' => $title, //文件主题 名称不支持以下9个字符：/ \ : * " < > | ？
            'autoArchive' => 'true', //是否自动归档
            'configInfo' => [ //任务配置信息
                'noticeType' => '1', //通知方式，逗号分割，1-短信，2-邮件 。默认值1，
                'signPlatform' => 1, //签署平台，逗号分割，1-开放服务h5，2-支付宝签 ，默认值1，2
                'noticeDeveloperUrl' => request()->root(true).'/callback.Esign/flowFinish?id='.$contractid, //回调通知地址
                'redirectUrl' => $redirectUrl, //签署完成重定向地址,默认签署完成停在当前页面
            ],
            'initiatorAccountId' => $accountid, //发起人账户id，如不传，默认由对接平台发起
            'initiatorAuthorizedAccountId' => $orgid, //发起方主体id，如不传，则默认是对接平台
            'signValidity' => strtotime('+14 day') * 1000, //签署有效截止日期,毫秒 超过签署有效截止时间，则无法继续签署
        ];
        $return_content = HttpHelper::doPost(Config::$config[$this->model].Config::$urlMap['SIGN_FLOWS'], $data);
        $result = (array) json_decode($return_content, true);
        $this->isErr($result);
        $data2 = $result['data'];
        //flowId作为$data2的第一个关键字
        $flowId = $data2['flowId'];
        $this->flowid = $flowId;

        return $flowId;
    }

    /***
     * 流程文档添加
     *
     * @param string $flowid 流程id
     * @param string $fileid 待签署文件id
     */
    protected function addFile(string $flowid, string $fileid)
    {
        $data = [
            'docs' => [ //文档列表数据
                ['fileId' => $fileid],
            ],
        ];
        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['ADD_DOCUMENTS'], $flowid), $data);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['message'];
    }

    /**
     * 签署方手动签署区域
     *
     * @param string $flowid    流程id
     * @param string $fileid    待签署文件id
     * @param string $accountid 签署人个人账户id
     * @param array  $pos_bean  签署区位置信息
     * @param string $orgid     签署人所属机构账户id
     */
    protected function handSign(string $flowid, string $fileid, string $accountid, array $pos_bean, string $orgid)
    {
        $data = [
            'signfields' => [ //签署区列表数据
                [
                    'order' => $this->signOrder, //签署区顺序，默认1,且不小于1，顺序越小越先处理
                    'fileId' => $fileid, //文件file id
                    'signerAccountId' => $accountid, //签署操作人个人账号标识
                    'authorizedAccountId' => $orgid, //签约主体账号标识
                    'actorIndentityType' => $orgid ? 2 : null, //是否企业盖章
                    'signType' => '1', //签署类型，0-不限，1-单页签署，2-骑缝签署，默认1
                    'posBean' => [ //签署区位置信息 。signType为0时，本参数无效； signType为1时, 页码和XY坐标不能为空； signType为2时, 页码和Y坐标不能为空
                        'posPage' => $pos_bean['page'] > 0 ? $pos_bean['page'] : 1, //页码信息，当签署区signType为2时, 页码可以'-'分割, 其他情况只能是数字
                        'posX' => $pos_bean['x'] >= 0 ? $pos_bean['x'] : 100, //x坐标
                        'posY' => $pos_bean['y'] >= 0 ? $pos_bean['y'] : 100, //y坐标
                    ],
                ],
            ],
        ];
        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['HAND_SIGN'], $flowid), $data);
        $result = (array) json_decode($result, true);

        $this->isErr($result);
        ++$this->signOrder;

        return $result['data'];
    }

    /**
     * 签署方自动签章.
     */
    protected function autoSign()
    {
        $data = [
            'signfields' => [ //签署区列表数据
                [
                    'fileId' => $this->fileid ?? '', //文件file id
                    'authorizedAccountId' => $this->initiatorAuthorizedAccountId, //签约主体账号标识
                    'order' => 1, //签署区顺序，默认1,且不小于1
                    'posBean' => [ //签署区位置信息,
                        'posPage' => $this->yf_posbean['page'] > 0 ? $this->yf_posbean['page'] : 1, //页码信息，当签署区signType为2时, 页码可以'-'分割, 其他情况只能是数字
                        'posX' => $this->yf_posbean['x'] >= 0 ? $this->yf_posbean['x'] : 500, //x坐标
                        'posY' => $this->yf_posbean['y'] >= 0 ? $this->yf_posbean['y'] : 100, //y坐标
                    ],
                    'sealId' => $this->platform_seal_id, //印章id ，如不传，则采用账号下的默认印章
                    'signType' => 1, //签署类型， 1-单页签署，2-骑缝签署，默认1
                ],
            ],
        ];

        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['AUTO_SIGN'], $this->flowid), $data);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['data'];
    }

    /**
     * 签署方静默签署.
     */
    protected function signAuth()
    {
        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['SIGN_AUTH'], $this->initiatorAuthorizedAccountId), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['message'];
    }

    /**
     * 添加平台方自动盖章签署区.
     *
     * @param string $flowid   流程id
     * @param array  $pos_bean 签署区位置信息
     */
    protected function platformSign(string $flowid, string $fileId, array $pos_bean)
    {
        $data = [
            'signfields' => [ //签署区列表数据
                [
                    'order' => '1', //签署顺序，默认1,
                    'fileId' => $fileId, //文件file id
                    'sealId' => $this->platform_seal_id ?? '', //印章id， 仅限企业公章，暂不支持指定企业法定代表人印章  ，如不传，则采用账号下的默认印章
                    'signType' => '1', //签署类型， 1-单页签署，2-骑缝签署，默认1
                    'posBean' => [ //签署区位置信息,
                        'posPage' => $pos_bean['page'] > 0 ? $pos_bean['page'] : 1, //页码信息，当签署区signType为2时, 页码可以'-'分割, 其他情况只能是数字
                        'posX' => $pos_bean['x'] >= 0 ? $pos_bean['x'] : 500, //x坐标
                        'posY' => $pos_bean['y'] >= 0 ? $pos_bean['y'] : 100, //y坐标
                    ],
                ],
            ],
        ];
        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['PLATFORM_SIGN'], $flowid), $data);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['data'];
    }

    /**
     * 签署流程开启.
     *
     * @param string $flowid 流程id
     */
    protected function flowStart(string $flowid)
    {
        $result = HttpHelper::doPut(Config::$config[$this->model].sprintf(Config::$urlMap['FLOW_START'], $flowid), null);

        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result;
    }

    /**
     * 创建印章.
     *
     * @param string $orgId 机构id
     */
    protected function createSeal(string $orgId)
    {
        $data = [
            'color' => 'RED',
            'type' => 'TEMPLATE_ROUND',
            'central' => 'STAR',
            'height' => '159',
            'width' => '159',
        ];

        $result = HttpHelper::doPost(Config::$config[$this->model].sprintf(Config::$urlMap['CREATE_SEAL'], $orgId), $data);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result['data']['sealId'];
    }

    /**
     * 查询印章.
     *
     * @param string $orgId 机构id
     */
    protected function getSeal(string $orgId)
    {
        $result = HttpHelper::doGet(Config::$config[$this->model].sprintf(Config::$urlMap['GET_SEAL'], $orgId), null);
        $result = (array) json_decode($result, true);
        $this->isErr($result);

        return $result;
    }

    /**
     *获取文件的Content-MD5
     *原理：1.先计算MD5加密的二进制数组（128位）
     *2.再对这个二进制进行base64编码（而不是对32位字符串编码）.
     */
    private function getContentBase64Md5(string $filePath)
    {
        //获取文件MD5的128位二进制数组
        $md5file = md5_file($filePath, true);
        //计算文件的Content-MD5
        $contentBase64Md5 = base64_encode($md5file);

        return $contentBase64Md5;
    }

    /**
     * 判断返回是否有错误.
     *
     * @param array $result 返回值
     */
    protected function isErr(array $result)
    {
        if (isset($result['code']) && $result['code'] != 0) {
            writeLog(__METHOD__."\tcode:".$result['code']."\tmessage:".$result['message']);
            if (isset($this->errMsg[$result['code']])) {
                throw new Exception('esign_'.$result['code'].'：'.$this->errMsg[$result['code']], 0);
            }
            throw new Exception('esign_'.$result['code'].'：'.$result['message'], 0);
        }

        return true;
    }

    /**
     * 上传文件.
     *
     * @param string $upload_url 获取的上传地址
     * @param string $file_path  要上传文件的路径
     */
    protected function upLoadFile(string $upload_url, string $file_path)
    {
        //文件内容
        $fileContent = file_get_contents($file_path);
        $contentMd5 = $this->getContentBase64Md5($file_path);
        $result = HttpHelper::sendHttpPUT($upload_url, $contentMd5, $fileContent);
        if ($result != 200) {
            throw new Exception('esign_上传失败', 0);
        }

        return true;
    }

    /**
     * 获取发起人e签宝个人id.
     *
     * @param int $userid user表id
     */
    protected function getInitiatorAccountId(int $userid)
    {
        $yf_creater = ModelUser::where('id', $userid)->find();
        $user_obj = [
            'third_party_userid' => $yf_creater->name.$yf_creater->tel.$yf_creater->idcard_no,
            'name' => $yf_creater->name,
            'tel' => $yf_creater->tel,
            'idcard_no' => $yf_creater->idcard_no,
        ];
        $accountid = $this->createUser(json_decode(json_encode($user_obj)));

        return $accountid;
    }

    /**
     * 机构账户.
     *
     * @param int $corpid corp表id
     */
    protected function getInitiatorOrgId(int $corpid)
    {
        //机构信息
        $corp = ModelCorp::where('id', $corpid)->find();

        $orgid = $corp->esign_orgid;
        if (empty($corp->esign_orgid)) {
            //默认创建人
            /* $corp_creatorid = \app\model\Conf::where('key', 'corp_creatorid')->value('value');
            $creator_userid = $corp_creatorid > 0 ? $corp_creatorid : \app\model\User::min('id', false);
            $user = ModelUser::where('id', $creator_userid)->find() ?? ModelUser::where('id', \app\model\User::min('id', false))->find(); */
            $user = $this->getDefaultCreator();
            $user->third_party_userid = $user->name.$user->tel.$user->idcard_no;
            //机构信息
            $org_obj = [
                'third_party_userid' => $corp->name.$corp->tyshxydm,
                'creator' => $this->createUser($user),
                'tyshxydm' => $corp->tyshxydm,
                'name' => $corp->name,
            ];
            //创建账户
            $orgid = $this->createOrg(json_decode(json_encode($org_obj)));
            $corp->esign_orgid = $orgid;
        }
        //生成印章
        if ($corp->type == 1) {
            $this->initiatorAuthorizedAccountId = $orgid;
            if (empty($corp->seal_id)) {
                //印章图片不为空则设置图片印章
                if (!empty($corp->seal_url)) {
                    $seal_id = $this->setSealImg($orgid, $corp->seal_url);
                } else {
                    //否则生成默认印章
                    $seal_id = $this->createSeal($orgid);
                }
                $corp->seal_id = $seal_id;
            }
            $this->platform_seal_id = $corp->seal_id;
            //设置静默签署
            $this->signAuth();
        }
        $corp->save();

        return $orgid;
    }

    /**
     * 默认机构创建人.
     */
    protected function getDefaultCreator()
    {
        $corp_creatorid = \app\model\Conf::where('key', 'corp_creatorid')->value('value');
        $creator_userid = $corp_creatorid > 0 ? $corp_creatorid : \app\model\User::min('id', false);
        $user = ModelUser::where('id', $creator_userid)->find() ?? ModelUser::where('id', \app\model\User::min('id', false))->find();

        return $user;
    }

    /**
     * 获取签署人和所属机构信息.
     *
     * @param int $corpid corp表id
     */
    protected function getSignerInfo($corpid)
    {
        $corp = ModelCorp::where('id', $corpid)->find();
        if (!empty($corp->tyshxydm)) {
            $orgid = $this->getInitiatorOrgId($corp->id);
        }
        $accountid = $corp->esign_accountid;
        if (empty($corp->esign_accountid)) {
            $user_obj = [
                'third_party_userid' => $corp->fzr_name.$corp->fzr_tel.$corp->fzr_idcardno,
                'name' => $corp->fzr_name,
                'tel' => $corp->fzr_tel,
                'idcard_no' => $corp->fzr_idcardno,
            ];
            $accountid = $this->createUser(json_decode(json_encode($user_obj)));
            $corp->esign_accountid = $accountid;
            $corp->save();
        }

        return  [
            'accountid' => $accountid,
            'orgid' => $orgid ?? '',
        ];
    }
}
