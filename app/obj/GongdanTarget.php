<?php

namespace app\obj;

use app\model\Area as ModelArea;
use app\model\CheckItem as ModelCheckItem;
use app\model\CheckTask as ModelCheckTask;
use app\model\Device as ModelDevice;

/**
 * 工单目标类
 * @Author: godliu
 * @Date: 2022-06-17 10:00:00
 */
trait GongdanTarget
{
    /**************************************************属性：允许外部设置**************************************************/
    // 目标参数
    private $target_params = [];
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    // 目标信息
    private $target_info = [];
    /**************************************************属性：不允许外部设置************************************************/
    /**
     * 设置目标参数
     */
    public function setTargetParams($target_params)
    {
        $this->target_params = $target_params;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * 获取目标
     */
    public function getTarget()
    {
        try {
            switch ($this->target_params['target']) {
                case 'device':
                    $r = $this->getDeviceTarget();
                    break;
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        $data['target_info'] = $this->target_info;
        return $data;
    }

    /**************************************************逻辑函数***********************************************************/
    /**
     * 获取设备目标
     */
    private function getDeviceTarget()
    {
        try {
            $maps = [];
            $maps[] = ['id', '=', $this->target_params['targetid']];
            $d_detail = ModelDevice::getDetail($maps, 'id,name,areaid,model,dingwei');
            $target_name = !empty($d_detail) ? $d_detail['name'] : '';
            $target_model = !empty($d_detail) ? $d_detail['model'] : '';
            $target_dingwei = !empty($d_detail) ? $d_detail['dingwei'] : '';

            $areaid = !empty($d_detail) ? $d_detail['areaid'] : 0;
            $maps = [];
            $maps[] = ['id', '=', $areaid];
            $a_detail =  ModelArea::getDetail($maps, 'id,name');
            $target_area = !empty($a_detail) ? $a_detail['name'] : '';

            $maps = [];
            $maps[] = ['id', '=', $this->target_params['check_taskid']];
            $ct_detail = ModelCheckTask::getDetail($maps, 'id,check_itemid,note,fujian');
            $target_guzhang = !empty($ct_detail) ? $ct_detail['note'] : '';
            $target_biaoxian = mb_substr($this->target_params['name'], 5);
            $target_fujian = !empty($ct_detail) && !empty($ct_detail['fujian']) ? json_decode($ct_detail['fujian'], true) : [];

            $check_itemid = !empty($ct_detail) ? $ct_detail['check_itemid'] : 0;
            $maps = [];
            $maps[] = ['id', '=', $check_itemid];
            $ci_detail = ModelCheckItem::getDetail($maps, 'id,tag');
            $target_tag = !empty($ci_detail) ? $ci_detail['tag'] : '';
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        $target_info = [];
        $target_info['target_name'] = $target_name;
        $target_info['target_model'] = $target_model;
        $target_info['target_area'] = $target_area;
        $target_info['target_dingwei'] = $target_dingwei;
        $target_info['target_tag'] = $target_tag;
        $target_info['target_guzhang'] = $target_guzhang;
        $target_info['target_biaoxian'] = $target_biaoxian;
        $target_info['target_fujian'] = $target_fujian;
        $this->target_info = $target_info;
        return 1;
    }
}
