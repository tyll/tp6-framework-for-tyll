<?php

namespace app\obj;

use OSS\Core\OssException;
use OSS\OssClient;

/**
 * 上传文件类
 * @Author: godliu
 * @Date: 2021-04-09 10:00:00
 */
class UploadOss
{
    /**************************************************属性：允许外部设置**************************************************/
    private $upload_params = [];
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    private $access_key_id = 'LTAI4Fm7FCnjThvu6oneb4Uj';

    private $access_key_secret = 'UxIMArmlsyXFVeiE2FAijJAxth3kV8';

    private $end_point = 'http://oss-cn-beijing.aliyuncs.com';

    private $end_point_cdn = 'https://imandiao.oss-accelerate.aliyuncs.com';
    /**************************************************属性：不允许外部设置************************************************/
    public function setUploadParams($upload_params)
    {
        $this->upload_params = $upload_params;
    }

    /**************************************************业务函数***********************************************************/
    public function upload()
    {
        try {
            $url = '';
            $ossClient = new OssClient($this->access_key_id, $this->access_key_secret, $this->end_point);
            $r = $ossClient->putObject($this->upload_params['bucket'], $this->upload_params['object'], $this->upload_params['content']);
            if (isset($r['info'])) {
                // $url = $r['info']['url'];
                $url = $this->end_point_cdn . '/' . $this->upload_params['object'];
            }
        } catch (OssException $e) {
            throw new \Exception($e->getMessage(), 0);
        }
        $data['url'] = $url;
        return $data;
    }
    /**
     * 获取元信息
     */
    public function getInfo()
    {
        try {
            $ossClient = new OssClient($this->access_key_id, $this->access_key_secret, $this->end_point);
            // 获取文件的全部元信息。
            $objectMeta = $ossClient->getObjectMeta($this->upload_params['bucket'], $this->upload_params['object']);

            // 获取文件的部分元信息。
            //$objectMeta = $ossClient->getSimplifiedObjectMeta($this->upload_params['bucket'], $this->upload_params['object']);
        } catch (\Throwable $th) {
            throw $th;
        }
        return $objectMeta;
    }

    /**************************************************逻辑函数**************************************************/
}
