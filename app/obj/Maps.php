<?php

namespace app\obj;

use Closure;
use think\helper\Str;

/**
 * 生成where查询条件.
 */
class Maps
{
    /**
     * 生成的查询条件.
     */
    protected $maps = [];
    /**
     * 请求参数.
     */
    protected $input = [];
    /**
     * 忽略.
     */
    protected $ignore = [];
    /**
     * 等于.
     */
    protected $equal = [];
    /**
     * 不判断的字段.
     */
    protected $strictField = [];

    protected static $instance;

    protected function __construct()
    {
    }

    /**
     * 初始化.
     *
     * @param array $input 请求参数
     * @param bool  $renew 是否直接实例化
     */
    public static function getInstance(array $input = [], bool $renew = false)
    {
        if ($renew || !self::$instance instanceof self) {
            self::$instance = new static();
        }
        self::initialize($input);

        return self::$instance;
    }

    /**
     * 初始化.
     */
    protected static function initialize($input)
    {
        $instance = self::$instance;
        $instance->input = $input;
        $instance->maps = [];
        $instance->ignore = [];
        $instance->equal = [];
        $instance->strictField = [];

        return true;
    }

    /**
     * 请求参数里模糊查找.
     *
     * @param int|string|array $data  key
     * @param int|string       $value value
     */
    public function like($data, $value = null)
    {
        //参数转数组
        $array = $this->getArray($data, $value);
        //循环处理
        foreach ($array as $k => $v) {
            //如果存在且有值
            if (isset($this->input[$v]) && !empty($this->input[$v])) {
                $map = [$k, 'like', '%'.$this->input[$v].'%'];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * 直接模糊查找.
     *
     * @param int|string|array $data  key
     * @param int|string       $value value
     */
    public function execLike($data, $value = null)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            $map = [$k, 'like', '%'.$v.'%'];
            $this->add($map);
        }

        return $this;
    }

    /**
     * 多个模糊查找.
     *
     * @param int|string|array $data  key
     * @param int|string       $value value
     */
    public function eachLike($data, $value)
    {
        if (isset($this->input[$value]) && !empty($this->input[$value])) {
            foreach ($data as $field) {
                $map = [$field, 'like', '%'.$this->input[$value].'%'];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * 等于.
     *
     * @param bool $unequal 是否不等于
     */
    public function equal($data, $value = null, $unequal = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            if ($this->checkStrict($k)) {
                goto equal_end;
            }
            if (isset($this->input[$v]) && ((is_string($this->input[$v]) && !empty($this->input[$v]) && $this->input[$v] != '-1')
                ||
                (is_numeric($this->input[$v]) && $this->input[$v] >= 0))) {
                equal_end:
                $map = [$k, $unequal ? '<>' : '=', $this->input[$v] ?? -1];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * 直接等于.
     *
     * @param bool $unequal 是否不等于
     */
    public function execEqual($data, $value = null, bool $unequal = false)
    {
        $array = $this->getArray($data, $value ?? '');

        foreach ($array as $k => $v) {
            $map = [$k, $unequal ? '<>' : '=', $v];
            $this->add($map);
        }

        return $this;
    }

    /**
     * 之间.
     *
     * @param bool   $timestamp  是否转为时间戳
     * @param string $end_change 更改结束值  跟strtotime一致
     */
    public function between($data, array $value = [], bool $timestamp = false, string $end_change = null)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            if (empty($v) || $k == $v) {
                $v = ['start_time', 'end_time'];
            }
            [$start, $end] = $v;
            //开始和结束都存在时
            if (
                isset($this->input[$start]) &&
                ((is_string($this->input[$start]) && !empty($this->input[$start])) || (is_numeric($this->input[$start]) && $this->input[$start] >= 0))
                &&
                isset($this->input[$end]) && !empty($this->input[$end])
            ) {
                //更改结束值
                if ($end_change) {
                    $this->input[$end] = $this->changeTime($this->input[$end], $end_change);
                }
                $values = [$this->input[$start], $this->input[$end]];
                $map = [$k, 'between', $timestamp ? $this->arrStrToTime($values) : $values];
                $this->add($map);
            } elseif (
                //只存在开始值时使用大于等于
                isset($this->input[$start]) &&
                ((is_string($this->input[$start]) && !empty($this->input[$start])) || (is_numeric($this->input[$start]) && $this->input[$start] >= 0))
            ) {
                return $this->gt($k, $start, true, $timestamp);
            } elseif (
                //只存在结束值时使用小于等于
                isset($this->input[$end]) &&
                ((is_string($this->input[$end]) && !empty($this->input[$end])) || (is_numeric($this->input[$end]) && $this->input[$end] >= 0))
            ) {
                //更改结束值
                if ($end_change) {
                    $this->input[$end] = $this->changeTime($this->input[$end], $end_change);
                }

                return $this->lt($k, $end, true, $timestamp);
            }
        }

        return $this;
    }

    /**
     * exec之间.
     */
    public function execBetween($data, array $value = [], bool $timestamp = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            $map = [$k, 'between', $timestamp ? $this->arrStrToTime($v) : $v];
            $this->add($map);
        }

        return $this;
    }

    /**
     * 大于或大于等于.
     */
    public function gt($data, $value = null, bool $egt = false, $timestamp = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            if (isset($this->input[$v]) && !empty($this->input[$v])) {
                $map = [$k, $egt ? '>=' : '>', $timestamp ? strtotime($this->input[$v]) : $this->input[$v]];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * exec大于或大于等于.
     */
    public function execGt($data, $value = null, bool $egt = false, $timestamp = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            $map = [$k, $egt ? '>=' : '>', $timestamp ? strtotime($v) : $v];
            $this->add($map);
        }

        return $this;
    }

    /**
     * 小于或小于等于.
     */
    public function lt($data, $value = null, bool $elt = false, $timestamp = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            if (isset($this->input[$v]) && !empty($this->input[$v])) {
                $map = [$k, $elt ? '<=' : '<', $timestamp ? strtotime($this->input[$v]) : $this->input[$v]];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * exec小于或小于等于.
     */
    public function execLt($data, $value = null, bool $elt = false, $timestamp = false)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            $map = [$k, $elt ? '<=' : '<', $timestamp ? strtotime($v) : $v];
            $this->add($map);
        }

        return $this;
    }

    /**
     * in.
     *
     * @param string|array $data
     * @param array|string $value
     */
    public function in($data, string $value = null)
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $field => $param) {
            $operate = 'in';
            if ($this->checkStrict($field)) {
                goto in_end;
            }
            if (isset($this->input[$param]) && !empty($this->input[$param])) {
                if (!is_array($this->input[$param])) {
                    $operate = '=';
                }
                in_end:
                $map = [$field, $operate, $this->input[$param] ?? []];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * 直接in.
     */
    public function execIn($data, array $value = [])
    {
        $array = $this->getArray($data, $value);

        foreach ($array as $k => $v) {
            if (empty($v)) {
                continue;
            }
            $map = [$k, 'in', $v];
            $this->add($map);
        }

        return $this;
    }

    /**
     * 联表的模糊查询条件.
     *
     * @param string $field  当前表的字段
     * @param string $key    对应表的字段
     * @param string $value  请求对应的参数
     * @param string $model  对应表的model
     * @param string $column 对应表的取值
     *
     * @return self
     */
    public function joinIn(string $field, string $key, string $value, string $model, string $column = 'id')
    {
        $array = [];
        $model = $this->getModel($model);

        if (isset($this->input[$value]) && !empty($this->input[$value])) {
            $array = $model::where($key, 'like', '%'.$this->input[$value].'%')->column($column);
        }
        if (($this->checkStrict($field) && !empty($this->input[$value])) || !empty($array)) {
            $map = [$field, 'in', $array];
            $this->add($map);
        }

        return $this;
    }

    public function joinInV2(string $field, string $key, string $value, string $model, bool $not_in = false, string $column = 'id')
    {
        if (isset($this->input[$value]) && !empty($this->input[$value])) {
            return $this->mapsIn($field, function ($maps) use ($key, $value) {
                return $maps->like($key, $value);
            }, $model, $column, $not_in);
        }

        return $this;
    }

    /**
     * 根据条件in.
     *
     * @param string     $field  当前表的字段
     * @param array|self $maps   对应表的条件
     * @param string     $model  对应表的model
     * @param string     $column 对应表的取值
     */
    public function mapsIn(string $field, $maps, string $model, string $column = 'id', bool $not_in = false)
    {
        $maps = $this->getMaps($maps);

        if (!empty($maps)) {
            $model = $this->getModel($model);

            $array = $model::where($maps)->column($column);
            if ($this->checkStrict($field) || !empty($array)) {
                $map = [$field, $not_in ? 'not in' : 'in', $array];
                $this->add($map);
            }
        }

        return $this;
    }

    /**
     * 当$condition空时忽略字段$field.
     *
     * @param string|array $field     要忽略的字段
     * @param string       $condition 条件字段
     */
    public function ignoreIfEmpty($field, string $condition = null)
    {
        $data = $field;
        if (!is_array($field)) {
            $data = [
                $field => $condition,
            ];
        }
        foreach ($data as $field => $condition) {
            $this->ignore[$condition] = $field;
        }

        return $this;
    }

    /**
     * 当$condition空时设置字段$field值
     *
     * @param string|array $field     要设置值的字段
     * @param string|int   $value
     * @param string       $condition 条件字段
     * @param bool         $unequal   是否不等于
     */
    public function equalIfEmpty($field, $value = null, string $condition = null, bool $unequal = false)
    {
        $array = $field;
        if (!is_array($field)) {
            $array = [[$field, $value, $condition]];
        }
        //处理
        foreach ($array as $k => $v) {
            $field = $k;
            $value = $v;
            $condition = null;
            if (is_array($v)) {
                [$field, $value] = $v;
                $condition = $v[2] ?? null;
            }
            //条件$condition为空则与字段$field相同
            $condition = $condition ?: $field;
            //保存
            $this->equal[$condition] = [$field, $unequal ? '<>' : '=', $value];
        }

        return $this;
    }

    /**
     * 当$condition为true时改变$field值
     *
     * @param array|string $field 字段
     */
    /* public function equalWhen(bool $condition, $field, $value = null)
    {
        if ($condition) {
            $data = $field;
            if (!is_array($data)) {
                $data = [$field => $value];
            }
            foreach ($data as $k => $v) {
                if (isset($this->input[$k])) {
                    $this->input[$k] = $v;
                }
            }
        }
        return $this;
    } */

    /**
     * 不判断是否存在.
     *
     * @param array|string $field 不判断的本表字段
     */
    public function strictWith($field)
    {
        $field = (array) $field;
        $this->strictField = $field;

        return $this;
    }

    /**
     * 数字转换.
     *
     * @param array|string $field   待转换的字段
     * @param string       $change  转换方式  +-* /  例如 '* 100'
     * @param bool         $integer 是否返回整数
     */
    public function numberChange($field, string $change, bool $integer = false)
    {
        $field = (array) $field;
        [$operate, $value] = explode(' ', $change);
        foreach ($field as $k => $v) {
            if (isset($this->input[$v]) && (!empty($this->input[$v]) || $this->input[$v] != '-1')) {
                $number = $this->calc($this->input[$v], $operate, $value);
                $this->input[$v] = $integer ? (int) $number : $number;
            }
        }

        return $this;
    }

    /**
     * 改变时间值
     */
    /* public function timeChange($field, string $change, bool $timestamp = false)
    {
        $field = (array)$field;
        foreach ($field as $k => $v) {
            if (isset($this->input[$v]) && (!empty($this->input[$v]) || $this->input[$v] != "-1")) {
                $time = $this->changeTime($this->input[$v], $change);
                //是否转时间戳
                if ($timestamp) {
                    $time = $this->strToTime($time);
                }
                $this->input[$v] = $time;
            }
        }
        return $this;
    } */

    /**
     * 返回生成的查询条件.
     */
    public function get(): array
    {
        return $this->maps;
    }

    /**********************************************************内部方法******************************************************/

    /**
     * 计算.
     */
    protected function calc($v_a, $oper, $v_b)
    {
        switch ($oper) {
            case '+':
                $value = round($v_a + $v_b, 2);
                break;
            case '-':
                $value = round($v_a - $v_b, 2);
                break;
            case '*':
                $value = round($v_a * $v_b, 2);
                break;
            case '/':
                $value = round($v_a / $v_b, 2);
                break;

            default:
                $value = 0;
                break;
        }

        return $value;
    }

    /**
     * 检查是否不判断.
     */
    protected function checkStrict($field)
    {
        if (in_array($field, $this->strictField)) {
            return true;
        }

        return false;
    }

    /**
     * 获取处理传入的maps.
     *
     * @param array|self|Closure $maps 传入的maps
     */
    protected function getMaps($maps): array
    {
        if ($maps instanceof Closure) {
            $maps = $maps(self::getInstance($this->input, true));
        }
        if ($maps instanceof self) {
            $maps = $maps->get();
        }
        if (!is_array($maps)) {
            return [];
        }

        return $maps;
    }

    /**
     * 获取模型包含命名空间.
     *
     * @param string $model 模型
     */
    protected function getModel(string $model): string
    {
        if (strpos($model, '\\') === false) {
            $model = "\app\model\\".ucfirst(Str::camel($model));
        }

        return $model;
    }

    /**
     * 更改时间值
     *
     * @param int|string|array $time   时间值
     * @param string           $change 操作  跟strtotime一致
     */
    protected function changeTime($time, string $change)
    {
        if (is_array($time)) {
            $new_value = [];
            foreach ($time as $k => $v) {
                $new_value[] = $this->changeTime($v, $change);
            }
        } else {
            if (is_numeric($time)) {
                $new_value = strtotime($change, $time);
            } else {
                $new_value = date('Y-m-d', strtotime($change, strtotime($time)));
            }
        }

        return $new_value;
    }

    /**
     * 检查忽略项.
     *
     * @param array $array 待处理数组
     */
    protected function checkIgnore(array $array): array
    {
        foreach ($this->ignore as $k => $v) {
            if (!isset($this->input[$k]) || ((empty($this->input[$k]) && $this->input[$k] != '0') || $this->input[$k] === 0)) {
                unset($array[$v]);
            }
        }

        return $array;
    }

    /**
     * 检查设置值
     *
     * @param array $array 待处理数组
     */
    protected function checkEqual(array $array)
    {
        foreach ($this->equal as $k => $v) {
            //如果条件不存在或为空则设置值
            if (!isset($this->input[$k]) || ((empty($this->input[$k]) && $this->input[$k] != '0') || $this->input[$k] === 0)) {
                [$field, $operate, $value] = $v;
                if (array_key_exists($field, $array)) {
                    $this->add($v);
                }
            }
        }
    }

    /**
     * 添加到条件数组里.
     *
     * @param array $map 单个条件
     */
    protected function add(array $map)
    {
        //if (!$this->inMaps($map)) {
        $this->maps[] = $map;
        //}

        return true;
    }

    /**
     * 是否条件已存在.
     *
     * @param array $map 单个条件
     */
    protected function inMaps(array $map)
    {
        foreach ($this->maps as $k => $v) {
            if ($map[0] == $v[0]) {
                return true;
            }
        }

        return false;
    }

    /**
     * 转数组.
     *
     * @param array|string $data
     * @param string|array $value
     */
    protected function getArray($data, $value = null)
    {
        $array = [];
        if (is_array($data)) { //数组处理
            foreach ($data as $k => $v) {
                if (is_numeric($k)) {
                    $k = $v;
                }
                $array[$k] = $v;
            }
        } else { //不是数组则转数组
            if ($value === null) {
                $value = $data;
            }
            $array[$data] = $value;
        }
        //检查忽略项
        $array = $this->checkIgnore($array);
        //检查设置值
        $this->checkEqual($array);

        return $array;
    }

    protected function strToTime($time)
    {
        if (is_array($time)) {
            $data = [];
            foreach ($time as $k => $v) {
                $data[] = strtotime((string) $v);
            }
        } else {
            $data = strtotime((string) $time);
        }

        return $data;
    }

    /**
     * 数组元素转时间戳.
     *
     * @param array $array 一维数组
     */
    protected function arrStrToTime(array $array)
    {
        $data = [];
        foreach ($array as $k => $v) {
            $data[] = strtotime($v);
        }

        return $data;
    }
}
