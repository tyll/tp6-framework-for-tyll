<?php

namespace app\obj;

use app\model\CheckItem as ModelCheckItem;
use app\model\CheckTask as ModelCheckTask;
use app\model\Gongdan as ModelGongdan;
use app\model\GongdanFlow as ModelGongdanFlow;
use think\exception\ValidateException;

/**
 * 工单类
 * @Author: godliu
 * @Date: 2022-06-17 10:00:00
 */
class Gongdan
{
    use GongdanTarget, GongdanFlow, GongdanAction;
    /**************************************************属性：允许外部设置**************************************************/
    // 巡检工单参数
    private $gongdan_params = null;
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    // 工单名称
    private $gongdan_name = '';

    // 工单id
    private $gongdanid = 0;

    // 工单
    private $gongdan_model = null;
    /**************************************************属性：不允许外部设置************************************************/
    /**
     * 设置工单参数
     */
    public function setGongdanParams($gongdan_params)
    {
        $this->gongdan_params = $gongdan_params;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * 生成工单
     */
    public function genGongdan()
    {
        try {
            validate(\app\validate\Gongdan::class)->scene('gen')->check($this->gongdan_params);
        } catch (ValidateException $th) {
            throw new \Exception($th->getError(), 0);
        }

        try {
            $r = $this->getGongdanName();

            $r = $this->createGongdan();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }

    /**
     * 处理工单
     */
    public function dealGongdan()
    {
        try {
            validate(\app\validate\GongdanFlow::class)->scene('deal')->check($this->gongdan_params);
        } catch (ValidateException $th) {
            throw new \Exception($th->getError(), 0);
        }

        $maps = [];
        $maps[] = ['id', '=', $this->gongdan_params['gongdanid']];
        $g_detail = ModelGongdan::getDetail($maps, 'id,sure_userid,curr_userids,status');
        if (empty($g_detail)) {
            throw new \Exception('该工单不存在', 0);
        }
        $curr_userid_arr = !empty($g_detail['curr_userids']) ? json_decode($g_detail['curr_userids'], true) : [];
        if (!in_array($this->gongdan_params['userid'], $curr_userid_arr)) {
            throw new \Exception('您不能处理该工单', 0);
        }
        if (!in_array($g_detail['status'], [10, 20, 30, 40])) {
            throw new \Exception('该工单已结单', 0);
        }
        if (
            $g_detail['status'] == 10
            && !in_array($this->gongdan_params['action'], ['fenpai', 'kaishi'])
        ) {
            throw new \Exception('该工单只能[分派工单]、[开始处理]', 0);
        } elseif (
            $g_detail['status'] == 20
            && !in_array($this->gongdan_params['action'], ['kaishi', 'zhuanfa'])
        ) {
            throw new \Exception('该工单只能[开始处理]、[转发]', 0);
        } elseif (
            $g_detail['status'] == 30
            && !in_array($this->gongdan_params['action'], ['wanbi', 'zhuanfa'])
        ) {
            throw new \Exception('该工单只能[处理完毕]、[转发]', 0);
        } elseif (
            $g_detail['status'] == 40
            && !in_array($this->gongdan_params['action'], ['jiedan', 'weijiejue'])
        ) {
            throw new \Exception('该工单只能[结单]、[未解决]', 0);
        }
        $this->gongdan_model = $g_detail;

        try {
            $r = $this->createGongdanFlow();

            $r = $this->updateGongdan();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }

    /**************************************************逻辑函数***********************************************************/
    /**
     * 获取工单名称
     */
    private function getGongdanName()
    {
        try {
            $gongdan_name = '';
            switch ($this->gongdan_params['target']) {
                case 'device':
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_params['check_taskid']];
                    $ct_detail = ModelCheckTask::getDetail($maps, 'id,check_itemid');
                    $check_itemid = !empty($ct_detail) ? $ct_detail['check_itemid'] : 0;
                    $maps = [];
                    $maps[] = ['id', '=', $check_itemid];
                    $ci_detail = ModelCheckItem::getDetail($maps, 'id,content');
                    $content = !empty($ci_detail) ? $ci_detail['content'] : '';
                    $gongdan_name = '巡检任务：' . $content . '-异常';
                    break;
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        $this->gongdan_name = $gongdan_name;
        return 1;
    }

    /**
     * 创建工单
     */
    private function createGongdan()
    {
        try {
            $maps = [];
            $maps[] = ['check_taskid', '=', $this->gongdan_params['check_taskid']];
            $g_detail = ModelGongdan::getDetail($maps, 'id');
            if (empty($g_detail)) {
                $g_data = [];
                $g_data['from_userid'] = $this->gongdan_params['from_userid'];
                $g_data['sure_userid'] = $this->gongdan_params['sure_userid'];
                $g_data['curr_userids'] = json_encode($this->gongdan_params['curr_userids'], JSON_UNESCAPED_UNICODE);
                $g_data['target'] = $this->gongdan_params['target'];
                $g_data['targetid'] = $this->gongdan_params['targetid'];
                $g_data['check_taskid'] = $this->gongdan_params['check_taskid'];
                $g_data['name'] = $this->gongdan_name;
                $g_data['status'] = 10;
                $r = ModelGongdan::create($g_data);
                $this->gongdanid = $r->id;

                $gf_data = [];
                $gf_data['gongdanid'] = $this->gongdanid;
                $gf_data['oper_userid'] = $this->gongdan_params['from_userid'];
                $gf_data['to_userids'] = json_encode([], JSON_UNESCAPED_UNICODE);
                $gf_data['action'] = 'faxian';
                $gf_data['note'] = '';
                $gf_data['fujian'] = json_encode([], JSON_UNESCAPED_UNICODE);
                $r = ModelGongdanFlow::create($gf_data);

                // 发送消息
                $msg_params = [];
                $msg_params['gongdanid'] = $this->gongdanid;
                $msg_params['scene'] = 'gen_gongdan';
                $sm = new \app\obj\SendMsg();
                $sm->setMsgParams($msg_params);
                $r = $sm->sendMsg();
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }

    /**
     * 创建工单流程
     */
    private function createGongdanFlow()
    {
        try {
            $gf_data = [];
            $gf_data['gongdanid'] = $this->gongdan_model['id'];
            $gf_data['oper_userid'] = $this->gongdan_params['userid'];
            if ($this->gongdan_params['action'] == 'fenpai') {
                $to_userids = $this->gongdan_params['to_userids'];
            } elseif ($this->gongdan_params['action'] == 'zhuanfa') {
                $to_userids = $this->gongdan_params['to_userids'];
            } else {
                $to_userids = [];
            }
            $gf_data['to_userids'] = json_encode($to_userids, JSON_UNESCAPED_UNICODE);
            $gf_data['action'] = $this->gongdan_params['action'];
            $gf_data['note'] = trim($this->gongdan_params['note']);
            $gf_data['fujian'] = json_encode($this->gongdan_params['fujian'], JSON_UNESCAPED_UNICODE);
            $r = ModelGongdanFlow::create($gf_data);
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }

    /**
     * 更新工单
     */
    private function updateGongdan()
    {
        try {
            switch ($this->gongdan_params['action']) {
                case 'fenpai':
                    $g_data = [];
                    $g_data['curr_userids'] = json_encode($this->gongdan_params['to_userids'], JSON_UNESCAPED_UNICODE);
                    $g_data['status'] = 20;
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);

                    // 发送消息
                    $msg_params = [];
                    $msg_params['gongdanid'] = $this->gongdan_model['id'];
                    $msg_params['scene'] = 'fenpai_gongdan';
                    $sm = new \app\obj\SendMsg();
                    $sm->setMsgParams($msg_params);
                    $r = $sm->sendMsg();
                    break;
                case 'kaishi':
                    $g_data = [];
                    $g_data['status'] = 30;
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);
                    break;
                case 'zhuanfa':
                    $g_data = [];
                    $g_data['curr_userids'] = json_encode($this->gongdan_params['to_userids'], JSON_UNESCAPED_UNICODE);
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);

                    // 发送消息
                    $msg_params = [];
                    $msg_params['gongdanid'] = $this->gongdan_model['id'];
                    $msg_params['scene'] = 'zhuanfa_gongdan';
                    $sm = new \app\obj\SendMsg();
                    $sm->setMsgParams($msg_params);
                    $r = $sm->sendMsg();
                    break;
                case 'wanbi':
                    $g_data = [];
                    $g_data['curr_userids'] = json_encode([$this->gongdan_model['sure_userid']], JSON_UNESCAPED_UNICODE);
                    $g_data['status'] = 40;
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);

                    // 发送消息
                    $msg_params = [];
                    $msg_params['gongdanid'] = $this->gongdan_model['id'];
                    $msg_params['scene'] = 'wanbi_gongdan';
                    $sm = new \app\obj\SendMsg();
                    $sm->setMsgParams($msg_params);
                    $r = $sm->sendMsg();
                    break;
                case 'weijiejue':
                    $maps = [];
                    $maps[] = ['gongdanid', '=', $this->gongdan_model['id']];
                    $maps[] = ['action', '=', 'wanbi'];
                    $gf_detail = ModelGongdanFlow::getDetail($maps, 'id,oper_userid', 'id DESC');
                    $oper_userid = !empty($gf_detail) ? $gf_detail['oper_userid'] : 0;
                    $g_data = [];
                    $g_data['curr_userids'] = json_encode([$oper_userid], JSON_UNESCAPED_UNICODE);
                    $g_data['status'] = 30;
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);

                    // 发送消息
                    $msg_params = [];
                    $msg_params['gongdanid'] = $this->gongdan_model['id'];
                    $msg_params['scene'] = 'weijiejue_gongdan';
                    $sm = new \app\obj\SendMsg();
                    $sm->setMsgParams($msg_params);
                    $r = $sm->sendMsg();
                    break;
                case 'jiedan':
                    $g_data = [];
                    $g_data['status'] = 100;
                    $maps = [];
                    $maps[] = ['id', '=', $this->gongdan_model['id']];
                    $r = ModelGongdan::update($g_data, $maps);
                    break;
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }
}
