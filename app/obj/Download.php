<?php

namespace app\obj;

use app\model\Download as ModelDownload;

/**
 * 下载类
 * @Author: godliu
 * @Date: 2021-01-13 18:00:00
 */
class Download
{
    /**************************************************属性：允许外部设置**************************************************/
    // 所属人id
    private $owner = 0;

    // 文件名
    private $file_name = '';

    // 文件扩展名
    private $file_ext = '';

    // 文件路径
    private $file_path = '';
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    // 下载id
    private $did = 0;
    /**************************************************属性：不允许外部设置************************************************/
    /**
     * 设置所属人id
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * 设置文件名
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * 设置文件扩展名
     */
    public function setFileExt($file_ext)
    {
        $this->file_ext = $file_ext;
    }

    /**
     * 设置文件路径
     */
    public function setFilePath($file_path)
    {
        $this->file_path = $file_path;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * 新增下载记录
     */
    public function addDownload()
    {
        try {
            $d_data = [];
            $d_data['owner'] = $this->owner;
            $d_data['name'] = $this->file_name . $this->file_ext;
            $r = ModelDownload::create($d_data);
            $this->did = $r->id;
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }

    /**
     * 更新下载记录
     */
    public function saveDownload()
    {
        try {
            $file_url = $this->file_path . '/' . $this->file_name . $this->file_ext;
            $d_data = [];
            $d_data['url'] = \think\facade\Request::root(true) . '/' . $file_url;
            $d_data['status'] = 1;

            $maps = [];
            $maps[] = ['id', '=', $this->did];
            $r = ModelDownload::update($d_data, $maps);
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        return 1;
    }
}
