<?php

namespace app\obj;

use app\model\Token as ModelToken;
use DateTime;

/**
 * token.
 */
class Token
{
    protected static $instance;
    protected $retoken;
    protected $retoken_expire_time;

    protected $timeStr = [
        'year', 'month', 'day', 'h', 'm', 's',
    ];

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        self::$instance->init();

        return self::$instance;
    }

    protected function init()
    {
        $this->retoken = '';
        $this->retoken_expire_time = 0;
    }

    /**
     * 刷新token.
     */
    public function setReToken($retoken, $expire_time)
    {
        $this->retoken = $retoken;
        $this->retoken_expire_time = $this->dealExpireTime($expire_time);

        return $this;
    }

    /**
     * 保存token.
     *
     * @param string     $key         token标识
     * @param string     $token       token
     * @param string|int $expire_time 有效时间
     * @param array      $option      额外参数，包含刷新token和其有效时间
     */
    public function save(string $key, string $token, $expire_time = '1h', array $option = [])
    {
        $option = $this->dealOption($option);
        $data = [
            'key' => $key,
            'token' => $token,
            'token_expire_time' => $this->dealExpireTime($expire_time),
            'refresh_token' => $this->retoken ?? $option['refresh_token'],
            'retoken_expire_time' => $this->retoken_expire_time ?? $option['retoken_expire_time'],
        ];

        $token = ModelToken::where('key', $key)->find();
        //token存在且与新token不相等则更新
        if ($token && $token != $token->token) {
            //刷新token存在且相等则不更新刷新token及过期时间
            if ($data['refresh_token'] == $token->refresh_token) {
                unset($data['refresh_token'], $data['retoken_expire_time']);
            }
            $data['id'] = $token->id;
            ModelToken::update($data);
        } elseif (!$token) {
            ModelToken::create($data);
        }

        return true;
    }

    public function getToken(string $key, $default = null)
    {
        $token = ModelToken::where('key', $key)->find();
        if (!$token || $token->token_expire_time < time()) {
            return $default;
        }

        return $token->token;
    }

    /**
     * 处理刷新token.
     */
    protected function dealOption($option)
    {
        $data = [
            'refresh_token' => '',
            'retoken_expire_time' => 0,
        ];
        if (empty($option)) {
            return $data;
        }
        if (!isset($option['retoken'])) {
            throw new \Exception('缺少刷新token');
        }
        if (!isset($option['expire_time'])) {
            throw new \Exception('缺少刷新token的有效期');
        }
        $data['refresh_token'] = $option['retoken'];
        $data['retoken_expire_time'] = $this->dealExpireTime($option['expire_time']);

        return $data;
    }

    /**
     * 处理有效期
     */
    protected function dealExpireTime($time)
    {
        if (is_numeric($time)) {
            return time() + $time;
        } elseif (is_string($time)) {
            return $this->getTimeByStr($time);
        } elseif ($time instanceof DateTime) {
            return $time->getTimestamp();
        }
    }

    /**
     * 获取时间.
     */
    protected function getTimeByStr($time)
    {
        $res = $this->getNumAndStr($time);
        $num = $res['num'];
        $str = $res['str'];
        if (!in_array($str, $this->timeStr)) {
            throw new \Exception('错误的有效时间');
        }
        switch ($str) {
            case 'year':
                $base_time = 3600 * 24 * 365;
                break;
            case 'month':
                $base_time = 3600 * 24 * 30;
                break;
            case 'day':
                $base_time = 3600 * 24;
                break;
            case 'h':
                $base_time = 3600;
                break;
            case 'm':
                $base_time = 60;
                break;
            case 'm':
                $base_time = 1;
                break;

            default:
                $base_time = 0;
                break;
        }

        return time() + $num * $base_time;
    }

    /**
     * 获取字符串的数字及字符串.
     */
    protected function getNumAndStr(string $str)
    {
        $number = '';
        $string = '';
        for ($i = 0; $i < strlen($str); ++$i) {
            $tmp = substr($str, $i, 1);
            if (is_numeric($tmp)) {
                $number .= $tmp;
            }
            if (is_string($tmp) && !is_numeric($tmp)) {
                $string .= $tmp;
            }
        }

        return ['num' => (int) $number, 'str' => $string];
    }
}
