<?php

namespace app\obj;

/**
 * 工单操作类
 * @Author: godliu
 * @Date: 2022-06-17 10:00:00
 */
trait GongdanAction
{
    /**************************************************属性：允许外部设置**************************************************/
    // 操作参数
    private $action_params = [];
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    /**************************************************属性：不允许外部设置************************************************/
    /**
     * 设置操作参数
     */
    public function setActionParams($action_params)
    {
        $this->action_params = $action_params;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * 获取操作
     */
    public function getAction()
    {
        try {
            $action_info = [];
            $action_arr = \think\facade\Config::get('tyll.dict.gongdan_flow.action');
            unset($action_arr['faxian']);

            if ($this->action_params['status'] == 10) {
                $primary = 'fenpai';
                $plain = 'kaishi';
            } elseif ($this->action_params['status'] == 20) {
                $primary = 'kaishi';
                $plain = 'zhuanfa';
            } elseif ($this->action_params['status'] == 30) {
                $primary = 'wanbi';
                $plain = 'zhuanfa';
            } elseif ($this->action_params['status'] == 40) {
                $primary = 'jiedan';
                $plain = 'weijiejue';
            } elseif ($this->action_params['status'] == 100) {
                $primary = '';
                $plain = '';
            }
            $primary_arr = $plain_arr = $other_arr = [];
            foreach ($action_arr as $k => $v) {
                if (!empty($primary) && !empty($plain)) {
                    if ($k == $primary) {
                        $primary_arr['action'] = $k;
                        $primary_arr['name'] = $v;
                    } elseif ($k == $plain) {
                        $plain_arr['action'] = $k;
                        $plain_arr['name'] = $v;
                    } else {
                        $temp_arr = [];
                        $temp_arr['action'] = $k;
                        $temp_arr['name'] = $v;
                        $other_arr[] = $temp_arr;
                    }
                }
            }

            $action_info['primary'] = $primary_arr;
            $action_info['plain'] = $plain_arr;
            $action_info['others'] = $other_arr;
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        $data['action_info'] = $action_info;
        return $data;
    }

    /**************************************************逻辑函数***********************************************************/
}
