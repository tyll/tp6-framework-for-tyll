<?php

namespace app\obj;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * excel.
 *
 * 使用需要安装PhpOffice\PhpSpreadsheet
 *      composer require phpoffice/phpspreadsheet
 */
class Excel
{
    /**允许的扩展名 */
    const ALLOW_EXT = ['Xlsx', 'xlsx', 'Xls', 'xls', 'Html', 'html'];

    /**数据 */
    protected $data;

    /**文件扩展名 */
    protected $ext = 'xlsx';

    /**日期列索引 */
    protected $dateColumn = [];
    /**日期列格式 */
    protected $dateFormat;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * 设置日期列.
     *
     * @param string|int|array $index  列索引 (数字或字母,1/A)
     * @param string           $format 日期列输出的格式
     */
    public function setDateColumn($index, $format = 'Y-m-d H:i:s'): self
    {
        $this->dateColumn = $this->getColumnIndex($index);
        $this->dateFormat = $format;

        return $this;
    }

    /**
     * 生成文件.
     *
     * @param string $path 生成文件保存地址
     * @param string $name 生成文件名(可包含在地址里)
     * @param string $ext  文件扩展名(可包含在地址或文件名里)
     */
    public function write(string $path, string $name = '', string $ext = ''): string
    {
        try {
            $path_info = $this->getPathinfo($path, $name, $ext);

            // 获取Spreadsheet对象
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            // 从第一行开始写入数据
            $rows = 1;
            foreach ($this->data as $item) {
                $column_key = 'A';
                foreach ($item as $value) {
                    // 单元格内容写入
                    $sheet->setCellValue($column_key.$rows, $value);
                    ++$column_key;
                }
                ++$rows;
            }

            $writer = IOFactory::createWriter($spreadsheet, ucfirst($path_info['ext']));

            $writer->save($path_info['complete_path']);
        } catch (\Throwable $th) {
            throw $th;
        }

        return $path_info['complete_path'];
    }

    /**
     * 读取表格内容.
     *
     * @param string     $file_path  上传的文件路径
     * @param int|string $row_num    从第几行开始读取
     * @param int|string $column_num 从第几列开始读取
     */
    public function read(string $file_path, $row_num = 1, $column_num = 1): array
    {
        try {
            // 创建读操作
            $reader = IOFactory::createReader(ucfirst($this->ext));

            // 打开文件 载入excel表格
            $spreadsheet = $reader->load($file_path);

            // 获取活动工作簿
            $sheet = $spreadsheet->getActiveSheet();

            // 获取内容的最大列 如 D
            $highest = Coordinate::columnIndexFromString($sheet->getHighestColumn());

            // 获取内容的最大行 如 4
            $rows = $sheet->getHighestRow();

            // 用于存储表格数据
            $data = [];
            for ($i = $row_num; $i <= $rows; ++$i) {//行
                $row = [];
                for ($j = $column_num; $j <= $highest; ++$j) {//列
                    //单元格的值
                    $tmp_value = $sheet->getCellByColumnAndRow($j, $i)->getValue();

                    //日期列转日期格式
                    if (in_array($j, $this->dateColumn)) {
                        $tmp_value = Date::excelToDateTimeObject($tmp_value)->format($this->dateFormat);
                    }

                    $row[] = $tmp_value;
                }
                $data[] = $row;
            }
            $this->data = $data;
        } catch (\Throwable $th) {
            throw $th;
        }

        return $data;
    }

    /*****************************************************************************************************. */

    /**
     * 列索引转数字.
     *
     * @param string|int|array $index 列索引 (数字或字母,1/A)
     */
    private function getColumnIndex($index): array
    {
        settype($index, 'array');

        $index_arr = [];
        foreach ($index as $k => $v) {
            if (!is_numeric($v) && is_string($v)) {
                $v = Coordinate::columnIndexFromString($v);
            }
            $index_arr[] = $v;
        }

        return $index_arr;
    }

    /**
     * 验证导出参数.
     *
     * @param string $path 生成文件保存地址
     * @param string $name 生成文件名(可包含在地址里)
     * @param string $ext  文件扩展名(可包含在地址或文件名里)
     */
    private function getPathinfo(string $path, string $name = '', string $ext = ''): array
    {
        //文件路径
        $dirname = pathinfo($path, PATHINFO_DIRNAME);
        if (mb_substr($path, -1, 1) == '/' || !empty($name)) {
            $dirname = rtrim($path, '/');
        }
        //文件名
        $basename = $this->getBasename($path);
        if (!empty($name)) {
            $basename = $this->getBasename($name);
        }
        //后缀
        $ext = (($ext ?: pathinfo($name, PATHINFO_EXTENSION)) ?: pathinfo($path, PATHINFO_EXTENSION)) ?: $this->ext;
        if (!in_array($ext, self::ALLOW_EXT)) {
            throw new \Exception('文件扩展名错误');
        }
        //完整地址
        $complete_path = $dirname.'/'.$basename.'.'.$ext;

        return [
            'complete_path' => $complete_path,
            'dirname' => $dirname,
            'basename' => $basename,
            'ext' => $ext,
        ];

        /* $path_info = pathinfo($path);
        if (mb_substr($path, -1, 1) == '/' || !empty($name)) {
            $path_info['dirname'] = rtrim($path, '/');
            $path_info['basename'] = '';
            $path_info['extension'] = '';
        }
        if (empty($path_info['basename']) && empty($name)) {
            throw new \Exception('缺少文件名');
        }
        //文件名
        $name = $path_info['basename'] ?? '' ?: $name;
        $name_info = pathinfo($name);

        //扩展名
        $ext = (($path_info['extension'] ?? '' ?: $name_info['extension'] ?? '') ?: $ext) ?: $this->ext;
        if (!in_array($ext, self::ALLOW_EXT)) {
            throw new \Exception('文件扩展名错误');
        }
        //完整地址
        $complete_path = $path_info['dirname'].'/'.$name.'.'.$ext;

        return [
            'complete_path' => $complete_path,
            'dirname' => $path_info['dirname'],
            'name' => $name,
            'ext' => $ext,
        ];*/
    }

    /**
     * 文件名.
     */
    private function getBasename($filename)
    {
        return preg_replace('/^.+[\\\\\\/]/', '', $filename);
    }
}
