<?php

namespace app\obj;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * 导出类.
 *
 * @Author: godliu
 * @Date: 2020-11-02 10:17:00
 */
class Export
{
    /**************************************************属性：允许外部设置**************************************************/
    // 文件名
    private $file_name = '';

    // 文件路径
    private $file_path = '';

    // 导出数据
    private $export_data = [];
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    // 文件扩展名
    private $file_ext = '.xlsx';
    /**************************************************属性：不允许外部设置************************************************/

    /**
     * 设置文件名.
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * 设置文件路径.
     */
    public function setFilePath($file_path)
    {
        $this->file_path = $file_path;
    }

    /**
     * 设置导出数据.
     */
    public function setExportData($export_data)
    {
        $this->export_data = $export_data;
    }

    /**************************************************业务函数***********************************************************/

    /**
     * 生成excel.
     */
    public function genExcel()
    {
        /* if (count($this->export_data[0]) > 26) {
            throw new \Exception('导出excel最大列数为26，请缩减数据列数', 0);
        } */
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $length = count($this->export_data);
        for ($row = 1; $row <= $length; ++$row) {
            $index = 'A';
            foreach ($this->export_data[$row - 1] as $k => $v) {
                //$col = chr($index + 65);
                $cell = ($index.$row);
                $sheet->setCellValue($cell, $v);
                ++$index;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $file_url = $this->file_path.'/'.$this->file_name.$this->file_ext;
        $writer->save($file_url);

        $data['file_url'] = app()->getRootPath().'public/'.$this->file_path.'/'.$this->file_name.$this->file_ext;

        return $data;
    }
}
