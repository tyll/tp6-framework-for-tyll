<?php

namespace app\obj;

use app\model\Position as ModelPosition;
use app\model\RolePermition as ModelRolePermition;
use app\model\User as ModelUser;
use think\Collection;
use think\facade\Config;

/**
 * 权限.
 */
class Permition
{
    /**允许 */
    const ACTION_ALLOW = 'allow';
    /**拒绝 */
    const ACTION_DENY = 'deny';

    protected static $instance;

    //所有权限
    protected $permitions;

    public function __construct()
    {
        $this->permitions = new Collection(Config::get('tyll.permitionsDict'));
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * 获取权限描述.
     */
    public function getDesc($code)
    {
        $permition = $this->permitions->where('code', $code)->first();

        return $permition['desc'] ?? '';
    }

    /**
     * 获取人员的权限.
     */
    public function getUserPermitionArr(ModelUser $user)
    {
        //默认允许的权限
        $permition_arr = $this->permitions->where('default', self::ACTION_ALLOW)->column('code');

        //查找设置的权限
        $maps = [
            ['positionid', '=', $user->tongxunlu->positionid ?? 0],
        ];
        $role_permitions = ModelRolePermition::where($maps)->select();

        //合并
        foreach ($role_permitions as $k => $v) {
            //允许则直接添加
            if ($v->action == self::ACTION_ALLOW && !in_array($v->permition, $permition_arr)) {
                $permition_arr[] = $v->permition;
            } elseif ($v->action == self::ACTION_DENY && in_array($v->permition, $permition_arr)) {
                //不允许则去掉默认
                $index = array_search($v->permition, $permition_arr);
                unset($permition_arr[$index]);
            }
        }

        return $permition_arr;
    }

    /**
     * 获取拥有指定权限的岗位id.
     *
     * @param string $code 权限code
     */
    public function getPositionidByCode(string $code)
    {
        $permition = $this->permitions->where('code', $code)->first();

        $positionids = [];
        if ($permition['default'] == self::ACTION_ALLOW) {
            $positionids = ModelPosition::column('id');
            //找出禁止的岗位
            $maps = [
                'permition' => $code,
                'action' => self::ACTION_DENY,
            ];
            $deny_positionids = ModelRolePermition::where($maps)->column('positionid');
            $positionids = array_diff($positionids, $deny_positionids);
        } elseif ($permition['default'] == self::ACTION_DENY) {
            $maps = [
                'permition' => $code,
                'action' => self::ACTION_ALLOW,
            ];
            $positionids = ModelRolePermition::where($maps)->column('positionid');
        }

        return $positionids;
    }
}
