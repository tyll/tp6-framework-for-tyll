<?php

namespace app\obj;

use app\model\GongdanFlow as ModelGongdanFlow;
use app\model\User as ModelUser;
use app\model\Visitor as ModelVisitor;

/**
 * 工单流程类
 * @Author: godliu
 * @Date: 2022-06-17 10:00:00
 */
trait GongdanFlow
{
    /**************************************************属性：允许外部设置**************************************************/
    // 流程参数
    private $flow_params = [];
    /**************************************************属性：允许外部设置**************************************************/

    /**************************************************属性：不允许外部设置************************************************/
    /**************************************************属性：不允许外部设置************************************************/
    /**
     * 设置流程参数
     */
    public function setFlowParams($flow_params)
    {
        $this->flow_params = $flow_params;
    }

    /**************************************************业务函数***********************************************************/
    /**
     * 获取流程
     */
    public function getFlow()
    {
        try {
            $flow_info = [];
            $maps = [];
            $maps[] = ['gongdanid', '=', $this->flow_params['gongdanid']];
            $list = ModelGongdanFlow::getList($maps, 'id,oper_userid,to_userids,action,note,fujian,c_time', 'id DESC');
            foreach ($list as $k => $v) {
                $to_userid_arr = !empty($v['to_userids']) ? json_decode($v['to_userids'], true) : [];
                $maps = [];
                $maps[] = ['id', 'in', $to_userid_arr];
                $name_arr = ModelUser::getColumn($maps, 'name');

                if ($v['oper_userid'] > 0) {
                    $u_maps = [];
                    $u_maps[] = ['id', '=', $v['oper_userid']];
                    $u_detail = ModelUser::getDetail($u_maps, 'id,openid,name');
                    $username = !empty($u_detail) ? $u_detail['name'] : '';

                    $oper_openid = !empty($u_detail) ? $u_detail['openid'] : '';
                    $v_maps = [];
                    $v_maps[] = ['openid', '=', $oper_openid];
                    $v_detail = ModelVisitor::getDetail($v_maps, 'id,headimgurl');
                    $headimgurl = !empty($v_detail) ? $v_detail['headimgurl'] : '';
                } else {
                    $username = '系统';
                    $headimgurl = 'https://bcxj-1255346517.cos.ap-chongqing.myqcloud.com/static/logo-mini.png';
                }

                if ($v['action'] == 'faxian') {
                    $action_str = '发现问题';
                } elseif ($v['action'] == 'fenpai') {
                    $action_str = '分派工单给：' . implode(',', $name_arr);
                } elseif ($v['action'] == 'kaishi') {
                    $action_str = '开始处理';
                } elseif ($v['action'] == 'zhuanfa') {
                    $action_str = '转发给：' . implode(',', $name_arr);;
                } elseif ($v['action'] == 'wanbi') {
                    $action_str = '处理完毕';
                } elseif ($v['action'] == 'weijiejue') {
                    $action_str = '未解决';
                } elseif ($v['action'] == 'jiedan') {
                    $action_str = '已结单';
                }
                $flow_info[$k]['id'] = $v['id'];
                $flow_info[$k]['name'] = $username;
                $flow_info[$k]['headimgurl'] = $headimgurl;
                $flow_info[$k]['c_time'] = $v['c_time'];
                $flow_info[$k]['action_str'] = $action_str;
                $flow_info[$k]['note'] = $v['note'];
                $flow_info[$k]['fujian'] = !empty($v['fujian']) ? json_decode($v['fujian'], true) : [];
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
        $data['flow_info'] = $flow_info;
        return $data;
    }

    /**************************************************逻辑函数***********************************************************/
}
