<?php

namespace app\middleware;

use app\model\Tongxunlu as ModelTongxunlu;

class ApiAuth
{
    public function handle($request, \Closure $next)
    {
        $role_arr = [1, 2];
        $user_info = $request->user_info;
        if (!in_array($user_info['role'], $role_arr)) {
            return json(make_return_arr(0, '您没有权限进行该操作'));
        }
        return $next($request);
    }
}
