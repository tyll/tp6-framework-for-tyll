<?php

namespace app\middleware;

class TokenAuth
{
    public function handle($request, \Closure $next)
    {
        $param = $request->param();
        $token = isset($param['token']) ? $param['token'] : '';
        try {
            $tk = new \app\common\TyToken();
            $tk->setToken($token);

            // 检查token
            $tokenid = $tk->checkToken();
            $request->tokenid = $tokenid;
            return $next($request);
        } catch (\Throwable $th) {
            return json(make_return_arr($th->getCode(), $th->getMessage()));
        }
    }
}
