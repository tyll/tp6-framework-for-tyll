<?php

namespace app\middleware;

use app\model\Tongxunlu as ModelTongxunlu;

class RoleAuth
{
    public function handle($request, \Closure $next)
    {
        $user_info = $request->user_info;

        $maps = [];
        $maps[] = ['userid', '=', $user_info['id']];
        $maps[] = ['deptid', 'in', $user_info['deptid_arr']];
        $detail = ModelTongxunlu::getDetail($maps, 'id,role');
        $role = !empty($detail) ? $detail['role'] : -1;

        $request->user_info->role = $role;
        return $next($request);
    }
}
