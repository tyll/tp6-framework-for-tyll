<?php

namespace app\middleware;

use app\model\User as ModelUser;

class UserAuth
{
    public function handle($request, \Closure $next)
    {
        $userid = $request->tokenid;

        $maps = [];
        $maps[] = ['id', '=', $userid];
        $detail = ModelUser::getDetail($maps, 'id,master_corpid,name,tel,idcard_no,idcard_photo,photo,status,c_time');
        if (empty($detail)) {
            return json(make_return_arr(0, '该用户不存在'));
        }

        $request->user_info = $detail;
        return $next($request);
    }
}
