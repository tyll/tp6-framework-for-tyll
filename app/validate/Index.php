<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Index extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'old_password|旧密码'  =>  'require|max:16',
        'new_password|新密码'   =>  'require|max:16|different:old_password',
        'confirm_password|确认密码'    =>  'require|max:16|confirm:new_password'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'confirm_password.confirm'  =>  '第二次与第一次输入的密码不一致',
        'new_password.different'    =>  '新密码与旧密码相同'
    ];

    protected $scene = [
        'updatePwd' =>  ['old_password', 'new_password', 'confirm_password']
    ];
}
