<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'name'          => 'require',
        'photo'         => 'require',
        'idcard_photo'  => 'require|checkIdcardPhoto',
        'idcard_no'     => 'require|idCard',
        'rz_type'       => 'require|in:1,2',
        'tel'           => 'require|mobile',
        'master_corpid' => 'integer',
        'status'        => 'integer'
    ];

    protected $message = [
        'name.require'         => '姓名必须填写',
        'photo.require'        => '照片必须上传',
        'idcard_photo.require' => '身份证照片必须上传',
        'idcard_no.require'    => '身份证号必须填写',
        'idcard_no.idCard'     => '身份证号填写错误',
        'rz_type.require'      => '认证类型必须填写',
        'rz_type.in'           => '认证类型填写错误',
    ];

    protected function checkIdcardPhoto($value, $rule, $data)
    {
        $idcard_photo_arr = $data['idcard_photo'];
        if (!is_array($idcard_photo_arr)) {
            return '身份证照片数据格式不对';
        }
        if (empty($idcard_photo_arr['zm'])) {
            return '身份证照片正面必须上传';
        }
        if (empty($idcard_photo_arr['fm'])) {
            return '身份证照片反面必须上传';
        }

        return true;
    }

    protected function sceneIndex()
    {
        return $this->only(['name', 'master_corpid', 'status'])
            ->remove('name', 'require');
    }

    protected $scene = [
        'rz' => ['name', 'photo', 'idcard_photo', 'idcard_no', 'rz_type'],
        //'index' => ['name', 'tel', 'master_corpid', 'status']
    ];
}
