<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Login extends Validate
{
    protected $rule = [
        'tel'      => 'require|mobile',
        'code'     => 'require',
        'deviceid' => 'require',
        'username|用户名'=>'require',
        'password|密码'=>"require"
    ];

    protected $message = [
        'tel.require'      => '手机号码必须填写',
        'tel.mobile'       => '手机号码填写错误',
        'code.require'     => '手机验证码必须填写',
        'deviceid.require' => '手机设备id必须填写',
    ];

    protected $scene = [
        'send_code' => ['tel', 'deviceid'],
        'login'     => ['tel', 'code'],
        "admin_login"=>['username',"password"]
    ];
}
