<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class CheckItem extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...].
     *
     * @var array
     */
    protected $rule = [
        'id' => 'require|number|gt:0',
        'target' => 'require|in:device',
        'pk' => 'require|number|gt:0',
        'content' => 'require|max:255',
        'option' => 'array',
        'jianchazhouqi' => 'require|number',
        'tag' => 'max:255',
        'jianchazhouqi_unit' => 'max:16',
        'type' => 'require|in:radio,checkbox,number',
        'unit' => 'requireIf:type,number|max:16',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'.
     *
     * @var array
     */
    protected $message = [
        'target.require' => '目标必须填写',
        'target.in' => '目标填写错误',
        'pk' => '目标id必须填写',
        'content.require' => '检查内容必须填写',
        'content.max' => '检查内容长度错误',
        'option.require' => '选项必须填写',
        'option.array' => '选项格式错误',
        'tag.max' => '标签长度错误',
        'type.require' => '类型必须填写',
        'type.in' => '类型填写错误',
        'unit.requireIf' => '单位必须填写',
        'unit.max' => '单位长度错误',
        'jianchazhouqi_unit.max' => '检查周期单位长度错误',
    ];

    protected $scene = [
        'create' => ['target', 'pk', 'type', 'content', 'option', 'unit', 'jianchazhouqi_unit', 'tag'],
        'update' => ['id', 'target', 'pk', 'type', 'content', 'option', 'unit', 'jianchazhouqi_unit', 'tag'],
        'delete' => ['id'],
    ];
}
