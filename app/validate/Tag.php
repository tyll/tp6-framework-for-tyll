<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class tag extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...].
     *
     * @var array
     */
    protected $rule = [
        'id' => 'require|number|gt:0',
        'name' => 'require|max:64',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'.
     *
     * @var array
     */
    protected $message = [
        'name.require' => '名称必须填写',
        'name.max' => '名称长度错误',
    ];

    protected $scene = [
        'create' => ['name'],
        'update' => ['id', 'name'],
        'delete' => ['id'],
    ];
}
