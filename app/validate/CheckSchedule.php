<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class CheckSchedule extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...].
     *
     * @var array
     */
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:255',
        'canyu_userids' => 'require|array',
        'check_itemids' => 'require|array',
        'period' => 'require|in:day,week,month',
        'sn_day' => 'number',
        'start_time' => 'require',
        'end_time' => 'require',
        'time_arr' => 'require|array',
        'type' => 'require|in:1,2',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'.
     *
     * @var array
     */
    protected $message = [
        'title.require' => '标题必须填写',
        'title.max' => '标题长度错误',
        'canyu_userids.require' => '参与人必须填写',
        'canyu_userids.array' => '参与人格式错误',
        'check_itemids.require' => '检查项必须填写',
        'check_itemids.array' => '检查项格式错误',
        'period.require' => '周期必须填写',
        'period.array' => '周期填写错误',
        'sn_day.number' => 'sn_day格式错误',
        'start_time.array' => '开始时间必须填写',
        'end_time.array' => '结束时间必须填写',
        'time_arr.require' => '时间段必须填写',
        'time_arr.array' => '时间段格式错误',
        'type.require' => '类型必须填写',
        'type.in' => '类型填写错误',
    ];

    protected $scene = [
        'create' => ['title', 'canyu_userids', 'type', 'check_itemids', 'period', 'time_arr'],
        'update' => ['id', 'title', 'canyu_userids', 'type', 'check_itemids', 'period', 'time_arr'],
        'delete' => ['id'],
        'enable' => ['id'],
    ];
}
