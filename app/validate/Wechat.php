<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Wechat extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'sign' => 'require',
        'gen_time' => 'require|integer',
        'tel|手机号' => 'require|mobile',
        'code|验证码' => 'require|length:4,6',
        'corpid|企业id' => 'require|number|gt:0'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'openid'  =>  ['gen_time', 'sign'],
        'bind' => ['tel', 'code'],
        'get_info' => ['corpid'],
        'send_code' => ['tel']
    ];
}
