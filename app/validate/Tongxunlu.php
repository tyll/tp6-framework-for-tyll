<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Tongxunlu extends Validate
{
    protected $rule = [
        'corpid'    => 'require|gt:0',
        'deptid'    => 'require|gt:0',
        'position'  => 'require',
        'sh_action' => 'require|in:rz,no_pass,lz'
    ];

    protected $message = [
        'corpid.require'    => '企业必须填写',
        'corpid.gt'         => '企业填写错误',
        'deptid.require'    => '部门必须填写',
        'deptid.gt'         => '部门填写错误',
        'position.require'  => '职位必须填写',
        'sh_action.require' => '审核操作必须填写',
        'sh_action.in'      => '审核操作填写错误',
    ];

    protected $scene = [
        'rl'   => ['corpid', 'deptid', 'position'],
        'save' => ['corpid', 'deptid', 'position'],
        'sh'   => ['sh_action']
    ];
}
