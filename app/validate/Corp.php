<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Corp extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'page' => 'number',
        'rows' => 'number',
        "type|企业类型" => "integer|in:1,2",
        'fzr_tel|负责人电话' => 'require|mobile',
        'name|企业名称' => "require",
        'tyshxydm|统一社会信用代码' => 'require|length:18',
        'id'    =>  'require|number|gt:0',
        'ruzhu_status|入驻状态' => 'number',
        'fzr_name|负责人姓名' => 'require',
        'fzr_idcardno|负责人身份证号' => 'require|length:18',
        'staff_num|办公人数' => 'number',
        'first_ruzhu_date|首次入驻日期' => 'date'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'index' => ['page', 'rows'],
        'create'   =>   ['type', 'fzr_tel', 'name', 'tyshxydm', 'ruzhu_status', 'fzr_name', 'fzr_idcardno',  'staff_num', 'first_ruzhu_date'],
        'update'   =>   ['id'/* , 'fzr_tel', 'name', 'tyshxydm', 'ruzhu_status', 'fzr_name', 'fzr_idcardno',  'staff_num', 'first_ruzhu_date' */],
        'delete'    =>  ['id']
    ];
}
