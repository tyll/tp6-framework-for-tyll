<?php

declare(strict_types=1);

namespace app\validate;

use think\Validate;

class Device extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'page' => 'number',
        'rows' => 'number',
        'type|设备类型' => 'require|number',
        'name|设备名称' => "require",
        'model|设备型号' => 'require',
        'sn|设备序列号' => 'require',
        'id|设备id' => 'require'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        //'index' => ['type', 'page', 'rows'],
        'create' => ['type', 'name', 'model', 'sn'],
        'update' => ['id', 'type', 'name', 'model', 'sn'],
        'delete' => ['id']
    ];

    //验证场景定义
    public function sceneIndex()
    {
        return $this->only(['type', 'page', 'rows'])
            ->remove('type', 'require');
    }
}
