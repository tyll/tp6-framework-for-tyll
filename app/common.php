<?php

// 应用公共文件

/**
 * 返回数据.
 */
function make_return_arr($code, $msg, $data = [], $url = '')
{
    $result['code'] = $code;
    $result['msg'] = $msg;
    $result['data'] = $data;
    $result['url'] = $url;

    return $result;
}

/**
 * 数字乘（四舍五入）.
 */
function deal_num_mul($num, $base_num, $pos)
{
    $num = round($num * $base_num, $pos);

    return $num;
}

/**
 * 数字除（四舍五入）.
 */
function deal_num_div($num, $base_num, $pos)
{
    if ($base_num > 0) {
        $num = round($num / $base_num, $pos);
    } else {
        $num = 0;
    }

    return $num;
}

/**
 * 根据日期获取星期
 */
function week_by_date($date)
{
    $week_arr = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
    $week = date('w', strtotime($date));

    return $week_arr[$week];
}

/**
 * 写入日志.
 */
function write_log(string $msg, $level = 'info')
{
    if (is_object($msg) || is_array($msg)) {
        $msg = json_encode($msg, JSON_UNESCAPED_UNICODE);
    }

    $debug = env('app_debug');
    if ($debug) {
        \think\facade\Log::write($msg);
    } else {
        return false;
    }

    return true;
}

/**
 * 数字金额转换中文大写金额.
 */
function numToRmb($num)
{
    $c1 = '零壹贰叁肆伍陆柒捌玖';
    $c2 = '分角元拾佰仟万拾佰仟亿';

    // 将数字转化为整数
    $num = deal_num_mul($num, 100, 0);
    if (strlen($num) > 10) {
        return '';
    }
    $i = 0;
    $c = '';
    while (1) {
        if ($i == 0) {
            // 获取最后一位数字
            $n = substr($num, strlen($num) - 1, 1);
        } else {
            $n = $num % 10;
        }
        // 每次将最后一位数字转化为中文
        $p1 = substr($c1, 3 * $n, 3);
        $p2 = substr($c2, 3 * $i, 3);
        if ($n != '0' || ($n == '0' && ($p2 == '亿' || $p2 == '万' || $p2 == '元'))) {
            $c = $p1.$p2.$c;
        } else {
            $c = $p1.$c;
        }
        $i = $i + 1;
        // 去掉数字最后一位了
        $num = $num / 10;
        $num = (int) $num;
        //结束循环
        if ($num == 0) {
            break;
        }
    }

    $j = 0;
    $slen = strlen($c);
    while ($j < $slen) {
        // utf8一个汉字相当3个字符
        $m = substr($c, $j, 6);
        // 处理数字中很多0的情况，每次循环去掉一个汉字“零”
        if ($m == '零元' || $m == '零万' || $m == '零亿' || $m == '零零') {
            $left = substr($c, 0, $j);
            $right = substr($c, $j + 3);
            $c = $left.$right;
            $j = $j - 3;
            $slen = $slen - 3;
        }
        $j = $j + 3;
    }
    // 这个是为了去掉类似23.0中最后一个“零”字
    if (substr($c, strlen($c) - 3, 3) == '零') {
        $c = substr($c, 0, strlen($c) - 3);
    }
    // 将处理的汉字加上“整”
    if (empty($c)) {
        return '零元整';
    } else {
        return $c.'整';
    }
}
function get_day_date($date = null)
{
    $date ??= date('Y-m-d');

    $start_date = date('Y-m-d 00:00:00', strtotime($date));
    $end_date = date('Y-m-d 23:59:59', strtotime($date));

    return ['start_date' => $start_date, 'end_date' => $end_date];
}

/**
 * 获取日期的周开始与结束
 */
function get_week_date($date = null, $with_time = false)
{
    $date = $date ?? date('Y-m-d');

    $first_day = 1;
    $curr_day = date('w', strtotime($date));

    $start_format = $curr_day ? $curr_day - $first_day : 6;
    $start_date = date('Y-m-d', strtotime("$date -$start_format day"));
    $end_date = date('Y-m-d', strtotime("$start_date +6 day"));

    if ($with_time) {
        $start_date .= ' 00:00:00';
        $end_date .= ' 23:59:59';
    }

    return ['start_date' => $start_date, 'end_date' => $end_date];
}

/**
 * 获取月的开始与结束
 */
function get_month_date($date = null, $with_time = false)
{
    $date = $date ?: date('Y-m');
    $date = date('Y-m', is_numeric($date) ? $date : strtotime($date));

    $start_date = date('Y-m-01', strtotime($date));
    $end_date = date('Y-m-t', strtotime($date));

    if ($with_time) {
        $start_date .= ' 00:00:00';
        $end_date .= ' 23:59:59';
    }

    return [
        'start_date' => $start_date,
        'end_date' => $end_date,
    ];
}

/**
 * 获取年的开始与结束
 */
function get_year_date($date = null, $with_time = false)
{
    $date = $date ?: date('Y-01');
    if (strpos($date, '-') === false) {
        $date .= '-01';
    }

    $start_date = date('Y-01-01', strtotime($date));
    $end_date = date('Y-12-t', strtotime(date('Y-12', strtotime($date))));

    if ($with_time) {
        $start_date .= ' 00:00:00';
        $end_date .= ' 23:59:59';
    }

    return [
        'start_date' => $start_date,
        'end_date' => $end_date,
    ];
}

/**
 * 季度的开始与结束
 */
function get_season_date($date = null, $with_day = false, $with_time = false)
{
    $date = $date ?? date('Y-m-d');
    $year = date('Y', strtotime($date));

    //当前第几季度
    $month = (int) date('n', strtotime($date));
    $season = $month % 3 == 0 ? intdiv($month, 3) : intdiv($month, 3) + 1;

    //取出相应季度月份
    $month_arr = range(1, 12);
    $start_month = $month_arr[($season - 1) * 3];
    $end_month = $start_month + 2;

    $start_date = $year.'-'.($start_month < 10 ? '0'.$start_month : $start_month);
    $end_date = $year.'-'.($end_month < 10 ? '0'.$end_month : $end_month);

    if ($with_day) {
        $start_date = date('Y-m-01', strtotime($start_date));
        $end_date = date('Y-m-t', strtotime($end_date));
    }
    if ($with_day && $with_time) {
        $start_date .= ' 00:00:00';
        $end_date .= ' 23:59:59';
    }

    return ['start_date' => $start_date, 'end_date' => $end_date];
}

/**
 * 文件名.
 */
function get_basename($filename)
{
    return preg_replace('/^.+[\\\\\\/]/', '', $filename);
}

/**
 * 获取上传图片签名.
 */
function get_authorization($method, $pathname, $secret_id, $secret_key)
{
    //获取个人 API 密钥 https://console.qcloud.com/capi
    $SecretId = $secret_id;
    $SecretKey = $secret_key;
    //整理参数
    $queryParams = [];
    $headers = [];
    $method = strtolower($method ? $method : 'get');
    $pathname = $pathname ? $pathname : '/';
    substr($pathname, 0, 1) != '/' && ($pathname = '/'.$pathname);
    // 工具方法
    function getObjectKeys($obj)
    {
        $list = array_keys($obj);
        sort($list);

        return $list;
    }
    function obj2str($obj)
    {
        $list = [];
        $keyList = getObjectKeys($obj);
        $len = count($keyList);
        for ($i = 0; $i < $len; ++$i) {
            $key = $keyList[$i];
            $val = isset($obj[$key]) ? $obj[$key] : '';
            $key = strtolower($key);
            $list[] = rawurlencode($key).'='.rawurlencode($val);
        }

        return implode('&', $list);
    }
    //签名有效起止时间
    $now = time() - 1;
    $expired = $now + 600; //签名过期时刻，600 秒后
    //要用到的 Authorization 参数列表
    $qSignAlgorithm = 'sha1';
    $qAk = $SecretId;
    $qSignTime = $now.';'.$expired;
    $qKeyTime = $now.';'.$expired;
    $qHeaderList = strtolower(implode(';', getObjectKeys($headers)));
    $qUrlParamList = strtolower(implode(';', getObjectKeys($queryParams)));
    //签名算法说明文档：https://www.qcloud.com/document/product/436/7778
    //步骤一：计算 SignKey
    $signKey = hash_hmac('sha1', $qKeyTime, $SecretKey);
    //步骤二：构成 FormatString
    $formatString = implode("\n", [strtolower($method), $pathname, obj2str($queryParams), obj2str($headers), '']);
    //步骤三：计算 StringToSign
    $stringToSign = implode("\n", ['sha1', $qSignTime, sha1($formatString), '']);
    //步骤四：计算 Signature
    $qSignature = hash_hmac('sha1', $stringToSign, $signKey);
    //步骤五：构造 Authorization
    $authorization = implode('&', [
        'q-sign-algorithm='.$qSignAlgorithm,
        'q-ak='.$qAk,
        'q-sign-time='.$qSignTime,
        'q-key-time='.$qKeyTime,
        'q-header-list='.$qHeaderList,
        'q-url-param-list='.$qUrlParamList,
        'q-signature='.$qSignature,
    ]);

    return $authorization;
}

/**
 * cos_文件上传.
 */
function cos_upload($url, $conf, $filename)
{
    $cosClient = cos_client();
    $bucket = $conf['cos_bucket'].'-'.$conf['cos_appid'];  //存储桶名称 格式：BucketName-APPID
    $key = $conf['cos_path_prefix'].$filename;
    $file = fopen($url, 'rb');
    if ($file) {
        $result = $cosClient->Upload(
            $bucket = $bucket,
            $key = $key,
            $body = $file
        );
        $data = [];
        foreach ($result as $key => $value) {
            $data[$key] = $value;
        }

        return $data;
    }
}

/**
 * 创建cos_client 对象
 */
function cos_client()
{
    $secretId = 'AKIDSr5IKCwy770iEJnCxb4TgigXHUXQelAP'; //"云 API 密钥 SecretId";
    $secretKey = 'CC2hARWP9hUFtSr693mJesulRwX4Tmz3'; //"云 API 密钥 SecretKey";
    $region = 'ap-chongqing';
    $cosClient = new \Qcloud\Cos\Client(
        [
            'region' => $region,
            'schema' => 'https', //协议头部，默认为http
            'credentials' => [
                'secretId' => $secretId,
                'secretKey' => $secretKey,
            ],
        ]
    );

    return $cosClient;
}
