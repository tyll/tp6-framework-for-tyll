<?php

declare(strict_types=1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

class Task extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('app\command\task')
            ->setDescription('执行定时任务,如果定时任务比较多，每个有不同的执行频率的话，可以建多个command类');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $data = [100, 0];
        try {
            $yichang = new \Exception('test');
            $r = \app\common\Alarm::getInstance()->send(__METHOD__, $data, $yichang, 'test');
        } catch (\Throwable $th) {
            // \app\common\Alarm::getInstance()->send(__METHOD__, $data, $th, 'test');
            $output->writeln($th->getMessage());
        }
        $output->writeln($r);
    }
}
