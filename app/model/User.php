<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;
use app\model\Corp as ModelCorp;
use think\facade\Config;

class User extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /**
     * 主企业名称
     */
    public function getMasterCorpNameAttr($value, $data)
    {
        return ModelCorp::where('id', $data['master_corpid'])->value('name') ?? "";
    }

    /**
     * 身份证正反照片
     */
    public function getIdcardImgarrAttr($value, $data)
    {
        return json_decode($data['idcard_photo'], true);
    }

    /**
     * 身份证号
     */
    public function getIdcardNumAttr($value, $data)
    {
        return str_replace(substr($data['idcard_no'], 4, 11), "*****", $data['idcard_no']);
    }

    /**
     * 认证状态
     */
    public function getStatusZwAttr($value, $data)
    {
        return Config::get('tyll.dict.user_status')[$data['status']];
    }
}
