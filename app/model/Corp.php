<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;
use app\model\Dict as ModelDict;

class Corp extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;


    /**
     * 负责人姓名
     */
    public function getFzrAttr($value, $data)
    {
        return \app\model\User::where('id', $data['fzr_userid'])->value('name') ?? "";
    }

    /**
     * 身份证号
     */
    public function getFzrIdcardNumAttr($value, $data)
    {
        return str_replace(substr($data['fzr_idcardno'], 4, 11), "*****", $data['fzr_idcardno']);
    }

    /**
     * 企业类型
     */
    public function getTypeZwAttr($value, $data)
    {
        return \think\facade\Config::get('tyll.dict.corp_type')[$data['type']];
    }

    /**
     * 企业性质
     */
    public function getGsXingzhiZwAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'gs_xingzhi', 'key' => $data['gs_xingzhi']])->value('value') ?? "";
    }

    /**
     * 工商状态
     */
    public function getGsStatusZwAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'gs_status', 'key' => $data['gs_status']])->value('value') ?? "";
    }

    /**
     * 入驻状态
     */
    public function getRzStatusAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'ruzhu_status', 'key' => $data['ruzhu_status']])->value('value') ?? "";
    }
    /**
     * 纳税人类型
     */
    public function getNsrTypeZwAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'nsr_type', 'key' => $data['nsr_type']])->value('value') ?? "";
    }
    /**
     * 法定代表人类型
     */
    public function getFddbrTypeZwAttr($value, $data)
    {
        $arr = json_decode($data['fddbr_type'], true);
        $type = [];
        foreach ($arr as $v) {
            $type[] = ModelDict::where(['table' => 'corp', 'field' => 'fddbr_type', 'key' => $v])->value('value');
        }
        return $type;
    }
    /**
     * 销售易行业
     */
    public function getNeoHangyeZwAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'neo_hangye', 'key' => $data['neo_hangye']])->value('value') ?? "";
    }
    /**
     * 纳税信用级别
     */
    public function getNsCreditLevelAttr($value, $data)
    {
        return ModelDict::where(['table' => 'corp', 'field' => 'tax_credit_level', 'key' => $data['tax_credit_level']])->value('value') ?? "";
    }
    /**
     * 入库情况
     */
    public function getRukuZwAttr($value, $data)
    {
        $arr = json_decode($data['ruku'], true);
        $data = [];
        foreach ($arr as $v) {
            $data[] = ModelDict::where(['table' => 'corp', 'field' => 'ruku', 'key' => $v])->value('value');
        }
        return $data;
    }
}
