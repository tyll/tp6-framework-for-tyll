<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class CheckTaskGroup extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    //废弃字段
    protected $disuse = ['date'];

    /****************************************关联 */

    /**检查计划 */
    public function checkSchedule()
    {
        return $this->belongsTo(CheckSchedule::class, 'check_scheduleid', 'id');
    }

    /**检查任务 */
    public function task()
    {
        return $this->hasMany(CheckTask::class, 'task_groupid', 'id');
    }

    /**检查目标任务组 */
    public function taskTargetGroup()
    {
        return $this->hasMany(CheckTaskTargetGroup::class, 'task_groupid', 'id');
    }

    /***************************************获取器 */

    //状态
    public function getStatusZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_task_group.status')[$data['status']] ?? '';
    }

    //参与人
    public function getCanyuUseridArrAttr($value, $data)
    {
        return json_decode($data['canyu_userids'] ?? '[]', true) ?? [];
    }

    //参与人
    public function getCanyuUserNamesAttr($value, $data)
    {
        $userids = $this->canyu_userid_arr ?? [];
        $res = User::where('id', 'in', $userids)->column('name');

        return $res;
    }

    //实际巡检人
    public function getFactUseridArrAttr($value, $data)
    {
        return json_decode($data['fact_userids'] ?? '[]', true) ?? [];
    }

    //实际巡检人
    public function getFactUserArrAttr($value, $data)
    {
        $userids = $this->fact_userid_arr ?? [];
        $res = User::field('id,name,openid')->where('id', 'in', $userids)->append(['headimgurl'])->select();

        return $res;
    }

    //是否逾期
    public function getIsYuqiAttr($value, $data)
    {
        $is_yuqi = 0;
        if (in_array($data['status'], [0, 2])) {
            $datetime = date('Y-m-d H:i:s');
            if ($data['expect_end_time'] < $datetime) {
                $is_yuqi = 1;
            }
        }

        return $is_yuqi;
    }
}
