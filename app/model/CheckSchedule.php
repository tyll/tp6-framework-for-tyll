<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class CheckSchedule extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $type = [
        'canyu_userids' => 'array',
    ];
    //废弃字段
    protected $disuse = ['sn_day', 'start_time', 'end_time'];

    /*************************************关联 */

    /**检查项 */
    public function rItem()
    {
        return $this->hasMany(RItemSchedule::class, 'check_scheduleid', 'id');
    }

    /**目标计划 */
    public function targetSchedule()
    {
        return $this->hasMany(CheckTargetSchedule::class, 'check_scheduleid', 'id');
    }

    /**任务组 */
    public function taskGroup()
    {
        return $this->hasMany(CheckTaskGroup::class, 'check_scheduleid', 'id');
    }

    /**时间段 */
    public function times()
    {
        return $this->hasMany(CheckScheduleTimes::class, 'check_scheduleid', 'id');
    }

    /*************************************查询范围 */

    /**启用的 */
    public function scopeEnable($query, $enable = 1)
    {
        $query->where('enable', $enable);
    }

    /**********************************获取器 */

    //参与人
    public function getCanyuUserNamesAttr($value, $name)
    {
        $userids = $this->canyu_userids;
        $names = User::where('id', 'in', $userids)->column('name');

        return $names;
    }

    //参与人
    public function getCanyuUsersAttr($value, $name)
    {
        $userids = $this->canyu_userids;
        $names = User::where('id', 'in', $userids)->field('id,name,tel,openid')
            ->append(['rz_status', 'headimgurl', 'dept_name', 'position_name', 'rz_status_zw'])
            ->select();

        return $names;
    }

    //状态
    public function getStatusZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_schedule.status')[$data['status']] ?? '';
    }

    //启用禁用
    public function getEnableZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_schedule.enable')[$data['enable']] ?? '';
    }

    //检查项
    public function getCheckItemidsAttr($value, $data)
    {
        $items = $this->rItem()->field('id,check_itemid,sort')->order('sort asc')
        ->append(['item_content', 'item_target', 'item_pk', 'item_pk_name', 'item_area_name'])->select();

        return $items;
    }

    //路径
    public function getPathZwAttr($value, $data)
    {
        $areaids = explode('-', $data['path']);
        $names = Area::where('id', 'in', $areaids)->column('name');

        return implode('-', $names);
    }

    //检查项
    public function getTimeArrAttr($value, $data)
    {
        $times = $this->times()->field('id,start_sn_day,end_sn_day,start_time,end_time')->select();

        return $times;
    }

    //类型
    public function getTypeZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_schedule.type')[$data['type']] ?? '';
    }

    /**
     * 获取计划.
     */
    public static function getSchedule($period, $sn_day, $start_time)
    {
        $schedule = '';
        if ($period == 'day') {
            $schedule = $start_time;
        } elseif ($period == 'week') {
            $week = week_by_num($sn_day);
            $schedule = $week.$start_time;
        } elseif ($period == 'month') {
            $schedule = '每月'.$sn_day.'日'.$start_time;
        }
        $data['schedule'] = $schedule;

        return $data;
    }
}
