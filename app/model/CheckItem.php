<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class CheckItem extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /****************************************关联 */

    public function option()
    {
        return $this->hasMany(CheckItemOption::class, 'check_itemid', 'id');
    }

    /*************************************获取器 */

    //目标
    public function getTargetZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_item.target')[$data['target']] ?? '';
    }

    //类型
    public function getTypeZwAttr($value, $data)
    {
        return Config::get('tyll.dict.check_item.type')[$data['type']] ?? '';
    }

    //目标名称
    public function getPkNameAttr($value, $data)
    {
        return $this->targetModel['name'] ?? '';
    }

    //目标
    public function getTargetModelAttr($value, $data)
    {
        switch ($data['target']) {
            case 'device':
                return Device::find($data['pk']);
                break;

            default:
                return null;
                break;
        }
    }

    //区域
    public function getAreaNameAttr($value, $data)
    {
        $target = $this->target_model;

        return $target->area_name ?? '';
    }

    //标签
    public function getTagArrAttr($value, $data)
    {
        return !empty($data['tag']) ? explode(',', $data['tag']) : [];
    }
}
