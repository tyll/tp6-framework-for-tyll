<?php

declare(strict_types=1);

namespace app\model;

use think\Model;

class TyModel extends Model
{
    /**
     * 获取数据列表
     * @param array $maps
     * @param string $field
     * @param string $order
     * @param string $group
     * @param int $limit
     * @param int $row
     * @param bool $fetchSql
     * @return array|\PDOStatement|string|\think\Collection
     */
    public static function getList($maps = [], $field = '*', $order = '', $group = '', $limit = 0, $row = 0, $fetchSql = false)
    {
        $order = !empty($order) ? $order : 'id asc';
        $list = self::where($maps)->field($field)->orderRaw($order)->group($group)->limit($limit, $row)->fetchSql($fetchSql)->select();
        return $list;
    }

    /**
     * 获取数据详情
     * @param $maps
     * @param string $field
     * @param string $order
     * @param bool $fetchSql
     * @return array|\PDOStatement|string|Model|null
     */
    public static function getDetail($maps, $field = '*', $order = '', $fetchSql = false)
    {
        $order = !empty($order) ? $order : 'id asc';
        $detail = self::where($maps)->field($field)->orderRaw($order)->fetchSql($fetchSql)->find();
        return $detail;
    }

    /**
     * 获取某一列的值
     * @param array $maps
     * @param $field
     * @param string $order
     * @param string $group
     * @param int $limit
     * @param int $row
     * @param string $key
     * @param bool $fetchSql
     * @return array
     */
    public static function getColumn($maps = [], $field, $order = '', $group = '', $limit = 0, $row = 0, $key = '', $fetchSql = false)
    {
        $order = !empty($order) ? $order : 'id asc';
        $list = self::where($maps)->orderRaw($order)->group($group)->limit($limit, $row)->fetchSql($fetchSql)->column($field, $key);
        return $list;
    }

    /**
     * 统计数量
     * @param array $maps
     * @param $field
     * @param string $group
     * @param bool $fetchSql
     * @return float|string
     */
    public static function getCount($maps = [], $field, $group = '', $fetchSql = false)
    {
        $num = self::where($maps)->group($group)->fetchSql($fetchSql)->count($field);
        return $num;
    }

    /**
     * 获取总值
     * @param array $maps
     * @param $field
     * @param string $group
     * @param bool $fetchSql
     * @return float
     */
    public static function getSum($maps = [], $field, $group = '', $fetchSql = false)
    {
        $num = self::where($maps)->group($group)->fetchSql($fetchSql)->sum($field);
        return $num;
    }
}
