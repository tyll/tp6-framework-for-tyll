<?php

declare(strict_types=1);

namespace app\model;

use app\obj\Permition;
use think\model\concern\SoftDelete;

class User extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /************************关联 */

    /**通讯录 */
    public function tongxunlu()
    {
        return $this->hasOne(Tongxunlu::class, 'userid', 'id');
    }

    /**微信信息 */
    public function visitor()
    {
        return $this->hasOne(Visitor::class, 'openid', 'openid');
    }

    /************************获取器 */

    //入职状态
    public function getRzStatusAttr($value, $data)
    {
        return $this->tongxunlu->status ?? 0;
    }

    //入职状态
    public function getRzStatusZwAttr($value, $data)
    {
        return $this->tongxunlu->status_zw ?? '';
    }

    //头像
    public function getHeadimgurlAttr($value, $data)
    {
        return $this->visitor->headimgurl ?? '';
    }

    //部门
    public function getDeptNameAttr($value, $data)
    {
        $deptid = $this->tongxunlu->deptid ?? 0;

        return Dept::where('id', $deptid)->value('name') ?? '';
    }

    //岗位
    public function getPositionNameAttr($value, $data)
    {
        $positionid = $deptid = $this->tongxunlu->positionid ?? 0;

        return Position::where('id', $positionid)->value('name') ?? '';
    }

    //权限
    public function getPermitionArrAttr($value, $data)
    {
        return Permition::getInstance()->getUserPermitionArr($this);
    }
}
