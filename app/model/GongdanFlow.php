<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class GongdanFlow extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    // 动作
    public function getActionZwAttr($value, $data)
    {
        return Config::get('tyll.dict.gongdan_flow.action')[$data['action']] ?? '';
    }
}
