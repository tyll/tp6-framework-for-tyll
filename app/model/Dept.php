<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class Dept extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /****************************************获取器 */

    //下级
    public function getChildrenAttr($value, $data)
    {
        $children = $this->field('id,name,parentid')->where('parentid', $data['id'])->select();
        if (!$children) {
            $children->append(['children']);
        }

        return $children;
    }

    //上级
    public function getParentNameAttr($value, $data)
    {
        return $this->where('id', $data['parentid'])->value('name') ?? '';
    }
}
