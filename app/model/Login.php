<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class Login extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;
}
