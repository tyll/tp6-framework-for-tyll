<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class Conf extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /**
     * 获取设置信息
     */
    public static function getConfInfo($maps = [])
    {
        $conf_info = self::getList($maps);
        $conf_info_new = [];
        foreach ($conf_info as $k => $v) {
            $conf_info_new[$v['key']] = $v['value'];
        }
        return $conf_info_new;
    }

    /**
     * 更新设置信息
     */
    public static function saveConfInfo($data)
    {
        try {
            foreach ($data as $k => $v) {
                $c_data = [];
                $c_data['value'] = $v;

                $maps = [];
                $maps[] = ['key', '=', $k];
                $r = self::update($c_data, $maps);
            }
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 0);
        }
    }
}
