<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class CheckTaskTargetGroup extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    // 异常项
    public function getBadItemsNameAttr($value, $data)
    {
        if ($data['bad_items'] == 0) {
            $str = '无异常';
        } else {
            $str = '有' . $data['bad_items'] . '项异常';
        }
        return $str;
    }
}
