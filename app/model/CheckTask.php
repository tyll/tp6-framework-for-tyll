<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class CheckTask extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    //废弃字段
    protected $disuse = ['date'];

    /**********************************************关联 */

    /**检查项 */
    public function checkItem()
    {
        return $this->hasOne(CheckItem::class, 'id', 'check_itemid');
    }

    /**********************************************获取器 */

    //目标
    public function getItemTargetAttr($value, $data)
    {
        return $this->checkItem->target ?? '';
    }

    //目标id
    public function getItemTargetidAttr($value, $data)
    {
        return $this->checkItem['pk'] ?? '';
    }

    //检查项内容
    public function getItemContentAttr($value, $data)
    {
        return $this->checkItem['content'] ?? '';
    }

    //完成时间
    public function getDoneTimeStrAttr($value, $data)
    {
        return date('Y-m-d H:i:s', $data['done_time']);
    }

    //完成人头像
    public function getDoneUserHeadimgurlAttr($value, $data)
    {
        $user = User::find($data['done_userid']);

        return $user->headimgurl ?? '';
    }
}
