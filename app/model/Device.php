<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class Device extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    public function getTypeZwAttr($value, $data)
    {
        $type = \think\facade\Config::get('tyll.dict.device_type');
        return $type[$data['type']] ?? "";
    }
}
