<?php

declare(strict_types=1);

namespace app\model;

use app\obj\Permition;
use think\facade\Config;
use think\Model;
use think\model\concern\SoftDelete;

/**
 * @mixin \think\Model
 */
class RolePermition extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /*****************************************************获取器 */

    //操作
    public function getActionZwAttr($value, $data)
    {
        return Config::get('tyll.dict.role_permition.action')[$data['action']] ?? '';
    }

    //权限描述
    public function getPermitionDescAttr($value, $data)
    {
        return Permition::getInstance()->getDesc($data['permition']);
    }
}
