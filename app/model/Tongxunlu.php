<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class Tongxunlu extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /*************************获取器 */

    //状态
    public function getStatusZwAttr($value, $data)
    {
        return Config::get('tyll.dict.tongxunlu.status')[$data['status']] ?? '';
    }
}
