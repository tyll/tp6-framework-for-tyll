<?php

declare(strict_types=1);

namespace app\model;

use think\facade\Config;
use think\model\concern\SoftDelete;

class Gongdan extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    // 状态
    public function getStatusZwAttr($value, $data)
    {
        return Config::get('tyll.dict.gongdan.status')[$data['status']] ?? '';
    }
}
