<?php

declare(strict_types=1);

namespace app\model;

use think\Model;
use think\model\concern\SoftDelete;

/**
 * @mixin \think\Model
 */
class MessageDelivery extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /***************************************获取器 */

    /**
     * 内容.
     */
    public function getContentArrAttr($value, $data)
    {
        $value = json_decode($data['content'], true);
        if (is_bool($value) || is_null($value)) {
            $value = $data['content'];
        }

        return $value;
    }
}
