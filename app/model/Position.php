<?php

declare(strict_types=1);

namespace app\model;

use think\model\concern\SoftDelete;

class Position extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    /*********************************************关联 */

    /**权限 */
    public function permition()
    {
        return $this->hasMany(RolePermition::class, 'positionid', 'id');
    }

    /**********************************************获取器 */

    //部门
    public function getDeptNameAttr($value, $data)
    {
        return Dept::where('id', $data['deptid'])->value('name') ?? '';
    }

    //权限
    public function getPermitionsAttr($value, $data)
    {
        return $this->permition()->field('id,permition,action,note')->append(['action_zw', 'permition_desc'])->select();
    }
}
