<?php

namespace app\model;

use think\model\concern\SoftDelete;

class Download extends TyModel
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    public function getStatusNameAttr($value, $data)
    {
        $status = \think\facade\Config::get('tyll.dict.download_status');
        return $status[$data['status']];
    }
}
