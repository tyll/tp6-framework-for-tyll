<?php


namespace esign\esign_core;

use app\common\ESign;
use esign\esign_utils\HttpHelper;
use app\model\Token as ModelToken;
use Exception;

class TokenHelper extends BaseHelper
{
    //获取鉴权token
    public function getToken()
    {
        $eqb = new ESign();
        $appId = $eqb->getAppid();
        $secret = $eqb->getAppSecret();
        $getToken = $this->openApiUrl . $this->urlMap['TOKEN_URL'] . "?appId=$appId&secret=$secret&grantType=client_credentials";
        $return_content = HttpHelper::doGetCommon($getToken, []);
        $result = (array)json_decode($return_content, true);
        if (isset($result['code']) && $result['code'] != 0) {
            writeLog(__METHOD__ . $result['code'] . ":" . $result['message']);
            throw new Exception("esign_" . $result['code'] . ":" . $result['message'], 0);
        }
        //设置缓存
        //\think\facade\Cache::set("esign_token", $result['data']['token'], 7200);
        $this->saveToken($result['data']);

        return $result['data']['token'];
    }

    public static function getFileToken()
    {
        $token = ModelToken::where('key', 'esign')->find();
        //writeLog(__METHOD__ . "\ttoken:" . json_encode($token));
        if ($token && $token->token_expire_time > time()) {
            return $token->token;
        }
        return (new self())->getToken();
    }

    /**
     * 保存token
     */
    protected function saveToken(array $result)
    {
        $data = [
            'key' => 'esign',
            'token' => $result['token'],
            'token_expire_time' => time() + 3600 * 2
        ];
        if (ModelToken::where('key', $data['key'])->find()) {
            ModelToken::where('key', $data['key'])->update($data);
        } else {
            ModelToken::create($data);
        }

        return true;
    }
}
