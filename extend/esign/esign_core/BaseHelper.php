<?php

namespace esign\esign_core;


use esign\esign_constant\Config;
use app\common\ESign;

class BaseHelper
{
    protected $openApiUrl;
    protected $urlMap;
    protected $config;
    public function __construct()
    {
        $eqb = new \app\common\ESign();
        $this->config = Config::$config;
        $this->openApiUrl = Config::$config[(new ESign())->getModel()];
        $this->urlMap = Config::$urlMap;
    }
}
