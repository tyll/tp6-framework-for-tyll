<?php

/**
 * Created by IntelliJ IDEA.
 * User: cmn
 * Date: 2019-09-07
 * Time: 11:41.
 */

namespace esign\esign_utils;

use app\common\ESign;
use app\obj\Http;
use esign\esign_core\TokenHelper;

class HttpHelper
{
    public static $connectTimeout = 30; //30 second
    public static $readTimeout = 80; //80 second

    public static function doPost($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params = json_encode($data));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $appId = (new ESign())->getAppid();
        $token = TokenHelper::getFileToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header = ['X-Tsign-Open-App-Id:'.$appId, 'X-Tsign-Open-Token:'.$token, 'Content-Type:application/json']);
        ob_start();
        $data = curl_exec($ch);
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        \app\obj\Http::getInstance()->save($url, 'POST', $params, $data, $header);

        return $data;
    }

    /**
     * @param $url
     * @param $data
     *
     * @return mixed
     */
    public static function doGetCommon($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        return  $return_content;
    }

    public static function doGet($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params = $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $appId = (new ESign())->getAppid();
        $token = TokenHelper::getFileToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header = ['X-Tsign-Open-App-Id:'.$appId, 'X-Tsign-Open-Token:'.$token, 'Content-Type:application/json']);
        ob_start();
        $data = curl_exec($ch);
        ob_end_clean();
        \app\obj\Http::getInstance()->save($url, 'POST', $params, $data, $header);

        return $data;
    }

    /**
     * put.
     *
     * @param string $uploadUrls  上传的url地址
     * @param string $contentMd5  文件的Content-MD5
     * @param string $fileContent 文件内容字符串
     */
    public static function sendHttpPUT(string $uploadUrls, $contentMd5, $fileContent)
    {
        $header = [
            'Content-Type:application/pdf',
            'Content-Md5:'.$contentMd5,
        ];

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $uploadUrls);
        curl_setopt($curl_handle, CURLOPT_FILETIME, true);
        curl_setopt($curl_handle, CURLOPT_FRESH_CONNECT, false);
        curl_setopt($curl_handle, CURLOPT_HEADER, true); // 输出HTTP头 true
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 5184000);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');

        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fileContent);
        $result = curl_exec($curl_handle);
        $status = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

        if ($result === false) {
            $status = curl_errno($curl_handle);
            $result = 'put file to oss - curl error :'.curl_error($curl_handle);
        }
        curl_close($curl_handle);
        //    $this->debug($url, $fileContent, $header, $result);
        Http::getInstance()->save($uploadUrls, Http::METHOD_PUT, [], $result, $header);

        return $status;
    }

    public static function doPut($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        $appId = (new ESign())->getAppid();
        $token = TokenHelper::getFileToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header = ['X-Tsign-Open-App-Id:'.$appId, 'X-Tsign-Open-Token:'.$token, 'Content-Type:application/json']);
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        \app\obj\Http::getInstance()->save($url, 'POST', $data, $return_content, $header);

        return $return_content;
    }

    public static function doDelete($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $appId = (new ESign())->getAppid();
        $token = TokenHelper::getFileToken();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header = ['X-Tsign-Open-App-Id:'.$appId, 'X-Tsign-Open-Token:'.$token, 'Content-Type:application/json']);
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        \app\obj\Http::getInstance()->save($url, 'DELETE', $data, $return_content, $header);

        return $return_content;
    }

    public static function doPutNew($url, $data)
    {
        $data = (is_array($data)) ? json_encode($data, JSON_UNESCAPED_UNICODE) : $data;

        $ch = curl_init(); //初始化CURL句柄
        curl_setopt($ch, CURLOPT_URL, $url); //设置请求的URL
        $appId = (new ESign())->getAppid();
        $token = TokenHelper::getFileToken();
        //        curl_setopt($ch, CURLOPT_PUT, true); //设置为PUT请求
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['X-Tsign-Open-App-Id:'.$appId, 'X-Tsign-Open-Token:'.$token, 'Content-Type:application/json']);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //设为TRUE把curl_exec()结果转化为字串，而不是直接输出
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); //设置请求方式

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); //设置提交的字符串
        $res = curl_exec($ch);

        return $res;
    }
}
