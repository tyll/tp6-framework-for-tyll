<?php

$TYWYX_HOST = 'tywyx.tyll.net.cn';
$pid = 1;

//用户自定义一通配置信息
return [
    'fe' => [
        'web_path' => '/md-web/#/',
    ],
    'server_code' => env('server_code', 'unknowen'),
    // 字典
    'dict' => [
        //通讯录表
        'tongxunlu' => [
            'status' => [0 => '审核中', 1 => '在职', 2 => '审核未通过', 10 => '离职'],
        ],
        //设备检查项表
        'check_item' => [
            'target' => [/* 'area' => '区域',  */'device' => '设备'/* 'device_module' => '设备模块', 'device_part' => '设备部件' */],
            'type' => ['radio' => '单选', 'checkbox' => '多选', 'number' => '数值'],
        ],
        //检查计划表
        'check_schedule' => [
            'status' => [0 => '已停止', 1 => '正常'],
            'enable' => [0 => '禁用', 1 => '启用'],
            'type' => [1 => '每人完成', 2 => '共同完成'],
        ],

        //目标检查计划表
        'check_target_schedule' => [
            'enable' => [0 => '禁用', 1 => '启用'],
        ],

        //检查任务组表
        'check_task_group' => [
            'status' => [0 => '未开始', 1 => '已完成', 2 => '进行中'],
        ],

        //目标检查任务组表
        'check_task_target_group' => [
            'status' => [0 => '未开始', 1 => '已完成', 2 => '进行中'],
        ],

        //权限表
        'role_permition' => [
            'action' => ['allow' => '允许', 'deny' => '不允许'],
        ],
        //工单表
        'gongdan' => [
            'target' => ['device' => '设备'],
            'status' => [10 => '待分派', 20 => '未开始', 30 => '处理中', 40 => '待确认', 100 => '已结单'],
        ],

        //工单流程表
        'gongdan_flow' => [
            'action' => [
                'faxian' => '发现',
                'fenpai' => '分派工单',
                'kaishi' => '开始处理',
                'zhuanfa' => '转发',
                'wanbi' => '处理完毕',
                'weijiejue' => '未解决',
                'jiedan' => '结单',
            ],
        ],
    ],
    'redis' => [
        'host' => env('redis.host', 'bjredis.tyll.net.cn'),
        'port' => env('redis.port', 6379),
        'auth' => env('redis.auth', 'tyllredis'),
        'db' => env('redis.db', 4),
    ],
    'tywyx' => [
        'pid' => $pid,
        'openid_url' => "https://{$TYWYX_HOST}/index-index-openid-p_id-{$pid}",
        'userinfo_url' => "https://{$TYWYX_HOST}/index-index-get_userinfo",
        'accesstoken_url' => "https://{$TYWYX_HOST}/index-index-get_app_acesstoken-p_id-{$pid}",
        'jsticket_url' => "https://{$TYWYX_HOST}/index-index-get_jsticket-p_id-{$pid}",
        'template_msg_url' => "https://{$TYWYX_HOST}/index-index-send_template_msg-p_id-{$pid}",
    ],
];
