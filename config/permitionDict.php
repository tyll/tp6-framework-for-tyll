<?php

return [
    ['code' => 'v:button/fixDeviceLocation', 'desc' => '按钮/修正设备定位', 'default' => 'deny'],
    ['code' => 'v:view/newUser', 'desc' => '显示区域/待入职用户', 'default' => 'deny'],
    ['code' => 'v:view/totalNum', 'desc' => '显示区域/首页统计数据', 'default' => 'deny'],
];
