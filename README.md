TyTP6
===============
桃园立林公司基于tp6所做的一些个性化配置，基于tp6，不含任何前端代码，纯php，目的是为了能够帮助桃园立林公司的php工程师能够快速创建项目开始进行业务代码的开发

## 部署说明-to运维

1. 修改目录权限（public,pdf,runtime）

## 使用说明-to开发
1. app/common/Mail.php中需要设置密码，
    
2. unit是一个指向 phpunit的软连接，
    test目录里是示例的单元测试代码，这个版本的单元测试主要用来测试comman类,执行单元测试使用下面代码
    ```
    php unit test/common/AlarmTest.php 
    ```

## 定时任务

> 说明 
    ```
        0 0 * * * curl http://localhost:81/task.
    ```  
    